package com.amazing.tools.ad;

import android.util.Log;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;


/**
 * @author AvatarQing
 */
public class FacebookLogAdListener implements AdListener {

    private String tag;

    public FacebookLogAdListener(String tag) {
        this.tag = tag;
    }

    @Override
    public void onError(Ad ad, AdError adError) {
        Log.d(tag, "onError: " + adError);
    }

    @Override
    public void onAdLoaded(Ad ad) {
        Log.d(tag, "onAdLoaded");
    }

    @Override
    public void onAdClicked(Ad ad) {
    }

    @Override
    public void onLoggingImpression(Ad ad) {
    }

}
