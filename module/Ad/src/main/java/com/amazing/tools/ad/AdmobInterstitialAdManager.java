package com.amazing.tools.ad;

import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AvatarQing
 */
public class AdmobInterstitialAdManager {

    private volatile static AdmobInterstitialAdManager sInstance;

    public static AdmobInterstitialAdManager getInstance() {
        if (sInstance == null) {
            synchronized (AdmobInterstitialAdManager.class) {
                if (sInstance == null) {
                    sInstance = new AdmobInterstitialAdManager();
                }
            }
        }
        return sInstance;
    }

    private AdmobInterstitialAdManager() {
    }

    private Map<String, InterstitialAd> mAdMap = new HashMap<>();

    public InterstitialAd getAd(String adId) {
        return mAdMap.get(adId);
    }

    public InterstitialAd createAdIfAbsent(Context context, String adId, boolean reloadAfterClosed, String logTag) {
        InterstitialAd ad = mAdMap.get(adId);
        if (ad == null) {
            ad = new InterstitialAd(context.getApplicationContext());
            ad.setAdUnitId(adId);
            ad.setAdListener(new AdmobLogAdListener(logTag) {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    if (reloadAfterClosed) {
                        loadAd(adId);
                    }
                }
            });
            mAdMap.put(adId, ad);
        }
        return ad;
    }

    public void loadAd(String adId) {
        InterstitialAd ad = getAd(adId);
        if (ad != null && !ad.isLoaded() && !ad.isLoading()) {
            ad.loadAd(new AdRequest.Builder().build());
        }
    }

    public void showAd(String adId) {
        InterstitialAd ad = getAd(adId);
        if (ad != null && ad.isLoaded()) {
            ad.show();
        }
    }

}
