package com.amazing.tools.ad;

import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

/**
 * @author AvatarQing
 */
public class AdmobLogAdListener extends AdListener {

    private String tag;

    public AdmobLogAdListener(String tag) {
        this.tag = tag;
    }

    @Override
    public void onAdLoaded() {
        Log.d(tag, "onAdLoaded");
    }

    @Override
    public void onAdFailedToLoad(int errorCode) {
        String errorMsg;
        switch (errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                errorMsg = "INTERNAL_ERROR";
                break;
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                errorMsg = "INVALID_REQUEST";
                break;
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                errorMsg = "NETWORK_ERROR";
                break;
            case AdRequest.ERROR_CODE_NO_FILL:
                errorMsg = "NO_FILL";
                break;
            default:
                errorMsg = "UNKNOWN";
                break;
        }
        Log.w(tag, "onAdFailedToLoad, errorCode:" + errorCode + ", errorMsg:" + errorMsg);
    }

}
