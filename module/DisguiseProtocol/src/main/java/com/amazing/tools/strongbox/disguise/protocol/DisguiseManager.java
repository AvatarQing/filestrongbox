package com.amazing.tools.strongbox.disguise.protocol;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * @author AvatarQing
 */
public class DisguiseManager {

    public static final String ACTIVITY_ACTION_DISGUISE = "com.amazing.tools.strongbox.DISGUISE";
    public static final String EXTRA_DISGUISE_USE_TO_VERIFY = "EXTRA_DISGUISE_USE_TO_VERIFY";

    private static DisguiseManager sInstance;

    public static DisguiseManager get() {
        if (sInstance == null) {
            synchronized (DisguiseManager.class) {
                if (sInstance == null) {
                    sInstance = new DisguiseManager();
                }
            }
        }
        return sInstance;
    }

    private DisguiseManager() {
    }

    private IPinDisguiseCallback mPinDisguiseCallback;
    private int mPinLength = 0;

    public void setPinDisguise(IPinDisguiseCallback pinDisguise) {
        this.mPinDisguiseCallback = pinDisguise;
    }

    private int getPinLength() {
        return mPinLength;
    }

    public void setPinLength(int pinLength) {
        this.mPinLength = pinLength;
    }

    public boolean canUnlock(String inputNumbers) {
        return inputNumbers != null
                && inputNumbers.length() == getPinLength()
                && mPinDisguiseCallback != null
                && mPinDisguiseCallback.canUnlock(inputNumbers);
    }

    public void unlock(boolean useToVerify) {
        if (mPinDisguiseCallback != null) {
            mPinDisguiseCallback.unlock(useToVerify);
        }
    }

    public void onJustClosedDisguise() {
        if (mPinDisguiseCallback != null) {
            mPinDisguiseCallback.onJustClosedDisguise();
        }
    }

    public void startDisguiseActivity(Context context, boolean useToVerify) {
        Intent intent = new Intent(ACTIVITY_ACTION_DISGUISE)
                .putExtra(EXTRA_DISGUISE_USE_TO_VERIFY, useToVerify);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

}