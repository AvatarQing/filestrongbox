package com.amazing.tools.strongbox.disguise.protocol;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/7/30 </li>
 * </ul>
 */

public interface IPinDisguiseCallback {

    /**
     * 是否可解锁
     *
     * @param pin 给定一个密码
     * @return true表示可解锁，false表示不可解锁
     */
    boolean canUnlock(String pin);

    /**
     * 解锁
     *
     * @param useToVerify true表示仅用于验证密码
     */
    void unlock(boolean useToVerify);

    /**
     * 用户关闭了伪装页面，没有解锁
     */
    void onJustClosedDisguise();
}
