package com.amazing.tools.strongbox.datasource;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.entity.MediaFolder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Locale;

/**
 * @author LiuQing
 */
@RunWith(AndroidJUnit4.class)
public class ImageDataSourceTest {
    private static final String TAG = ImageDataSourceTest.class.getSimpleName();

    private ImageDataSource mImageDataSource;

    @Before
    public void setup() {
        Context context = InstrumentationRegistry.getTargetContext();
        mImageDataSource = new ImageDataSource(context);
    }

    @Test
    public void loadAllImages() {
        List<FileItem> imageList = mImageDataSource.loadAllImages();
        for (int i = 0; i < imageList.size(); i++) {
            FileItem fileItem = imageList.get(i);
            Log.d(TAG, String.format(Locale.getDefault(), "[%d] %s", i, fileItem.toString()));
        }
    }

    @Test
    public void loadImagesInFolder() {
        List<MediaFolder> mediaFolders = mImageDataSource.loadAllMediaFolders();
        if (mediaFolders.isEmpty()) {
            return;
        }
        for (MediaFolder folder : mediaFolders) {
            Log.d(TAG, String.format(Locale.getDefault(), "-------------Folder: %s--------------", folder.name));
            List<FileItem> imageList = mImageDataSource.loadImagesInFolder(folder.path);
            for (int i = 0; i < imageList.size(); i++) {
                FileItem fileItem = imageList.get(i);
                Log.d(TAG, String.format(Locale.getDefault(), "[%d] %s", i, fileItem.toString()));
            }
        }
    }

    @Test
    public void loadAllImageFolders() {
        List<MediaFolder> mediaFolders = mImageDataSource.loadAllMediaFolders();
        for (int i = 0; i < mediaFolders.size(); i++) {
            MediaFolder folder = mediaFolders.get(i);
            Log.d(TAG, String.format(Locale.getDefault(), "[%d] %s", i, folder.toString()));
        }
    }

}