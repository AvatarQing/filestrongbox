package com.amazing.tools.strongbox.ui.adatpter;

import android.content.Context;
import android.text.format.Formatter;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.ui.widget.FileThumbnailView;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

/**
 * @author AvatarQing
 */

public class RawFileListAdapter extends BaseQuickAdapter<FileItem, BaseViewHolder> {

    public RawFileListAdapter() {
        super(R.layout.item_ready_to_encrypt_image);
    }

    @Override
    protected void convert(BaseViewHolder helper, FileItem item) {
        Context context = helper.itemView.getContext();

        FileThumbnailView thumbnailView = helper.getView(R.id.thumbnail_view);
        if (item.fileType == FileTypeUtils.TYPE_IMAGE
                || item.fileType == FileTypeUtils.TYPE_VIDEO) {
            thumbnailView.showAsCircleImage(item.path);
        } else {
            thumbnailView.showAsFile();
        }

        helper.setText(R.id.file_name, item.name);

        String fileSizeFormatText = Formatter.formatFileSize(context, item.size);
        helper.setText(R.id.file_size, fileSizeFormatText);
    }

}