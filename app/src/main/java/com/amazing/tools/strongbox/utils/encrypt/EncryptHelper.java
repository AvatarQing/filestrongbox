package com.amazing.tools.strongbox.utils.encrypt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.amazing.tools.strongbox.BuildConfig;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.exception.DecryptFileException;
import com.amazing.tools.strongbox.exception.EncryptFileException;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.MD5Utils;
import com.amazing.tools.strongbox.utils.data.Keeper;
import com.raizlabs.android.dbflow.sql.saveable.ModelSaver;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Author: LiuQing
 * Date: 2017/7/11
 */

public class EncryptHelper {
    private static final String TAG = "EncryptHelper";

    private static volatile EncryptHelper sInstance;

    public static EncryptHelper getInstance() {
        if (sInstance == null) {
            synchronized (EncryptHelper.class) {
                if (sInstance == null) {
                    sInstance = new EncryptHelper();
                }
            }
        }
        return sInstance;
    }

    private EncryptHelper() {
    }

    public void init() {
        Completable.fromAction(CipherManager::init)
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public static File getRootSaveDir() {
        File rootDir = Environment.getExternalStorageDirectory();
        return new File(rootDir, BuildConfig.APPLICATION_ID);
    }

    public static File getEncryptedFilesSaveDir(@FileType int fileType) {
        String dirName;
        switch (fileType) {
            case FileTypeUtils.TYPE_IMAGE:
                dirName = "images";
                break;
            case FileTypeUtils.TYPE_VIDEO:
                dirName = "videos";
                break;
            case FileTypeUtils.TYPE_AUDIO:
                dirName = "audios";
                break;
            case FileTypeUtils.TYPE_TEXT:
            case FileTypeUtils.TYPE_FILE:
            default:
                dirName = "files";
                break;
        }
        return new File(getRootSaveDir(), dirName);
    }

    public static File getVideoThumbnailSaveDir() {
        return new File(getRootSaveDir(), ".thumbnails");
    }

    public boolean encryptFile(Context context, FileItem fileItem, EncryptedFileAlbum album) {
        File originalFile = new File(fileItem.path);
        if (!originalFile.exists()) {
            Timber.tag(getLogTag()).w(String.format(Locale.getDefault(), "Encrypt fail, file doesn't existed, %s ", originalFile.getAbsolutePath()));
            return false;
        }
        EncryptedFileInfo fileInfo = new EncryptedFileInfo();
        String encryptedFileName = System.currentTimeMillis() + "_" + MD5Utils.md5(originalFile.getAbsolutePath());
        File encryptedFile = new File(getEncryptedFilesSaveDir(fileItem.fileType), encryptedFileName);
        encryptedFile.getParentFile().mkdirs();

        fileInfo.encryptedFileName = encryptedFileName;
        fileInfo.encryptedFilePath = encryptedFile.getAbsolutePath();
        fileInfo.originalFileName = fileItem.name;
        fileInfo.originalFilePath = fileItem.path;
        fileInfo.modifiedTime = fileItem.modifiedTime;
        fileInfo.mimeType = fileItem.mimeType;
        fileInfo.fileType = fileItem.fileType;
        fileInfo.fileSize = fileItem.size;
        fileInfo.encryptedTime = System.currentTimeMillis();
        fileInfo.albumId = album != null ? album.id : EncryptedFilesDao.loadMainAlbum().id;
        // 保存视频缩略图
        if (fileInfo.fileType == FileTypeUtils.TYPE_VIDEO) {
            fileInfo.thumbnailFilePath = saveVideoThumbnail(originalFile.getAbsolutePath(), encryptedFileName);
        }

        long affectedRowCount = EncryptedFilesDao.insertFile(fileInfo);
        // 插入数据库成功
        if (affectedRowCount > ModelSaver.INSERT_FAILED) {
            boolean encryptSuccess = encryptFileInternal(originalFile, encryptedFile);
            if (encryptSuccess && originalFile.delete()) {
                // 刷新MediaStore
                deleteFileInfoInMediaStore(context, fileInfo.originalFilePath, fileInfo.fileType);
                Timber.tag(getLogTag()).i(String.format(Locale.getDefault(), "Encrypt success. %s", fileInfo.toString()));
                return true;
            }
        }
        BuglyLog.w(TAG, "Failed to encrypt file. " + fileInfo.toString());
        CrashReport.postCatchedException(new EncryptFileException(fileInfo));
        Timber.tag(getLogTag()).w(String.format(Locale.getDefault(), "Encrypt fail for %s ", fileInfo.toString()));
        return false;
    }

    private String saveVideoThumbnail(String videoPath, String thumbnailFileName) {
        Bitmap thumbnail = createVideoThumbnail(videoPath);
        if (thumbnail == null) {
            return null;
        }
        File thumbnailFile = new File(getVideoThumbnailSaveDir(), thumbnailFileName);
        saveBitmapToFile(thumbnail, thumbnailFile);
        thumbnail.recycle();
        return thumbnailFile.getAbsolutePath();
    }

    private boolean encryptFileInternal(File sourceFile, File destFile) {
        BufferedInputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(sourceFile));
            Cipher cipher = CipherManager.getEncryptCipher(Cipher.ENCRYPT_MODE);
            outputStream = new CipherOutputStream(new BufferedOutputStream(new FileOutputStream(destFile)), cipher);
            int read;
            byte[] buffer = new byte[1024];
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean decryptFile(Context context, EncryptedFileInfo fileInfo) {
        boolean decryptSuccess = false;
        if (fileInfo != null) {
            File encryptedFile = new File(fileInfo.encryptedFilePath);
            File originalFile = new File(fileInfo.originalFilePath); // TODO: 2017/7/12 处理命名冲突问题
            decryptSuccess = decryptFileInternal(encryptedFile, originalFile);
            // 删除视频缩略图
            if (fileInfo.fileType == FileTypeUtils.TYPE_VIDEO && !TextUtils.isEmpty(fileInfo.thumbnailFilePath)) {
                File thumbnailFile = new File(fileInfo.thumbnailFilePath);
                thumbnailFile.delete();
            }
            if (decryptSuccess && encryptedFile.delete()) {
                notifyMediaStoreReScanFile(context, originalFile);
                EncryptedFilesDao.deleteFile(fileInfo);
                Timber.tag(getLogTag()).i(String.format(Locale.getDefault(), "Decrypt success. %s", fileInfo.toString()));
            } else {
                Timber.tag(getLogTag()).w(String.format(Locale.getDefault(), "Decrypt fail for %s ", fileInfo.toString()));
                BuglyLog.w(TAG, "Failed to decrypt file. " + fileInfo.toString());
                CrashReport.postCatchedException(new DecryptFileException(fileInfo));
            }
        } else {
            String errorMsg = "Decrypt fail, can't query encrypted file info from db";
            Timber.tag(getLogTag()).w(errorMsg);
            BuglyLog.w(TAG, errorMsg);
            CrashReport.postCatchedException(new DecryptFileException(errorMsg));
        }
        return decryptSuccess;
    }

    private boolean decryptFileInternal(File sourceFile, File destFile) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            Cipher cipher = CipherManager.getEncryptCipher(Cipher.DECRYPT_MODE);
            inputStream = new CipherInputStream(new FileInputStream(sourceFile), cipher);
            outputStream = new BufferedOutputStream(new FileOutputStream(destFile));
            int read;
            byte[] buffer = new byte[1024];
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private void notifyMediaStoreReScanFile(Context context, File file) {
        // 参考链接：
        // https://stackoverflow.com/questions/20719198/how-to-delete-a-single-file-from-media-store
        // https://stackoverflow.com/questions/8379690/androids-media-scanner-how-do-i-remove-files
        // https://stackoverflow.com/questions/3300137/how-can-i-refresh-mediastore-on-android
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        } else {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
            MediaScannerConnection.scanFile(
                    context,
                    new String[]{file.getAbsolutePath()},
                    null,
                    null
            );
        }
    }

    private void deleteFileInfoInMediaStore(Context context, String filePath, @FileType int fileType) {
        Uri uri;
        switch (fileType) {
            case FileTypeUtils.TYPE_IMAGE:
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                break;
            case FileTypeUtils.TYPE_AUDIO:
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                break;
            case FileTypeUtils.TYPE_VIDEO:
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                break;
            case FileTypeUtils.TYPE_TEXT:
            case FileTypeUtils.TYPE_FILE:
            default:
                uri = MediaStore.Files.getContentUri("external");
                break;
        }
        int deleteCount = context.getContentResolver().delete(
                uri,
                MediaStore.MediaColumns.DATA + "='" + filePath + "'",
                null
        );
        Timber.tag(getLogTag()).i(String.format(Locale.getDefault(), "deleteFileInfoInMediaStore() -> deleteCount: %d, filePath: %s ", deleteCount, filePath));
    }

    public void increaseMaxEncryptCount(int increment) {
        int oldCount = Keeper.get().getMaxEncryptCount();
        int newCount = oldCount + increment;
        Keeper.get().setMaxEncryptCount(newCount);
    }

    public boolean hasEnoughEncryptCount(int wantToEncryptCount) {
        return wantToEncryptCount + getEncryptedFileCount() <= Keeper.get().getMaxEncryptCount();
    }

    public long getAvailableEncryptCount() {
        return Keeper.get().getMaxEncryptCount() - getEncryptedFileCount();
    }

    private long getEncryptedFileCount() {
        return EncryptedFilesDao.queryFileCount();
    }

    private Bitmap createVideoThumbnail(String filePath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
            return retriever.getFrameAtTime(-1);
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                // Ignore failures while cleaning up.
            }
        }
        return null;
    }

    private boolean saveBitmapToFile(Bitmap bitmap, File outFile) {
        if (bitmap == null) {
            return false;
        }
        outFile.getParentFile().mkdirs();
        OutputStream outputStream = null;
        try {
            Cipher cipher = CipherManager.getEncryptCipher(Cipher.ENCRYPT_MODE);
            outputStream = new CipherOutputStream(new BufferedOutputStream(new FileOutputStream(outFile)), cipher);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }

}