package com.amazing.tools.strongbox.typedef;

import android.support.annotation.IntDef;

import com.amazing.tools.strongbox.utils.FileTypeUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <ul>
 * <li> Authoer: AvatarQing </li>
 * <li> Date: 2017/7/12 </li>
 * </ul>
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({
        FileTypeUtils.TYPE_FILE,
        FileTypeUtils.TYPE_IMAGE,
        FileTypeUtils.TYPE_AUDIO,
        FileTypeUtils.TYPE_VIDEO,
        FileTypeUtils.TYPE_TEXT,
})
public @interface FileType {
}
