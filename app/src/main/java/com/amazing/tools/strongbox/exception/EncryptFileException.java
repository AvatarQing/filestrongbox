package com.amazing.tools.strongbox.exception;

import com.amazing.tools.strongbox.entity.EncryptedFileInfo;

/**
 * @author AvatarQing
 */
public class EncryptFileException extends Exception {

    public EncryptFileException(EncryptedFileInfo fileInfo) {
        super("Failed to encrypt file. " + fileInfo.toString());
    }

}
