package com.amazing.tools.strongbox.ui.adatpter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

/**
 * @author AvatarQing
 */
public class AlbumListAdapter extends BaseQuickAdapter<EncryptedFileAlbum, BaseViewHolder> {

    public AlbumListAdapter() {
        super(R.layout.item_album_in_dialog);
    }

    @Override
    protected void convert(BaseViewHolder helper, EncryptedFileAlbum item) {
        Context context = helper.itemView.getContext();

        // 封面图片
        ImageView coverView = helper.getView(R.id.album_cover);
        if (item.type != EncryptedFileAlbum.TYPE_RECYCLE_BIN && item.latestFileInfo != null) {
            if (item.latestFileInfo.fileType == FileTypeUtils.TYPE_IMAGE) {
                helper.setBackgroundColor(R.id.album_cover, Color.TRANSPARENT);
                GlideApp.with(coverView)
                        .load(item.latestFileInfo.encryptedFilePath)
                        .centerCrop()
                        .into(coverView);
            } else {
                helper.setBackgroundColor(R.id.album_cover, ContextCompat.getColor(context, R.color.file_thumbnail_normal_bg));
                helper.setImageResource(R.id.album_cover, R.drawable.ic_file);
            }
            coverView.setPadding(0, 0, 0, 0);
        } else {
            @DrawableRes int coverImageResId;
            @ColorRes int coverBgColorResId;
            switch (item.type) {
                case EncryptedFileAlbum.TYPE_MAIN:
                    coverImageResId = R.drawable.album_cover_photos_88_dp;
                    coverBgColorResId = R.color.album_main;
                    break;
                case EncryptedFileAlbum.TYPE_RECYCLE_BIN:
                    coverImageResId = R.drawable.album_cover_trash_88_dp;
                    coverBgColorResId = R.color.album_recycle_bin;
                    break;
                case EncryptedFileAlbum.TYPE_IMPORTANT:
                    coverImageResId = R.drawable.album_cover_heart_88_dp;
                    coverBgColorResId = R.color.album_important_files;
                    break;
                case EncryptedFileAlbum.TYPE_VIDEO:
                    coverImageResId = R.drawable.album_cover_video_88_dp;
                    coverBgColorResId = R.color.album_videos;
                    break;
                case EncryptedFileAlbum.TYPE_USER_CREATE:
                default:
                    coverImageResId = R.drawable.album_cover_photos_88_dp;
                    coverBgColorResId = R.color.album_user_created;
                    break;
            }
            int coverBgColor = ContextCompat.getColor(context, coverBgColorResId);
            Drawable coverDrawable = ContextCompat.getDrawable(context, coverImageResId);
            coverDrawable.setColorFilter(coverBgColor, PorterDuff.Mode.SCREEN);

            helper.setBackgroundColor(R.id.album_cover, coverBgColor);
            helper.setImageDrawable(R.id.album_cover, coverDrawable);

            int padding = (int) ViewUtils.dp2px(context, 8f);
            coverView.setPadding(padding, padding, padding, padding);
        }

        // 专辑名称
        helper.setText(R.id.album_name, item.name);

        // 专辑文件个数
        String countText = context.getString(R.string.format_file_count, item.fileCount);
        helper.setText(R.id.album_file_count, countText);
    }

}