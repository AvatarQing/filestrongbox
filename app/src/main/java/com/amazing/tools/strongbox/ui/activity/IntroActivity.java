package com.amazing.tools.strongbox.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.ColorUtils;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/8/12 </li>
 * </ul>
 */

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int bgColor = ColorUtils.getColorFromAttr(this, android.R.attr.colorBackground);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(bgColor);
        }
        setSkipText(getString(R.string.btn_skip));
        setDoneText(getString(R.string.btn_got_it));
        addSlide(AppIntroFragment.newInstance(
                newPageInfo(
                        R.string.title_disguise,
                        R.string.prompt_disguise,
                        R.drawable.view_guide_pageview_1,
                        bgColor
                )
        ));
        addSlide(AppIntroFragment.newInstance(
                newPageInfo(
                        R.string.title_encrypt,
                        R.string.prompt_encrypt,
                        R.drawable.view_guide_pageview_2,
                        bgColor
                )
        ));
    }

    private SliderPage newPageInfo(@StringRes int title, @StringRes int desc, @DrawableRes int image, @ColorInt int bgColor) {
        SliderPage pageInfo = new SliderPage();
        pageInfo.setTitle(getString(title));
        pageInfo.setDescription(getString(desc));
        pageInfo.setImageDrawable(image);
        pageInfo.setBgColor(bgColor);
        return pageInfo;
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        goToSetLockPinPage();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        goToSetLockPinPage();
    }

    private void goToSetLockPinPage() {
        startActivity(
                new Intent(this, SetInitialLockPinActivity.class)
        );
        finish();
    }

}
