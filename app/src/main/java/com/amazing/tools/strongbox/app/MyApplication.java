package com.amazing.tools.strongbox.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.amazing.tools.strongbox.BuildConfig;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.analytics.AppExceptionParser;
import com.amazing.tools.strongbox.db.BatchFlowContentObserver;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.db.EncryptedFilesDatabase;
import com.amazing.tools.strongbox.disguise.protocol.DisguiseManager;
import com.amazing.tools.strongbox.disguise.protocol.IPinDisguiseCallback;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.service.DaemonService;
import com.amazing.tools.strongbox.ui.activity.MainActivity;
import com.amazing.tools.strongbox.utils.LockPinHelper;
import com.amazing.tools.strongbox.utils.LockUiHelper;
import com.amazing.tools.strongbox.utils.SystemUtils;
import com.amazing.tools.strongbox.utils.data.Keeper;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;
import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.File;

import timber.log.Timber;

public class MyApplication extends Application {

    private Tracker mTracker;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initDebugTools();
        initGoogleAnalytics();
        initBugly();
        initLog();
        initDB();
        initData();
        initCipher();
        registerActivityCallback();
        setPinDisguiseCallback();
        DaemonService.start(this);
    }

    private void initCipher() {
        EncryptHelper.getInstance().init();
    }

    private void initDebugTools() {
        Stetho.initializeWithDefaults(this);
    }

    private void initBugly() {
        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = SystemUtils.getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化Bugly
        CrashReport.initCrashReport(getApplicationContext(), getString(R.string.bugly_app_id), BuildConfig.DEBUG);
    }

    private void initGoogleAnalytics() {
        ExceptionReporter myHandler = new ExceptionReporter(
                getDefaultTracker(),
                Thread.getDefaultUncaughtExceptionHandler(),
                this
        );
        myHandler.setExceptionParser(new AppExceptionParser(this));
        Thread.setDefaultUncaughtExceptionHandler(myHandler);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(getString(R.string.google_analytics_track_id));
            mTracker.enableAutoActivityTracking(true);
        }
        return mTracker;
    }

    private void initLog() {
        Logger.addLogAdapter(
                new AndroidLogAdapter(
                        PrettyFormatStrategy.newBuilder()
                                .tag(getString(R.string.app_name))
                                .methodOffset(4)
                                .methodCount(2)
                                .build()
                )
        );
        Timber.plant(new Timber.DebugTree() {
            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
                Logger.log(priority, tag, message, t);
            }
        });
    }

    private void registerActivityCallback() {
        registerActivityLifecycleCallbacks(LockUiHelper.getInstance());
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Timber.tag(getLogTag(activity)).i("onCreate()");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Timber.tag(getLogTag(activity)).i("onStart()");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Timber.tag(getLogTag(activity)).i("onResume()");
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Timber.tag(getLogTag(activity)).i("onPause()");
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Timber.tag(getLogTag(activity)).i("onStop()");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                Timber.tag(getLogTag(activity)).i("onSaveInstanceState()");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Timber.tag(getLogTag(activity)).i("onDestroyed()");
            }

            private String getLogTag(Activity activity) {
                return activity.getClass().getSimpleName();
            }
        });
    }

    private void initData() {
        Keeper.get().init(this);
        EncryptedFilesDao.insertDefaultAlbumsIfNeed(this);
    }

    private void initDB() {
        FlowManager.init(
                FlowConfig.builder(this)
                        .addDatabaseConfig(
                                DatabaseConfig.builder(EncryptedFilesDatabase.class)
                                        .databaseName(EncryptedFilesDatabase.NAME)
                                        .extensionName("db")
                                        .build()
                        )
                        .build()
        );
        FlowLog.setMinimumLoggingLevel(BuildConfig.DEBUG ? FlowLog.Level.V : FlowLog.Level.W);
        BatchFlowContentObserver.getInstance().registerForContentChanges(getContentResolver(), EncryptedFileInfo.class);
    }

    private void setPinDisguiseCallback() {
        DisguiseManager.get().setPinLength(getResources().getInteger(R.integer.pin_length));
        DisguiseManager.get().setPinDisguise(new IPinDisguiseCallback() {
            @Override
            public boolean canUnlock(String pin) {
                return LockPinHelper.get().verifyPin(pin);
            }

            @Override
            public void unlock(boolean useToVerify) {
                if (!useToVerify) {
                    Context context = getApplicationContext();
                    context.startActivity(
                            new Intent(context, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            }

            @Override
            public void onJustClosedDisguise() {
                goToLauncherPage();
            }

            private void goToLauncherPage() {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory) {
        if (needSaveDbFileOnSdcard(name)) {
            name = getDatabasePath(name).getAbsolutePath();
        }
        Timber.tag(getLogTag()).d("openOrCreateDatabase() -> " + name);
        return super.openOrCreateDatabase(name, mode, factory);
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        if (needSaveDbFileOnSdcard(name)) {
            name = getDatabasePath(name).getAbsolutePath();
        }
        Timber.tag(getLogTag()).d("openOrCreateDatabase() -> " + name);
        return super.openOrCreateDatabase(name, mode, factory, errorHandler);
    }

    @Override
    public boolean deleteDatabase(String name) {
        if (needSaveDbFileOnSdcard(name)) {
            name = getDatabasePath(name).getAbsolutePath();
        }
        Timber.tag(getLogTag()).d("deleteDatabase() -> " + name);
        return super.deleteDatabase(name);
    }

    @Override
    public File getDatabasePath(String name) {
        File dbFile;
        if (needSaveDbFileOnSdcard(name)) {
            dbFile = new File(EncryptHelper.getRootSaveDir(), "databases/" + name);
        } else {
            dbFile = super.getDatabasePath(name);
        }
        Timber.tag(getLogTag()).d("getDatabasePath() -> " + dbFile.getAbsolutePath());
        return dbFile;
    }

    protected boolean needSaveDbFileOnSdcard(String dbName) {
        return TextUtils.equals(dbName, EncryptedFilesDatabase.NAME + ".db");
    }

    private String getLogTag() {
        return getClass().getSimpleName();
    }
}