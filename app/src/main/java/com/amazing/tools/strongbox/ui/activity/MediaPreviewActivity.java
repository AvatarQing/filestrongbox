package com.amazing.tools.strongbox.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.dataholder.FileListDataHolder;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.ui.fragment.ImagePreviewFragment;
import com.amazing.tools.strongbox.ui.fragment.VideoPreviewFragment;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author AvatarQing
 */
public class MediaPreviewActivity extends BaseAppCompatActivity {

    public static final String EXTRA_CURRENT_INDEX = "EXTRA_CURRENT_INDEX";
    public static final String EXTRA_IS_ENCRYPTED = "EXTRA_IS_ENCRYPTED";

    public static void launch(Activity activity, int currentIndex, boolean isEncrypted) {
        activity.startActivity(
                new Intent(activity, MediaPreviewActivity.class)
                        .putExtra(EXTRA_CURRENT_INDEX, currentIndex)
                        .putExtra(EXTRA_IS_ENCRYPTED, isEncrypted)
        );
    }

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    protected int mCurrentIndex = 0;
    protected boolean mIsEncrypted = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        ButterKnife.bind(this);
        parseArguments();
        initViews();
    }

    @OnClick(R.id.fab)
    void exit() {
        fab.hide(new FloatingActionButton.OnVisibilityChangedListener() {
            @Override
            public void onHidden(FloatingActionButton fab) {
                finish();
            }
        });
    }

    protected void parseArguments() {
        mCurrentIndex = getIntent().getIntExtra(EXTRA_CURRENT_INDEX, mCurrentIndex);
        mIsEncrypted = getIntent().getBooleanExtra(EXTRA_IS_ENCRYPTED, mIsEncrypted);
    }

    protected void initViews() {
        List<FileItem> data = FileListDataHolder.getData();
        if (data == null) {
            CrashReport.postCatchedException(new IllegalArgumentException("Preview page data is null. preview encrypted medias:" + mIsEncrypted));
            return;
        }
        MediaPreviewAdapter adapter = new MediaPreviewAdapter(getSupportFragmentManager(), data, mIsEncrypted);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(mCurrentIndex);
    }

    static class MediaPreviewAdapter extends FragmentStatePagerAdapter {
        List<FileItem> mDataList = new ArrayList<>();
        boolean mIsEncrypted;

        MediaPreviewAdapter(FragmentManager fm, List<FileItem> dataList, boolean isEncrypted) {
            super(fm);
            if (dataList != null) {
                mDataList = dataList;
            }
            mIsEncrypted = isEncrypted;
        }

        @Override
        public Fragment getItem(int position) {
            FileItem fileItem = mDataList.get(position);
            switch (fileItem.fileType) {
                case FileTypeUtils.TYPE_VIDEO:
                    return VideoPreviewFragment.newInstance(fileItem.path, mIsEncrypted);
                case FileTypeUtils.TYPE_IMAGE:
                case FileTypeUtils.TYPE_AUDIO:
                case FileTypeUtils.TYPE_FILE:
                case FileTypeUtils.TYPE_TEXT:
                default:
                    return ImagePreviewFragment.newInstance(fileItem.path);
            }
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }
    }

}
