package com.amazing.tools.strongbox.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.amazing.tools.strongbox.utils.ViewUtils;

public class LotteryWheelLayout extends RelativeLayout {

    public static final int DEFAULT_TIME_PERIOD = 500;
    private static final String START_BTN_TAG = "startbtn";

    private Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint whitePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint yellowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int radius;
    private int circleX, circleY;
    private Canvas canvas;
    private boolean isYellow = false;
    private int delayTime = 500;
    private RotateWheel rotatePan;
    private ImageView startBtn;

    private int screenWidth, screeHeight;

    public LotteryWheelLayout(Context context) {
        this(context, null);
    }

    public LotteryWheelLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LotteryWheelLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        backgroundPaint.setColor(Color.rgb(255, 92, 93));
        whitePaint.setColor(Color.WHITE);
        yellowPaint.setColor(Color.YELLOW);
        screeHeight = getResources().getDisplayMetrics().heightPixels;
        screenWidth = getResources().getDisplayMetrics().widthPixels;
        post(this::startLuckLight);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int minValue = Math.min(screenWidth, screeHeight);
        minValue -= ViewUtils.dp2px(getContext(), 10) * 2;
        setMeasuredDimension(minValue, minValue);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;

        final int paddingLeft = getPaddingLeft();
        final int paddingRight = getPaddingRight();
        final int paddingTop = getPaddingTop();
        final int paddingBottom = getPaddingBottom();

        int width = getWidth() - paddingLeft - paddingRight;
        int height = getHeight() - paddingTop - paddingBottom;

        int minValue = Math.min(width, height);

        radius = minValue / 2;
        circleX = getWidth() / 2;
        circleY = getHeight() / 2;

        canvas.drawCircle(circleX, circleY, radius, backgroundPaint);

        drawSmallCircle(isYellow);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        int centerX = (right - left) / 2;
        int centerY = (bottom - top) / 2;
        boolean panReady = false;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child instanceof RotateWheel) {
                rotatePan = (RotateWheel) child;
                int panWidth = child.getWidth();
                int panHeight = child.getHeight();
                child.layout(centerX - panWidth / 2, centerY - panHeight / 2, centerX + panWidth / 2, centerY + panHeight / 2);
                panReady = true;
            } else if (child instanceof ImageView) {
                if (TextUtils.equals((String) child.getTag(), START_BTN_TAG)) {
                    startBtn = (ImageView) child;
                    int btnWidth = child.getWidth();
                    int btnHeight = child.getHeight();
                    child.layout(centerX - btnWidth / 2, centerY - btnHeight / 2, centerX + btnWidth / 2, centerY + btnHeight / 2);
                }
            }
        }

        if (!panReady) {
            throw new RuntimeException("Have you add RotatePan in LuckPanLayout element ?");
        }
    }

    private void drawSmallCircle(boolean firstYellow) {
        int pointDistance = (int) (radius - ViewUtils.dp2px(getContext(), 10));
        for (int i = 0; i <= 360; i += 20) {
            int x = (int) (pointDistance * Math.sin(change(i))) + circleX;
            int y = (int) (pointDistance * Math.cos(change(i))) + circleY;

            if (firstYellow) {
                canvas.drawCircle(x, y, ViewUtils.dp2px(getContext(), 4), yellowPaint);
            } else {
                canvas.drawCircle(x, y, ViewUtils.dp2px(getContext(), 4), whitePaint);
            }
            firstYellow = !firstYellow;
        }
    }

    /**
     * 开始旋转
     *
     * @param pos       转到指定的转盘，-1 则随机
     * @param delayTime 外围灯光闪烁的间隔时间
     */
    public void rotate(int pos, int delayTime) {
        rotatePan.startRotate(pos);
        setDelayTime(delayTime);
        setStartBtnEnable(false);
    }

    public void setStartBtnEnable(boolean enable) {
        if (startBtn != null) {
            startBtn.setEnabled(enable);
        } else {
            throw new RuntimeException("Have you add start button in LuckPanLayout element ?");
        }
    }

    private void startLuckLight() {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                isYellow = !isYellow;
                invalidate();
                postDelayed(this, delayTime);
            }
        }, delayTime);
    }

    protected void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    public interface AnimationEndListener {
        void endAnimation(int position, String value);
    }

    private AnimationEndListener l;

    public void setAnimationEndListener(AnimationEndListener l) {
        this.l = l;
    }

    public AnimationEndListener getAnimationEndListener() {
        return l;
    }

    public static double change(double a) {
        return a * Math.PI / 180;
    }
}
