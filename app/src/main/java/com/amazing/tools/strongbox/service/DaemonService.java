package com.amazing.tools.strongbox.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.amazing.tools.strongbox.contentobserver.ImageContentObserver;

/**
 * @author LiuQing
 */
public class DaemonService extends Service {

    private ImageContentObserver mImageContentObserver;

    public static void start(Context context) {
        context.startService(new Intent(context, DaemonService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mImageContentObserver = new ImageContentObserver(getApplicationContext());
        getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false, mImageContentObserver);
    }

    @Override
    public int onStartCommand(Intent intent,  int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(mImageContentObserver);
    }

}