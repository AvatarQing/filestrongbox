package com.amazing.tools.strongbox.ui.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.R;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author AvatarQing
 */
public class ImagePreviewFragment extends BaseFragment {

    private static final String ARG_IMAGE_PATH = "ARG_IMAGE_PATH";
    private static final String SUFFIX_GIF = ".gif";

    @BindView(R.id.scale_image)
    SubsamplingScaleImageView scaleImageView;
    @BindView(R.id.iv_gif)
    ImageView gifView;

    private String mImagePath;


    public static ImagePreviewFragment newInstance(String imagePath) {
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE_PATH, imagePath);

        ImagePreviewFragment fragment = new ImagePreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parseArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.item_detail_image, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showImage();
    }

    private void parseArguments() {
        if (getArguments() == null) {
            return;
        }
        mImagePath = getArguments().getString(ARG_IMAGE_PATH);
    }

    private void showImage() {
        boolean isGif = mImagePath.endsWith(SUFFIX_GIF);
        gifView.setVisibility(isGif ? View.VISIBLE : View.GONE);
        scaleImageView.setVisibility(!isGif ? View.VISIBLE : View.GONE);

        if (mImagePath.endsWith(SUFFIX_GIF)) {
            GlideApp.with(gifView)
                    .load(mImagePath)
                    .into(gifView);
        } else {
            GlideApp.with(scaleImageView)
                    .asBitmap()
                    .load(mImagePath)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            scaleImageView.setImage(ImageSource.bitmap(resource));
                        }
                    });
        }
    }

}
