package com.amazing.tools.strongbox.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.MediaFolder;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.ui.fragment.SelectMediaFolderFragment;
import com.amazing.tools.strongbox.ui.fragment.SelectMediaItemFragment;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Author: LiuQing
 * Date: 2017/7/4
 */

public class SelectMediaActivity extends BaseAppCompatActivity implements SelectMediaFolderFragment.OnFolderSelectListener {

    private static final String EXTRA_ALBUM = "EXTRA_ALBUM";
    private static final String EXTRA_MEDIA_TYPE = "EXTRA_MEDIA_TYPE";

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.available_encrypt_count)
    TextView availableEncryptCount;
    @BindView(R.id.space_remind_bar)
    View spaceRemindBar;

    private EncryptedFileAlbum mAlbum;
    @FileType
    private int mMediaType;

    public static void launch(Activity activity, EncryptedFileAlbum album, @FileType int mediaType) {
        activity.startActivity(
                new Intent(activity, SelectMediaActivity.class)
                        .putExtra(EXTRA_ALBUM, album)
                        .putExtra(EXTRA_MEDIA_TYPE, mediaType)
        );
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);
        parseArguments();
        initViews();
    }

    private void initViews() {
        ButterKnife.bind(this);
        updateTitle();
        initToolBar();
        addFragment();
        showAvailableEncryptCount();
    }

    private void parseArguments() {
        mAlbum = getIntent().getParcelableExtra(EXTRA_ALBUM);
        mMediaType = getIntent().getIntExtra(EXTRA_MEDIA_TYPE, FileTypeUtils.TYPE_IMAGE);
    }

    private void updateTitle() {
        switch (mMediaType) {
            case FileTypeUtils.TYPE_VIDEO:
                setTitle(R.string.title_select_videos_to_encrypt);
                break;
            case FileTypeUtils.TYPE_IMAGE:
            case FileTypeUtils.TYPE_AUDIO:
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                setTitle(R.string.title_select_images_to_encrypt);
                break;
        }
    }

    private void initToolBar() {
        toolbar.setTitle(getTitle());
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void addFragment() {
        SelectMediaFolderFragment foldersFragment = SelectMediaFolderFragment.newInstance(mMediaType);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, foldersFragment, SelectMediaFolderFragment.TAG)
                .commit();
    }

    private void showAvailableEncryptCount() {
        long count = EncryptHelper.getInstance().getAvailableEncryptCount();
        String text = getString(R.string.format_available_encrypt_count, count);
        availableEncryptCount.setText(text);
    }

    @Override
    public void onFolderSelected(MediaFolder folder) {
        SelectMediaItemFragment imagesFragment = SelectMediaItemFragment.newInstance(folder.path, mAlbum, mMediaType);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, imagesFragment, SelectMediaItemFragment.TAG)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                .addToBackStack(SelectMediaItemFragment.TAG)
                .commit();
    }

    @OnClick(R.id.btn_continue)
    void goToLotteryPage() {
        spaceRemindBar.setVisibility(View.GONE);
        startActivity(new Intent(this, LotteryActivity.class));
    }

}