package com.amazing.tools.strongbox.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.CycleInterpolator;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.disguise.protocol.DisguiseManager;
import com.amazing.tools.strongbox.utils.AppIconHelper;
import com.amazing.tools.strongbox.utils.LockPinHelper;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.amazing.tools.strongbox.utils.data.Keeper;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/8/13 </li>
 * </ul>
 */

public class SetInitialLockPinActivity extends BaseAppCompatActivity {

    private static final String TAG = SetInitialLockPinActivity.class.getSimpleName();

    @BindView(R.id.pin_lock_view)
    PinLockView pin_lock_view;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicator_dots;
    @BindView(R.id.tv_subtitle)
    TextView tv_subtitle;
    @BindView(R.id.btn_reset)
    View btn_reset;
    @BindView(R.id.btn_ok)
    View btn_ok;

    private ViewPropertyAnimator shakeAnim;
    private Animator titleAnim;
    private String tempPin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue typedValue = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.colorBackground, typedValue, true);
            getWindow().setStatusBarColor(typedValue.data);
        }
        setContentView(R.layout.activity_set_initial_lock_pin);
        ButterKnife.bind(this);
        initLockPinView();
    }

    @OnClick(R.id.btn_reset)
    void resetPin() {
        tempPin = null;
        pin_lock_view.resetPinLockView();
        btn_reset.setEnabled(false);
        btn_ok.setEnabled(false);
        animateToChangeTitle(R.string.title_input_lock_pin);
    }

    @OnClick(R.id.btn_ok)
    void done() {
        LockPinHelper.get().savePin(tempPin);
        setToNotShowIntroPageAnymore();
        DisguiseManager.get().startDisguiseActivity(this, false);
        AppIconHelper.changeAppIconToDisguise(this);
        finish();
    }

    private void initLockPinView() {
        pin_lock_view.setPinLength(getResources().getInteger(R.integer.pin_length));
        pin_lock_view.attachIndicatorDots(indicator_dots);
        pin_lock_view.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                Timber.tag(TAG).d("Pin complete: " + pin);
                if (hasInputOnce()) { // 第二次输入完成
                    boolean isSame = TextUtils.equals(pin, tempPin);
                    if (isSame) {
                        btn_ok.setEnabled(true);
                        animateToChangeTitle(R.string.prompt_enter_disguise);
                    } else {
                        animateToChangeTitle(R.string.prompt_password_is_not_consistent);
                        shakeInputBox();
                    }
                } else { // 第一次输入完成
                    tempPin = pin;
                    btn_reset.setEnabled(true);
                    animateToChangeTitle(R.string.prompt_confirm_new_password);
                    pin_lock_view.resetPinLockView();
                }
            }

            @Override
            public void onEmpty() {
                Timber.tag(TAG).d("Pin empty");
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                Timber.tag(TAG).d("Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
                if (hasInputOnce() && pinLength < pin_lock_view.getPinLength()) {
                    animateToChangeTitle(R.string.prompt_confirm_new_password);
                }
            }
        });
    }

    private boolean hasInputOnce() {
        return !TextUtils.isEmpty(tempPin);
    }

    private void setToNotShowIntroPageAnymore() {
        Keeper.get().setNeedShowIntroPage(false);
    }

    private void shakeInputBox() {
        if (shakeAnim != null) {
            shakeAnim.cancel();
            indicator_dots.setTranslationX(0);
        }
        ViewPropertyAnimator animator = indicator_dots.animate()
                .translationX(ViewUtils.dp2px(getContext(), 8f))
                .setInterpolator(new CycleInterpolator(3))
                .setDuration(400);
        animator.start();
        shakeAnim = animator;
    }

    private void animateToChangeTitle(@StringRes final int text) {
        if (TextUtils.equals(getString(text), tv_subtitle.getText().toString())) {
            return;
        }
        if (titleAnim != null) {
            titleAnim.cancel();
        }
        ObjectAnimator fadeOutAnim = ObjectAnimator.ofFloat(tv_subtitle, View.ALPHA, 0f)
                .setDuration(200);
        ObjectAnimator fadeInAnim = ObjectAnimator.ofFloat(tv_subtitle, View.ALPHA, 1f)
                .setDuration(200);
        AnimatorSet animSet = new AnimatorSet();
        animSet.playSequentially(fadeOutAnim, fadeInAnim);

        fadeOutAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                tv_subtitle.setText(text);
            }
        });
        animSet.start();
        titleAnim = animSet;
    }
}