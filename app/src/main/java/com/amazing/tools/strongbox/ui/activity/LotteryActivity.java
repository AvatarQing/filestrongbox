package com.amazing.tools.strongbox.ui.activity;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.amazing.tools.ad.AdmobLogAdListener;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.ui.widget.LotteryWheelLayout;
import com.amazing.tools.strongbox.ui.widget.RotateWheel;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.amazing.tools.strongbox.utils.Const.MAX_AWARD_COUNT;
import static com.amazing.tools.strongbox.utils.Const.MAX_AWARD_SPACE;
import static com.amazing.tools.strongbox.utils.Const.NEXT_LOTTERY_PERIOD;

/**
 * @author AvatarQing
 */
public class LotteryActivity extends BaseAppCompatActivity {

    private static final String PREF_NAME_LOTTERY = "lottery";
    private static final String PREF_KEY_LOTTERY_TIME = "lottery_time";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lottery_wheel_layout)
    LotteryWheelLayout mLotteryWheelLayout;
    @BindView(R.id.rotate_wheel)
    RotateWheel mRotateWheel;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.btn_play)
    View mBtnPlay;

    private int[] mAwards;
    private CountDownTimer mCountDownTimer;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery);
        ButterKnife.bind(this);
        removeInvalidLotteryTime();
        initToolbar();
        refreshTitle();
        resetLotteryValues();
        checkStartButtonEnabled();
        initAd();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    @OnClick(R.id.btn_play)
    void playWheel() {
        mLotteryWheelLayout.rotate(-1, 500);
        mLotteryWheelLayout.setAnimationEndListener((position, value) -> {
            int award = mAwards[position];
            EncryptHelper.getInstance().increaseMaxEncryptCount(award);
            saveNewLotteryTime();
            refreshTitle();
            checkStartButtonEnabled();
            showAwardDialog(award);
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        });
    }

    private void initAd() {
        InterstitialAd interstitialAd = mInterstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_after_lottery_ad_id));
        interstitialAd.setAdListener(new AdmobLogAdListener("InterstitialAd-Lottery") {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void initToolbar() {
        toolbar.setTitle(getTitle());
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void resetLotteryValues() {
        int lotteryCount = getResources().getInteger(R.integer.lottery_count);
        mAwards = new int[lotteryCount];
        String[] awardStrings = new String[lotteryCount];
        Random random = new Random();
        for (int i = 0; i < lotteryCount; i++) {
            mAwards[i] = random.nextInt(MAX_AWARD_SPACE);
            awardStrings[i] = mAwards[i] + "";
        }
        mRotateWheel.setStr(awardStrings);
    }

    private void checkStartButtonEnabled() {
        if (MAX_AWARD_COUNT - getTakenLotteryCount() > 0) {
            mLotteryWheelLayout.setEnabled(true);
            mRotateWheel.setEnabled(true);
            mBtnPlay.setEnabled(true);
        } else {
            mLotteryWheelLayout.setEnabled(false);
            mRotateWheel.setEnabled(false);
            mBtnPlay.setEnabled(false);
        }
    }

    private void showAwardDialog(int awardCount) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.title_congratulations)
                .setMessage(getString(R.string.format_encrypt_count_award, awardCount))
                .setPositiveButton(android.R.string.ok, null)
                .create();
        alertDialog.setOnDismissListener(dialog -> resetLotteryValues());
        alertDialog.show();
    }

    private void refreshTitle() {
        int notTakenLotteryCount = MAX_AWARD_COUNT - getTakenLotteryCount();
        if (notTakenLotteryCount > 0) {
            mTitle.setText(getString(R.string.format_rest_lottery_count, notTakenLotteryCount));
        } else {
            cancelTimer();
            long leftTime = getEarliestLotteryTime() + NEXT_LOTTERY_PERIOD - System.currentTimeMillis();
            mCountDownTimer = createCountDownTimer(leftTime);
            mCountDownTimer.onTick(leftTime);
            mCountDownTimer.start();
        }
    }

    private void removeInvalidLotteryTime() {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME_LOTTERY, MODE_PRIVATE);
        String jsonArrayString = preferences.getString(PREF_KEY_LOTTERY_TIME, "");
        if (!TextUtils.isEmpty(jsonArrayString)) {
            try {
                long now = System.currentTimeMillis();
                JSONArray newJsonArray = new JSONArray();
                JSONArray jsonArray = new JSONArray(jsonArrayString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    long time = jsonArray.getLong(i);
                    if (time + NEXT_LOTTERY_PERIOD > now) {
                        newJsonArray.put(time);
                    }
                }
                preferences.edit().putString(PREF_KEY_LOTTERY_TIME, newJsonArray.toString()).apply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private long getEarliestLotteryTime() {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME_LOTTERY, MODE_PRIVATE);
        String jsonArrayString = preferences.getString(PREF_KEY_LOTTERY_TIME, "");
        if (!TextUtils.isEmpty(jsonArrayString)) {
            try {
                JSONArray jsonArray = new JSONArray(jsonArrayString);
                if (jsonArray.length() > 0) {
                    return jsonArray.getLong(0);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private int getTakenLotteryCount() {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME_LOTTERY, MODE_PRIVATE);
        String jsonArrayString = preferences.getString(PREF_KEY_LOTTERY_TIME, "");
        if (!TextUtils.isEmpty(jsonArrayString)) {
            try {
                return new JSONArray(jsonArrayString).length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private void saveNewLotteryTime() {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME_LOTTERY, MODE_PRIVATE);
        String json = preferences.getString(PREF_KEY_LOTTERY_TIME, "");
        try {
            JSONArray jsonArray;
            if (TextUtils.isEmpty(json)) {
                jsonArray = new JSONArray();
            } else {
                jsonArray = new JSONArray(json);
            }
            jsonArray.put(System.currentTimeMillis());
            preferences.edit().putString(PREF_KEY_LOTTERY_TIME, jsonArray.toString()).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void cancelTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    private CountDownTimer createCountDownTimer(long millisInFuture) {
        return new CountDownTimer(millisInFuture, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long hour = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                long minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - hour * 60;
                long second = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - hour * 60 * 60 - minute * 60;
                String time = String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, second);
                String title = getString(R.string.format_next_lottery_time, time);
                mTitle.setText(title);
            }

            @Override
            public void onFinish() {
                removeInvalidLotteryTime();
                refreshTitle();
                checkStartButtonEnabled();
            }
        };
    }

}