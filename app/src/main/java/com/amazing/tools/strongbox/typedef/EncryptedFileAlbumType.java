package com.amazing.tools.strongbox.typedef;

import android.support.annotation.IntDef;

import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author LiuQing
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({
        EncryptedFileAlbum.TYPE_USER_CREATE,
        EncryptedFileAlbum.TYPE_MAIN,
        EncryptedFileAlbum.TYPE_RECYCLE_BIN,
        EncryptedFileAlbum.TYPE_IMPORTANT,
        EncryptedFileAlbum.TYPE_VIDEO,
})
public @interface EncryptedFileAlbumType {
}
