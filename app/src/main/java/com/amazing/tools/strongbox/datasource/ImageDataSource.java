package com.amazing.tools.strongbox.datasource;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.entity.MediaFolder;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.entityhelper.FileItemFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author LiuQing
 */
public class ImageDataSource {

    private Context mContext;

    public ImageDataSource(Context mContext) {
        this.mContext = mContext;
    }

    private Context getContext() {
        return mContext.getApplicationContext();
    }

    public List<FileItem> loadAllImages() {
        return loadAllMedias(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    }

    public List<FileItem> loadImagesInFolder(String folderPath) {
        return loadImagesInFolder(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, folderPath);
    }

    public List<FileItem> loadAllVideos() {
        return loadAllMedias(MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
    }

    public List<FileItem> loadVideosInFolder(String folderPath) {
        return loadImagesInFolder(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, folderPath);
    }

    public List<FileItem> loadAllMedias(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(
                uri,
                null,
                null,
                null,
                MediaStore.MediaColumns.DATE_MODIFIED + " DESC"
        );
        return parseImageListFromCursor(cursor);
    }

    public List<FileItem> loadImagesInFolder(Uri uri, String folderPath) {
        Cursor cursor = getContext().getContentResolver().query(
                uri,
                null,
                MediaStore.MediaColumns.DATA + " like '%" + folderPath + "%'",
                null,
                MediaStore.MediaColumns.DATE_MODIFIED + " DESC"
        );
        return parseImageListFromCursor(cursor);
    }

    private List<FileItem> parseImageListFromCursor(Cursor cursor) {
        List<FileItem> imageList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                FileItem fileItem = FileItemFactory.parseFileItemFromMediaStoreImagesCursor(cursor);
                if (fileItem != null) {
                    imageList.add(fileItem);
                }
            }
            cursor.close();
        }
        return imageList;
    }

    public List<MediaFolder> loadAllMediaFolders(@FileType int fileType) {
        int mediaType;
        switch (fileType) {
            case FileTypeUtils.TYPE_IMAGE:
                mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
                break;
            case FileTypeUtils.TYPE_VIDEO:
                mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
                break;
            case FileTypeUtils.TYPE_AUDIO:
                mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO;
                break;
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;
                break;
        }

        List<MediaFolder> folderList = new ArrayList<>();
        /* 按照ContentResolver查询风格的SQL语句，通过拼凑注入到ContentProvider中取巧组成复杂的查询语句
        SELECT COUNT(parent) AS fileCount, _data
        FROM (SELECT * FROM files WHERE (media_type = 1) ORDER BY date_modified)
        GROUP BY (parent)
        ORDER BY fileCount DESC
         */
        Cursor cursor = getContext().getContentResolver().query(
                MediaStore.Files.getContentUri("external"),
                new String[]{
                        "count(" + MediaStore.Files.FileColumns.PARENT + ") as fileCount",
                        MediaStore.Files.FileColumns.DATA + " from (select *",
                },
                MediaStore.Files.FileColumns.MEDIA_TYPE + " = " + mediaType + ")"
                        + " order by " + MediaStore.Files.FileColumns.DATE_MODIFIED + " )"
                        + " group by (" + MediaStore.Files.FileColumns.PARENT,
                null,
                "fileCount desc"
        );
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int imageFileCountInFolder = cursor.getInt(0);
                String latestImageFilePath = cursor.getString(1);
                File folderFile = new File(latestImageFilePath).getParentFile();

                MediaFolder folder = new MediaFolder();
                folder.name = folderFile.getName();
                folder.path = folderFile.getAbsolutePath();
                folder.cover = latestImageFilePath;
                folder.fileCount = imageFileCountInFolder;
                folderList.add(folder);
            }
            cursor.close();
        }
        Collections.sort(folderList);
        return folderList;
    }

}