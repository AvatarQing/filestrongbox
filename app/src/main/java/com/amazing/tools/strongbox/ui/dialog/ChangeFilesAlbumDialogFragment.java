package com.amazing.tools.strongbox.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.ui.adatpter.AlbumListAdapter;
import com.amazing.tools.strongbox.utils.DialogUtils;
import com.trello.rxlifecycle2.components.support.RxAppCompatDialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * @author AvatarQing
 */

public class ChangeFilesAlbumDialogFragment extends RxAppCompatDialogFragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private AlbumListAdapter mAdapter;
    private EncryptedFileAlbum mCurrentAlbum;
    private OnChangeFilesAlbumListener mOnChangeFilesAlbumListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        ViewGroup parentView = dialog.getWindow() != null ? (ViewGroup) dialog.getWindow().getDecorView() : null;
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_change_album, parentView, false);
        dialog.setContentView(contentView);
        initViews(contentView);
        DialogUtils.configDialogSize(dialog, 9f / 10f);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadAlbums();
    }

    private void initViews(View contentView) {
        ButterKnife.bind(this, contentView);

        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(lm);

        mAdapter = new AlbumListAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (mOnChangeFilesAlbumListener != null) {
                mOnChangeFilesAlbumListener.onSelectedOneAlbum(this, mAdapter.getItem(position));
            }
        });
    }

    private void loadAlbums() {
        EncryptedFilesDao.loadAllAlbums()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .filter(album -> !album.equals(mCurrentAlbum) && album.type != EncryptedFileAlbum.TYPE_RECYCLE_BIN)
                .toList()
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<EncryptedFileAlbum>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(List<EncryptedFileAlbum> albumList) {
                        mAdapter.setNewData(albumList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    @OnClick(R.id.btn_add_album)
    void createAlbumThenMoveFilesToIt() {
        if (mOnChangeFilesAlbumListener != null) {
            mOnChangeFilesAlbumListener.onCreateNewAlbum(this);
        }
    }

    public static class Builder {
        private EncryptedFileAlbum currentAlbum;
        private OnChangeFilesAlbumListener onChangeFilesAlbumListener;

        public Builder setOnChangeFilesAlbumListener(OnChangeFilesAlbumListener onChangeFilesAlbumListener) {
            this.onChangeFilesAlbumListener = onChangeFilesAlbumListener;
            return this;
        }

        public Builder setCurrentAlbum(EncryptedFileAlbum currentAlbum) {
            this.currentAlbum = currentAlbum;
            return this;
        }

        public ChangeFilesAlbumDialogFragment create() {
            ChangeFilesAlbumDialogFragment dialogFragment = new ChangeFilesAlbumDialogFragment();
            dialogFragment.mCurrentAlbum = currentAlbum;
            dialogFragment.mOnChangeFilesAlbumListener = onChangeFilesAlbumListener;
            return dialogFragment;
        }
    }

    public interface OnChangeFilesAlbumListener {
        void onCreateNewAlbum(ChangeFilesAlbumDialogFragment dialogFragment);

        void onSelectedOneAlbum(ChangeFilesAlbumDialogFragment dialogFragment, EncryptedFileAlbum album);
    }
}