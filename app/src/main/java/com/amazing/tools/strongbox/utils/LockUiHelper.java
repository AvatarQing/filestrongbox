package com.amazing.tools.strongbox.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.amazing.tools.strongbox.disguise.protocol.DisguiseManager;
import com.amazing.tools.strongbox.ui.activity.ReceiveFileToEncryptActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AvatarQing
 */
public class LockUiHelper implements Application.ActivityLifecycleCallbacks {

    private static volatile LockUiHelper sInstance;

    public static LockUiHelper getInstance() {
        if (sInstance == null) {
            synchronized (LockUiHelper.class) {
                if (sInstance == null) {
                    sInstance = new LockUiHelper();
                }
            }
        }
        return sInstance;
    }

    private int mVisibleNeedLockActivityCount = 0;
    private List<String> mNoNeedLockActivityList = new ArrayList<>();

    private LockUiHelper() {
        mNoNeedLockActivityList.add(ReceiveFileToEncryptActivity.class.getName());
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (needLock(activity)) {
            if (LockPinHelper.get().hasSetPin() && !hasAnyNeedLockVisibleActivity()) {
                DisguiseManager.get().startDisguiseActivity(activity, true);
            }
            mVisibleNeedLockActivityCount++;
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (needLock(activity)) {
            mVisibleNeedLockActivityCount--;
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    private boolean hasAnyNeedLockVisibleActivity() {
        return mVisibleNeedLockActivityCount > 0;
    }

    private boolean needLock(Activity activity) {
        return !mNoNeedLockActivityList.contains(activity.getClass().getName());
    }

}