package com.amazing.tools.strongbox.db;

import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Author: LiuQing
 * Date: 2017/7/11
 */
@Database(
        version = EncryptedFilesDatabase.VERSION,
        insertConflict = ConflictAction.IGNORE,
        updateConflict = ConflictAction.REPLACE
)
public class EncryptedFilesDatabase {
    public static final String NAME = "EncryptedFilesInfo";

    static final int VERSION = 1;
}
