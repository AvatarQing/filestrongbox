package com.amazing.tools.strongbox.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.datasource.ImageDataSource;
import com.amazing.tools.strongbox.entity.MediaFolder;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.ui.adatpter.ImageFolderListAdapter;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author LiuQing
 */
public class SelectMediaFolderFragment extends BaseFragment {
    public static final String TAG = "SelectMediaFolderFragment";
    private static final String ARG_MEDIA_TYPE = "ARG_MEDIA_TYPE";

    private RecyclerView mRecyclerView;

    private ImageFolderListAdapter mAdapter;

    private ImageDataSource mImageDataSource;

    private OnFolderSelectListener mOnFolderSelectListener;

    @FileType
    private int mMediaType;

    public static SelectMediaFolderFragment newInstance(@FileType int mediaType) {
        Bundle args = new Bundle();
        args.putInt(ARG_MEDIA_TYPE, mediaType);

        SelectMediaFolderFragment fragment = new SelectMediaFolderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parseArguments();
        if (context instanceof OnFolderSelectListener) {
            mOnFolderSelectListener = (OnFolderSelectListener) context;
        }
        mImageDataSource = new ImageDataSource(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRecyclerView = new RecyclerView(getContext());
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void parseArguments() {
        if (getArguments() == null) {
            return;
        }
        mMediaType = getArguments().getInt(ARG_MEDIA_TYPE, FileTypeUtils.TYPE_IMAGE);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new ImageFolderListAdapter();
        mAdapter.bindToRecyclerView(mRecyclerView);

        mAdapter.setEmptyView(R.layout.list_empty_view);
        TextView tvEmptyText = mAdapter.getEmptyView().findViewById(R.id.empty_text);
        tvEmptyText.setText(getEmptyStringResId());

        mAdapter.setOnItemClickListener(mOnItemClickListener);
    }

    @StringRes
    private int getEmptyStringResId() {
        switch (mMediaType) {
            case FileTypeUtils.TYPE_VIDEO:
                return R.string.prompt_no_videos;
            case FileTypeUtils.TYPE_IMAGE:
            case FileTypeUtils.TYPE_AUDIO:
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                return R.string.prompt_no_images;
        }
    }

    private void loadData() {
        Observable
                .fromCallable(this::getFolders)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MediaFolder>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull List<MediaFolder> mediaFolders) {
                        mAdapter.setNewData(mediaFolders);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private List<MediaFolder> getFolders() {
        return mImageDataSource.loadAllMediaFolders(mMediaType);
    }

    private BaseQuickAdapter.OnItemClickListener mOnItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (mOnFolderSelectListener != null) {
                MediaFolder folderItem = mAdapter.getItem(position);
                mOnFolderSelectListener.onFolderSelected(folderItem);
            }
        }
    };

    public interface OnFolderSelectListener {
        void onFolderSelected(MediaFolder folder);
    }
}