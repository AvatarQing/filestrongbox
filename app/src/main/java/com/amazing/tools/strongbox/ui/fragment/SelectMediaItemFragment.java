package com.amazing.tools.strongbox.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.dataholder.FileListDataHolder;
import com.amazing.tools.strongbox.datasource.ImageDataSource;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.ui.activity.BaseAppCompatActivity;
import com.amazing.tools.strongbox.ui.activity.MediaPreviewActivity;
import com.amazing.tools.strongbox.ui.adatpter.MediaItemListAdapter;
import com.amazing.tools.strongbox.ui.decoration.SpacingDecoration;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.utils.ColorUtils;
import com.amazing.tools.strongbox.utils.FeatureDiscoveryUtils;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.utility.multichoice.ChoiceModeHelper;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author LiuQing
 */
public class SelectMediaItemFragment extends BaseFragment {
    public static final String TAG = SelectMediaItemFragment.class.getSimpleName();
    private static final String ARG_FOLDER_PATH = "ARG_FOLDER_PATH";
    private static final String ARG_ALBUM = "ARG_ALBUM";
    private static final String ARG_MEDIA_TYPE = "ARG_MEDIA_TYPE";

    private RecyclerView mRecyclerView;
    private MediaItemListAdapter mAdapter;
    private String mFolderPath;
    private ImageDataSource mImageDataSource;
    private EncryptedFileUIHelper mEncryptedFileUIHelper;
    private ChoiceModeHelper mChoiceModeHelper;
    private EncryptedFileAlbum mAlbum;
    @FileType
    private int mMediaType;

    public static SelectMediaItemFragment newInstance(String folderPath, EncryptedFileAlbum album, @FileType int mediaType) {
        Bundle args = new Bundle();
        args.putString(ARG_FOLDER_PATH, folderPath);
        args.putParcelable(ARG_ALBUM, album);
        args.putInt(ARG_MEDIA_TYPE, mediaType);

        SelectMediaItemFragment fragment = new SelectMediaItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mImageDataSource = new ImageDataSource(context);
        mEncryptedFileUIHelper = new EncryptedFileUIHelper(getActivity(), (LifecycleProvider<ActivityEvent>) getActivity());
        mEncryptedFileUIHelper.addOnEncryptCompleteDialogDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mChoiceModeHelper.finishActionMode();
                getActivity().finish();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRecyclerView = new RecyclerView(getContext());
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEncryptedFileUIHelper.removeAllListeners();
    }

    private void parseArguments() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            return;
        }
        mFolderPath = arguments.getString(ARG_FOLDER_PATH);
        mAlbum = arguments.getParcelable(ARG_ALBUM);
        mMediaType = getArguments().getInt(ARG_MEDIA_TYPE, FileTypeUtils.TYPE_IMAGE);
    }

    private void initRecyclerView() {
        GridLayoutManager lm = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.image_grid_column));
        mRecyclerView.setLayoutManager(lm);

        int itemSpacing = getResources().getDimensionPixelOffset(R.dimen.item_spacing);
        SpacingDecoration spacingDecoration = new SpacingDecoration(itemSpacing, itemSpacing, false);
        mRecyclerView.addItemDecoration(spacingDecoration);

        mAdapter = new MediaItemListAdapter(mMediaType);
        mAdapter.bindToRecyclerView(mRecyclerView);

        mAdapter.setEmptyView(R.layout.list_empty_view);
        TextView tvEmptyText = mAdapter.getEmptyView().findViewById(R.id.empty_text);
        tvEmptyText.setText(getEmptyStringResId());

        mAdapter.setOnItemClickListener(mOnItemClickListener);
        mAdapter.setOnItemLongClickListener(mOnItemLongClickListener);

        mChoiceModeHelper = new ChoiceModeHelper(mRecyclerView);
        mChoiceModeHelper.setChoiceMode(ChoiceModeHelper.CHOICE_MODE_MULTIPLE_MODAL);
        mChoiceModeHelper.setMultiChoiceModeListener(mMultiChoiceModeListener);
        mChoiceModeHelper.setUpdateOnScreenCheckedViewsCallback(mUpdateOnScreenCheckedViewsCallback);
        mAdapter.setChoiceModeHelper(mChoiceModeHelper);
    }

    @StringRes
    private int getEmptyStringResId() {
        switch (mMediaType) {
            case FileTypeUtils.TYPE_VIDEO:
                return R.string.prompt_no_videos;
            case FileTypeUtils.TYPE_IMAGE:
            case FileTypeUtils.TYPE_AUDIO:
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                return R.string.prompt_no_images;
        }
    }

    private void loadData() {
        Observable
                .fromCallable(this::getFileList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<FileItem>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull List<FileItem> fileItems) {
                        mAdapter.setNewData(fileItems);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        delayShowSelectImageFeatureDiscovery();
                    }
                });
    }

    private List<FileItem> getFileList() {
        if (TextUtils.isEmpty(mFolderPath)) {
            switch (mMediaType) {
                case FileTypeUtils.TYPE_VIDEO:
                    return mImageDataSource.loadAllVideos();
                case FileTypeUtils.TYPE_IMAGE:
                case FileTypeUtils.TYPE_AUDIO:
                case FileTypeUtils.TYPE_FILE:
                case FileTypeUtils.TYPE_TEXT:
                default:
                    return mImageDataSource.loadAllImages();
            }
        } else {
            switch (mMediaType) {
                case FileTypeUtils.TYPE_VIDEO:
                    return mImageDataSource.loadVideosInFolder(mFolderPath);
                case FileTypeUtils.TYPE_IMAGE:
                case FileTypeUtils.TYPE_AUDIO:
                case FileTypeUtils.TYPE_FILE:
                case FileTypeUtils.TYPE_TEXT:
                default:
                    return mImageDataSource.loadImagesInFolder(mFolderPath);
            }
        }
    }

    private void delayShowSelectImageFeatureDiscovery() {
        if (FeatureDiscoveryUtils.hasShownSelectImageFromGallery()) {
            return;
        }
        Observable.timer(800, TimeUnit.MILLISECONDS)
                .compose(bindUntilEvent(FragmentEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        showSelectImageFeatureDiscovery();
                    }
                });
    }

    private void showSelectImageFeatureDiscovery() {
        if (mAdapter.getData().isEmpty() || getActivity() == null) {
            return;
        }
        new TapTargetSequence(getActivity())
                .target(
                        TapTarget.forView(mRecyclerView.getChildAt(0), getString(R.string.title_multiple_select), getString(R.string.prompt_multiple_select_from_gallery))
                                .outerCircleColorInt(ColorUtils.getColorFromAttr(getContext(), R.attr.colorPrimary))
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white100)
                                .textColor(R.color.white100)
                                .dimColor(R.color.black100)
                                .drawShadow(true)
                                .tintTarget(false)
                )
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        FeatureDiscoveryUtils.setShownSelectImageFromGallery(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget tapTarget, boolean b) {
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget tapTarget) {
                    }
                })
                .start();
    }

    private ArrayList<FileItem> getSelectedImageList() {
        ArrayList<FileItem> selectedFileList = new ArrayList<>();
        SparseBooleanArray checkedItemPositions = mChoiceModeHelper.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            int positionKey = checkedItemPositions.keyAt(i);
            if (checkedItemPositions.get(positionKey, false)) {
                FileItem item = mAdapter.getItem(positionKey);
                if (item != null) {
                    selectedFileList.add(item);
                }
            }
        }
        return selectedFileList;
    }

    private BaseQuickAdapter.OnItemClickListener mOnItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (!mChoiceModeHelper.performItemClick(view, position)) {
                FileListDataHolder.setData(mAdapter.getData());
                MediaPreviewActivity.launch(getActivity(), position, false);
            }
        }
    };

    private BaseQuickAdapter.OnItemLongClickListener mOnItemLongClickListener = new BaseQuickAdapter.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
            return mChoiceModeHelper.performItemLongPress(view, position);
        }
    };

    private ChoiceModeHelper.MultiChoiceModeListener mMultiChoiceModeListener = new ChoiceModeHelper.MultiChoiceModeListener() {
        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
            ((BaseAppCompatActivity) getActivity()).changeStatusBarColorGradually(ContextCompat.getColor(getContext(), R.color.colorAccentDark));
            mode.getMenuInflater().inflate(R.menu.action_mode_select_files, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.confirm: {
                    mEncryptedFileUIHelper.showEncryptConfirmDialog(getSelectedImageList(), mAlbum);
                }
                return true;
                case R.id.select_all: {
                    if (mChoiceModeHelper.hasSelectedAll()) {
                        mChoiceModeHelper.deselectAll();
                        item.setTitle(R.string.menu_select_all);
                    } else {
                        mChoiceModeHelper.selectAll();
                        item.setTitle(R.string.menu_deselect_all);
                    }
                }
                return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            ((BaseAppCompatActivity) getActivity()).changeStatusBarColorGradually(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            updateSelectedItemsUI(mode, mChoiceModeHelper.getCheckedItemCount());
        }

        private void updateSelectedItemsUI(ActionMode actionMode, int selectedCount) {
            if (actionMode == null) {
                return;
            }
            String title = getString(R.string.format_items_selected, selectedCount);
            actionMode.setTitle(title);
        }
    };

    private ChoiceModeHelper.UpdateOnScreenCheckedViewsCallback mUpdateOnScreenCheckedViewsCallback = new ChoiceModeHelper.UpdateOnScreenCheckedViewsCallback() {
        @Override
        public void onItemUpdate(int position, boolean checked) {
            mAdapter.startCheckedAnimation(position, checked);
        }
    };

}