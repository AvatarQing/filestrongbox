package com.amazing.tools.strongbox.utils.imageloader.glide;

import android.support.annotation.Nullable;

import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.signature.ObjectKey;

import java.io.InputStream;

/**
 * @author AvatarQing
 */
public class ConcealImageLoader implements ModelLoader<String, InputStream> {

    private String mEncryptedImageDirPath = "";
    private String mEncryptedThumbnailDirPath = "";

    public ConcealImageLoader() {
        mEncryptedImageDirPath = EncryptHelper.getEncryptedFilesSaveDir(FileTypeUtils.TYPE_IMAGE).getAbsolutePath();
        mEncryptedThumbnailDirPath = EncryptHelper.getVideoThumbnailSaveDir().getAbsolutePath();
    }

    @Nullable
    @Override
    public LoadData<InputStream> buildLoadData(String model, int width, int height, Options options) {
        return new LoadData<>(new ObjectKey(model), new ConcealImageDataFetcher(model));
    }

    @Override
    public boolean handles(String model) {
        return model.startsWith(mEncryptedImageDirPath)
                || model.startsWith(mEncryptedThumbnailDirPath);
    }

    public static class Factory implements ModelLoaderFactory<String, InputStream> {

        @Override
        public ModelLoader<String, InputStream> build(MultiModelLoaderFactory multiFactory) {
            return new ConcealImageLoader();
        }

        @Override
        public void teardown() {
        }
    }

}
