package com.amazing.tools.strongbox.db;

import android.content.Context;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.AlbumQueryModel;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum_Table;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo_Table;
import com.amazing.tools.strongbox.exception.AlbumAlreadyExistedException;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.rx2.language.RXSQLite;
import com.raizlabs.android.dbflow.sql.language.Join;
import com.raizlabs.android.dbflow.sql.language.Method;
import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.property.IProperty;
import com.raizlabs.android.dbflow.sql.language.property.PropertyFactory;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * @author LiuQing
 */
public class EncryptedFilesDao {

    private static final String TAG = EncryptedFilesDao.class.getSimpleName();

    public static void insertDefaultAlbumsIfNeed(Context context) {
        final Context appContext = context.getApplicationContext();
        RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileAlbum.class)
                                .where(EncryptedFileAlbum_Table.type.in(
                                        EncryptedFileAlbum.TYPE_MAIN,
                                        EncryptedFileAlbum.TYPE_IMPORTANT,
                                        EncryptedFileAlbum.TYPE_VIDEO,
                                        EncryptedFileAlbum.TYPE_RECYCLE_BIN
                                ))
                )
                .queryList()
                .subscribeOn(Schedulers.io())
                .flatMap(new Function<List<EncryptedFileAlbum>, SingleSource<Boolean>>() {
                    @Override
                    public SingleSource<Boolean> apply(@NonNull List<EncryptedFileAlbum> predefinedAlbums) throws Exception {
                        if (predefinedAlbums.isEmpty()) {
                            final List<EncryptedFileAlbum> folderList = new ArrayList<>();
                            // 主相册
                            folderList.add(EncryptedFileAlbum.create(appContext.getString(R.string.title_main_album), EncryptedFileAlbum.TYPE_MAIN, System.currentTimeMillis()));
                            // 重要资料
                            folderList.add(EncryptedFileAlbum.create(appContext.getString(R.string.title_important_files), EncryptedFileAlbum.TYPE_IMPORTANT, System.currentTimeMillis()));
                            // 视频
                            folderList.add(EncryptedFileAlbum.create(appContext.getString(R.string.title_videos), EncryptedFileAlbum.TYPE_VIDEO, System.currentTimeMillis()));
                            // 回收站
                            folderList.add(EncryptedFileAlbum.create(appContext.getString(R.string.title_recycle_bin), EncryptedFileAlbum.TYPE_RECYCLE_BIN, System.currentTimeMillis()));

                            FlowManager.getDatabaseForTable(EncryptedFileAlbum.class)
                                    .executeTransaction(new ITransaction() {
                                        @Override
                                        public void execute(DatabaseWrapper databaseWrapper) {
                                            FlowManager.getModelAdapter(EncryptedFileAlbum.class)
                                                    .insertAll(folderList);
                                        }
                                    });
                            return Single.just(true);
                        } else {
                            return Single.just(false);
                        }
                    }
                })
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull Boolean hasNewlyCreatedDefaultFolders) {
                        if (hasNewlyCreatedDefaultFolders) {
                            Timber.tag(TAG).d("Newly created default albums");
                        } else {
                            Timber.tag(TAG).d("Already created default albums");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public static Single<List<EncryptedFileAlbum>> loadAllAlbums() {
        IProperty[] fileTableQueryColumns = new IProperty[1 + EncryptedFileInfo_Table.ALL_COLUMN_PROPERTIES.length];
        for (int i = 0; i < EncryptedFileInfo_Table.ALL_COLUMN_PROPERTIES.length; i++) {
            IProperty property = EncryptedFileInfo_Table.ALL_COLUMN_PROPERTIES[i];
            fileTableQueryColumns[i] = property.as("file_" + property.getNameAlias());
        }
        fileTableQueryColumns[EncryptedFileInfo_Table.ALL_COLUMN_PROPERTIES.length] = Method.count(EncryptedFileInfo_Table.albumId).as("fileCountInAlbum");

        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileAlbum.class).as("a")
                                .join(
                                        SQLite.select(fileTableQueryColumns)
                                                .from(EncryptedFileInfo.class)
                                                .groupBy(EncryptedFileInfo_Table.albumId),
                                        Join.JoinType.LEFT_OUTER
                                ).as("b")
                                .on(
                                        EncryptedFileAlbum_Table.id.withTable(NameAlias.builder("a").build())
                                                .eq(PropertyFactory.from("file_" + EncryptedFileInfo_Table.albumId.getNameAlias().nameRaw())
                                                        .withTable(NameAlias.builder("b").build()))

                                )
                                .orderBy(EncryptedFileAlbum_Table.type.getNameAlias(), true)
                                .orderBy(EncryptedFileAlbum_Table.createdTime.getNameAlias(), true)
                )
                .queryCustomList(AlbumQueryModel.class)
                .map(queryModelList -> {
                    List<EncryptedFileAlbum> albumList = new ArrayList<>();
                    for (AlbumQueryModel model : queryModelList) {
                        EncryptedFileAlbum album = EncryptedFileAlbum.create(model.name, model.type, model.createdTime);
                        album.id = model.id;
                        album.fileCount = model.fileCountInAlbum;

                        if (model.file_id > 0) {
                            EncryptedFileInfo fileInfo = new EncryptedFileInfo();
                            fileInfo.id = model.file_id;
                            fileInfo.albumId = model.id;
                            fileInfo.encryptedFileName = model.file_encryptedFileName;
                            fileInfo.encryptedFilePath = model.file_encryptedFilePath;
                            fileInfo.originalFileName = model.file_originalFileName;
                            fileInfo.originalFilePath = model.file_originalFilePath;
                            fileInfo.thumbnailFilePath = model.file_thumbnailFilePath;
                            fileInfo.fileSize = model.file_fileSize;
                            fileInfo.fileType = model.file_fileType;
                            fileInfo.modifiedTime = model.file_modifiedTime;
                            fileInfo.encryptedTime = model.file_encryptedTime;
                            fileInfo.mimeType = model.file_mimeType;
                            album.latestFileInfo = fileInfo;
                        }

                        albumList.add(album);
                    }
                    return albumList;
                })
                .subscribeOn(Schedulers.io());
    }

    public static EncryptedFileAlbum loadMainAlbum() {
        return SQLite.select()
                .from(EncryptedFileAlbum.class)
                .where(EncryptedFileAlbum_Table.type.eq(EncryptedFileAlbum.TYPE_MAIN))
                .querySingle();
    }

    public static EncryptedFileAlbum loadVideoAlbum() {
        return SQLite.select()
                .from(EncryptedFileAlbum.class)
                .where(EncryptedFileAlbum_Table.type.eq(EncryptedFileAlbum.TYPE_VIDEO))
                .querySingle();
    }

    public static EncryptedFileAlbum loadRecycleBinAlbum() {
        return SQLite.select()
                .from(EncryptedFileAlbum.class)
                .where(EncryptedFileAlbum_Table.type.eq(EncryptedFileAlbum.TYPE_RECYCLE_BIN))
                .querySingle();
    }

    /**
     * 创建相册，如果相册已存在则不创建
     *
     * @param name 相册名称
     * @return true表示创建成功，false表示失败
     */
    public static Single<EncryptedFileAlbum> createAlbumInRx(final String name) {
        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileAlbum.class)
                                .where(EncryptedFileAlbum_Table.name.eq(name))
                )
                .count()
                .map(new Function<Long, EncryptedFileAlbum>() {
                    @Override
                    public EncryptedFileAlbum apply(@NonNull Long count) throws Exception {
                        if (count > 0) {
                            throw new AlbumAlreadyExistedException(name);
                        }
                        EncryptedFileAlbum album = EncryptedFileAlbum.create(name, EncryptedFileAlbum.TYPE_USER_CREATE, System.currentTimeMillis());
                        long id = FlowManager.getModelAdapter(EncryptedFileAlbum.class)
                                .insert(album);
                        if (id < 0) {
                            throw new IllegalStateException("Insert new album \"" + name + "\" failed");
                        }
                        return album;
                    }
                })
                .subscribeOn(Schedulers.io());
    }

    public static Single<EncryptedFileAlbum> renameAlbumInRx(final EncryptedFileAlbum album, final String newName) {
        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileAlbum.class)
                                .where(EncryptedFileAlbum_Table.name.eq(newName))
                )
                .count()
                .map(new Function<Long, EncryptedFileAlbum>() {
                    @Override
                    public EncryptedFileAlbum apply(@NonNull Long count) throws Exception {
                        if (count > 0) {
                            throw new AlbumAlreadyExistedException(newName);
                        }
                        album.name = newName;
                        FlowManager.getModelAdapter(EncryptedFileAlbum.class)
                                .update(album);
                        return album;
                    }
                })
                .subscribeOn(Schedulers.io());
    }

    public static Single<Boolean> deleteAlbumAndMoveFilesToRecycleBinInRx(final EncryptedFileAlbum album) {
        return Observable
                .fromCallable(new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return deleteAlbumAndMoveFilesToRecycleBin(album);
                    }
                })
                .subscribeOn(Schedulers.io())
                .singleOrError();
    }

    public static boolean deleteAlbumAndMoveFilesToRecycleBin(final EncryptedFileAlbum album) {
        if (album.id <= 0) {
            Timber.tag(TAG).w("Cannot delete an invalid album");
            return false;
        }
        FlowManager.getDatabaseForTable(EncryptedFileAlbum.class)
                .executeTransaction(new ITransaction() {
                    @Override
                    public void execute(DatabaseWrapper databaseWrapper) {
                        transferFilesInAlbumToRecycleBin(album.id);
                        FlowManager.getModelAdapter(EncryptedFileAlbum.class)
                                .delete(album);
                    }
                });
        return true;
    }

    private static void transferFilesInAlbumToRecycleBin(long albumId) {
        EncryptedFileAlbum recycleBinAlbum = loadRecycleBinAlbum();
        if (recycleBinAlbum == null) {
            throw new IllegalStateException("Recycle Bin album is missing");
        }
        final List<EncryptedFileInfo> fileList = SQLite.select()
                .from(EncryptedFileInfo.class)
                .where(EncryptedFileInfo_Table.albumId.eq(albumId))
                .queryList();
        for (EncryptedFileInfo fileInfo : fileList) {
            fileInfo.albumId = recycleBinAlbum.id;
        }
        FlowManager.getModelAdapter(EncryptedFileInfo.class)
                .updateAll(fileList);
        Timber.tag(TAG).d("Transfer " + fileList.size() + " files to recycle bin");
    }

    /**
     * 把给定的加密文件都移动到回收站
     *
     * @param fileList 给定的加密文件列表
     */
    public static void moveFilesToRecycleBin(List<EncryptedFileInfo> fileList) {
        EncryptedFileAlbum recycleBinAlbum = loadRecycleBinAlbum();
        if (recycleBinAlbum == null) {
            throw new IllegalStateException("Recycle Bin album is missing");
        }
        for (EncryptedFileInfo fileInfo : fileList) {
            fileInfo.albumId = recycleBinAlbum.id;
        }
        FlowManager.getDatabaseForTable(EncryptedFileAlbum.class)
                .executeTransaction(databaseWrapper -> FlowManager.getModelAdapter(EncryptedFileInfo.class).updateAll(fileList));
        Timber.tag(TAG).d("Transfer " + fileList.size() + " files to recycle bin");
    }

    public static Single<Boolean> changeFilesAlbumInRx(List<EncryptedFileInfo> fileList, long albumId) {
        return Observable.fromIterable(fileList)
                .doOnNext(fileInfo -> fileInfo.albumId = albumId)
                .toList()
                .map(fileInfoList -> {
                    FlowManager.getDatabaseForTable(EncryptedFileAlbum.class)
                            .executeTransaction(databaseWrapper -> FlowManager.getModelAdapter(EncryptedFileInfo.class).updateAll(fileInfoList));
                    return true;
                })
                .onErrorReturn(throwable -> {
                    throwable.printStackTrace();
                    return false;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> BatchFlowContentObserver.getInstance().beginTransaction())
                .doFinally(() -> BatchFlowContentObserver.getInstance().endTransactionAndNotify());
    }

    public static Single<List<EncryptedFileInfo>> loadAllFiles() {
        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileInfo.class)
                                .orderBy(EncryptedFileInfo_Table.encryptedTime.getNameAlias(), false)
                )
                .queryList()
                .subscribeOn(Schedulers.io());
    }

    public static Single<List<EncryptedFileInfo>> loadFilesInAlbum(long albumId) {
        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileInfo.class)
                                .where(EncryptedFileInfo_Table.albumId.eq(albumId))
                                .orderBy(EncryptedFileInfo_Table.encryptedTime.getNameAlias(), false)
                )
                .queryList()
                .subscribeOn(Schedulers.io());
    }

    public static Maybe<EncryptedFileInfo> loadLatestFileInAlbum(long albumId) {
        return RXSQLite
                .rx(
                        SQLite.select()
                                .from(EncryptedFileInfo.class)
                                .where(EncryptedFileInfo_Table.albumId.eq(albumId))
                                .orderBy(EncryptedFileInfo_Table.encryptedTime.getNameAlias(), false)
                                .limit(1)
                )
                .querySingle()
                .subscribeOn(Schedulers.io());
    }

    public static long insertFile(EncryptedFileInfo fileInfo) {
        return FlowManager.getModelAdapter(EncryptedFileInfo.class).insert(fileInfo);
    }

    public static boolean deleteFile(EncryptedFileInfo fileInfo) {
        return FlowManager.getModelAdapter(EncryptedFileInfo.class).delete(fileInfo);
    }

    public static long queryFileCount() {
        return SQLite.selectCountOf().from(EncryptedFileInfo.class).count();
    }

}