package com.amazing.tools.strongbox.dataholder;

import com.amazing.tools.strongbox.entity.FileItem;

import java.util.List;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/24 </li>
 * </ul>
 */

public enum FileListDataHolder {
    INSTANCE;

    private List<FileItem> mDataList;

    public static boolean hasData() {
        return INSTANCE.mDataList != null;
    }

    public static void setData(final List<FileItem> objectList) {
        INSTANCE.mDataList = objectList;
    }

    public static List<FileItem> getData() {
        final List<FileItem> retList = INSTANCE.mDataList;
        INSTANCE.mDataList = null;
        return retList;
    }
}