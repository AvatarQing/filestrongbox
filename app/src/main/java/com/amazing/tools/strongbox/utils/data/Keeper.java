package com.amazing.tools.strongbox.utils.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.amazing.tools.strongbox.utils.Const;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/7/30 </li>
 * </ul>
 */

public class Keeper {

    private static final String PREF_NAME_SECURITY = "PREF_NAME_SECURITY";
    private static final String PREF_NAME_FEATURE_DISCOVERY = "PREF_NAME_FEATURE_DISCOVERY";
    private static final String PREF_KEY_PIN = "PREF_KEY_PIN";
    private static final String PREF_KEY_SHOW_INTRO = "PREF_KEY_SHOW_INTRO";
    private static final String PREF_KEY_MAX_ENCRYPT_COUNT = "PREF_KEY_MAX_ENCRYPT_COUNT";

    private static volatile Keeper sInstance;
    private SharedPreferences mPrefs;
    private SharedPreferences mFDPrefs;

    public static Keeper get() {
        if (sInstance == null) {
            synchronized (Keeper.class) {
                if (sInstance == null) {
                    sInstance = new Keeper();
                }
            }
        }
        return sInstance;
    }

    private Keeper() {
    }

    public void init(Context context) {
        mPrefs = context.getSharedPreferences(PREF_NAME_SECURITY, Context.MODE_PRIVATE);
        mFDPrefs = context.getSharedPreferences(PREF_NAME_FEATURE_DISCOVERY, Context.MODE_PRIVATE);
    }

    private SharedPreferences getPrefs() {
        return mPrefs;
    }

    private SharedPreferences getFeatureDiscoveryPrefs() {
        return mFDPrefs;
    }

    public void savePin(String pin) {
        getPrefs().edit().putString(PREF_KEY_PIN, pin).apply();
    }

    public String getPin() {
        return getPrefs().getString(PREF_KEY_PIN, "");
    }

    public void setMaxEncryptCount(int count) {
        getPrefs().edit().putInt(PREF_KEY_MAX_ENCRYPT_COUNT, count).apply();
    }

    public int getMaxEncryptCount() {
        return getPrefs().getInt(PREF_KEY_MAX_ENCRYPT_COUNT, Const.DEFAULT_MAX_ENCRYPT_COUNT);
    }

    public boolean needShowIntroPage() {
        return getPrefs().getBoolean(PREF_KEY_SHOW_INTRO, true);
    }

    public void setNeedShowIntroPage(boolean shown) {
        getPrefs().edit().putBoolean(PREF_KEY_SHOW_INTRO, shown).apply();
    }

    public boolean hasShownFeatureDiscovery(String key) {
        return getFeatureDiscoveryPrefs().getBoolean(key, false);
    }

    public void setShownFeatureDiscovery(String key, boolean shown) {
        getFeatureDiscoveryPrefs().edit().putBoolean(key, shown).apply();
    }

}