package com.amazing.tools.strongbox.ui.dialog;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.ui.adatpter.RawFileListAdapter;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Author: LiuQing
 * Date: 2017/7/10
 */

public class EncryptFilesDialogFragment extends BaseDialogFragment {

    @BindView(R.id.tv_total_file_size)
    TextView tv_total_file_size;
    @BindView(R.id.tv_file_size_unit)
    TextView tv_file_size_unit;
    @BindView(R.id.tv_ready_encrypt_images)
    TextView tv_ready_encrypt_images;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.file_size_summary_header)
    View file_size_summary_header;
    @BindView(R.id.header)
    View header;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private ArrayList<FileItem> mSelectedFileList = new ArrayList<>();
    private OnEncryptButtonClickListener mOnEncryptButtonClickListener;

    private void initFromBuilder(Builder builder) {
        mSelectedFileList = builder.selectedFileList;
        mOnEncryptButtonClickListener = builder.onEncryptButtonClickListener;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        startEnterAnimations();
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.dialog_encrypt_image, parentView, false);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);

        long totalFileSize = 0;
        for (int i = 0; i < mSelectedFileList.size(); i++) {
            FileItem item = mSelectedFileList.get(i);
            totalFileSize += item.size;
        }
        String totalFileSizeFormatText = Formatter.formatFileSize(getContext(), totalFileSize);
        String[] splitText = totalFileSizeFormatText.split(" ");
        tv_total_file_size.setText(splitText[0]);
        tv_file_size_unit.setText(splitText[1]);

        String imageCountString = getResources().getQuantityString(R.plurals.format_images_ready_to_encrypt, mSelectedFileList.size(), mSelectedFileList.size());
        tv_ready_encrypt_images.setText(imageCountString);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(layoutManager);

        RawFileListAdapter adapter = new RawFileListAdapter();
        recycler_view.setAdapter(adapter);
        adapter.setNewData(mSelectedFileList);
        addPlaceHolderFooterToRecyclerView(adapter);

        resetRecyclerViewHeight();
        resetViewProperties();
    }

    private void addPlaceHolderFooterToRecyclerView(RawFileListAdapter adapter) {
        View emptyFooterView = new View(getContext());
        int itemHeight = getResources().getDimensionPixelSize(R.dimen.item_height);
        LinearLayout.LayoutParams footerViewLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, itemHeight);
        emptyFooterView.setLayoutParams(footerViewLp);
        adapter.setFooterView(emptyFooterView);
    }

    private void resetRecyclerViewHeight() {
        int maxCount = 4;
        int visibleItemCount = mSelectedFileList.size();
        if (visibleItemCount > maxCount) {
            visibleItemCount = maxCount;
        }
        visibleItemCount++; // take placeholder footer view into account
        int itemHeight = getResources().getDimensionPixelSize(R.dimen.item_height);
        int recyclerViewHeight = itemHeight * visibleItemCount;
        ViewUtils.updateViewHeight(recycler_view, recyclerViewHeight);
    }

    private void resetViewProperties() {
        float headerInitialTranslationY = ViewUtils.dp2px(getContext(), 60);
        float listInitialTranslationY = ViewUtils.dp2px(getContext(), 90);
        file_size_summary_header.setTranslationY(headerInitialTranslationY);
        tv_ready_encrypt_images.setTranslationY(headerInitialTranslationY);
        recycler_view.setTranslationY(listInitialTranslationY);
        recycler_view.setAlpha(0);
        file_size_summary_header.setAlpha(0);
        tv_ready_encrypt_images.setAlpha(0);
        header.setPivotY(0f);
        header.setScaleY(0.5f);
        fab.setScaleX(0f);
        fab.setScaleY(0f);
    }

    private void startEnterAnimations() {
        long firstAnimDuration = 600;
        long secondAnimDuration = 500;
        long animSetStartDelay = 500;
        ObjectAnimator headerAnimator = ViewPropertyObjectAnimator.animate(header)
                .scaleY(1f)
                .setDuration(firstAnimDuration)
                .get();
        ObjectAnimator listAnimator = ViewPropertyObjectAnimator.animate(recycler_view)
                .translationY(0f)
                .alpha(1f)
                .setDuration(firstAnimDuration)
                .get();
        ObjectAnimator fabAnimator = ViewPropertyObjectAnimator.animate(fab)
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(firstAnimDuration)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .get();
        ObjectAnimator titleAnimator = ViewPropertyObjectAnimator.animate(file_size_summary_header)
                .alpha(1f)
                .translationY(0f)
                .setDuration(secondAnimDuration)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .get();
        ObjectAnimator subtitleAnimator = ViewPropertyObjectAnimator.animate(tv_ready_encrypt_images)
                .alpha(1f)
                .setStartDelay(100)
                .translationY(0f)
                .setDuration(secondAnimDuration)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .get();
        AnimatorSet set = new AnimatorSet();
        set.setStartDelay(animSetStartDelay);
        set.play(headerAnimator).with(listAnimator).with(fabAnimator);
        set.play(titleAnimator).with(subtitleAnimator);
        set.play(titleAnimator).after(listAnimator);
        set.start();
    }

    @OnClick(R.id.fab)
    void startEncrypt(View view) {
        if (mOnEncryptButtonClickListener != null) {
            mOnEncryptButtonClickListener.onEncryptButtonClicked(this, view, mSelectedFileList);
        }
    }

    public static class Builder {
        ArrayList<FileItem> selectedFileList;
        OnEncryptButtonClickListener onEncryptButtonClickListener;

        public Builder setSelectedFileList(ArrayList<FileItem> selectedFileList) {
            this.selectedFileList = selectedFileList;
            return this;
        }

        public Builder setOnEncryptButtonClickListener(OnEncryptButtonClickListener onEncryptButtonClickListener) {
            this.onEncryptButtonClickListener = onEncryptButtonClickListener;
            return this;
        }

        public EncryptFilesDialogFragment build() {
            EncryptFilesDialogFragment dialogFragment = new EncryptFilesDialogFragment();
            dialogFragment.initFromBuilder(this);
            return dialogFragment;
        }
    }

    public interface OnEncryptButtonClickListener {
        void onEncryptButtonClicked(EncryptFilesDialogFragment dialogFragment, View view, ArrayList<FileItem> selectedImageList);
    }

}
