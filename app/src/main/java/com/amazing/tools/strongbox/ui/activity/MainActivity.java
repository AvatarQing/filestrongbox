package com.amazing.tools.strongbox.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.amazing.tools.ad.AdmobLogAdListener;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.exception.AlbumAlreadyExistedException;
import com.amazing.tools.strongbox.ui.dialog.AlbumNameEditDialogFragment;
import com.amazing.tools.strongbox.ui.fragment.EncryptedFileAlbumsFragment;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.utils.ColorUtils;
import com.amazing.tools.strongbox.utils.FeatureDiscoveryUtils;
import com.amazing.tools.strongbox.utils.FilePickUtils;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.RateHelper;
import com.amazing.tools.strongbox.utils.entityhelper.FileItemFactory;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * @author LiuQing
 */
public class MainActivity extends BaseAppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_FILE = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab_menu)
    FloatingActionMenu fab_menu;

    private EncryptedFileAlbumsFragment mAlbumFragment;

    private EncryptedFileUIHelper mEncryptedFileUIHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initHelpers();
        initViews();
        initAd();
        RateHelper.getInstance().increaseAppLaunchTime(this);
        RateHelper.getInstance().showRateDialogAfterLaunch(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEncryptedFileUIHelper.removeAllListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings: {
                startActivity(
                        new Intent(this, SettingsActivity.class)
                );
            }
            break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (fab_menu.isOpened()) {
            fab_menu.close(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PICK_FILE: {
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                    String filePath = FilePickUtils.getPath(this, data.getData());
                    Timber.tag(TAG).d("Selected file path:" + filePath);
                    FileItem fileItem = FileItemFactory.parseFileItemFromPath(filePath);
                    if (fileItem != null) {
                        ArrayList<FileItem> fileItems = new ArrayList<>();
                        fileItems.add(fileItem);
                        mEncryptedFileUIHelper.showEncryptConfirmDialog(fileItems, null);
                    } else {
                        Toast.makeText(this, R.string.prompt_file_does_not_exist, Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;
            default:
                break;
        }
    }

    private void initAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_in_main_ad_id));
        interstitialAd.setAdListener(new AdmobLogAdListener("InterstitialAd-Main") {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }
        });
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void initHelpers() {
        mEncryptedFileUIHelper = new EncryptedFileUIHelper(this, this);
    }

    private void initViews() {
        ButterKnife.bind(this);
        initTitleBar();
        initFab();
        initFragments();
        delayShowFabFeatureDiscovery();
    }

    private void initTitleBar() {
        toolbar.setTitle(getTitle());
        setSupportActionBar(toolbar);
    }

    private void initFragments() {
        mAlbumFragment = new EncryptedFileAlbumsFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mAlbumFragment, EncryptedFileAlbumsFragment.TAG)
                .commit();
    }

    private void initFab() {
        fab_menu.setClosedOnTouchOutside(true);
    }

    private void delayShowFabFeatureDiscovery() {
        if (FeatureDiscoveryUtils.hasShownImportToVault()) {
            return;
        }
        Observable.timer(800, TimeUnit.MILLISECONDS)
                .compose(bindUntilEvent(ActivityEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        showFabFeatureDiscovery();
                    }
                });
    }

    private void showFabFeatureDiscovery() {
        new TapTargetSequence(this)
                .target(
                        TapTarget.forView(fab_menu.getMenuIconView(), getString(R.string.title_import_to_vault), getString(R.string.prompt_import_to_vault))
                                .outerCircleColorInt(ColorUtils.getColorFromAttr(this, R.attr.colorPrimary))
                                .outerCircleAlpha(0.96f)
                                .targetCircleColorInt(ColorUtils.getColorFromAttr(this, R.attr.colorAccent))
                                .textColor(R.color.white100)
                                .dimColor(R.color.black100)
                                .drawShadow(true)
                                .tintTarget(false)
                )
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        FeatureDiscoveryUtils.setShownImportToVault(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget tapTarget, boolean b) {
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget tapTarget) {
                    }
                })
                .start();
    }

    @OnClick(R.id.fab_add_album)
    void showAddAlbumDialog() {
        new AlbumNameEditDialogFragment.Builder()
                .setTitle(getString(R.string.title_create_album))
                .setPositiveClickListener(new AlbumNameEditDialogFragment.OnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClicked(final AlbumNameEditDialogFragment dialogFragment, String inputText) {
                        dialogFragment.dismiss();
                        EncryptedFilesDao.createAlbumInRx(inputText)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new SingleObserver<EncryptedFileAlbum>() {
                                    @Override
                                    public void onSubscribe(@NonNull Disposable d) {
                                    }

                                    @Override
                                    public void onSuccess(@NonNull EncryptedFileAlbum album) {
                                        mAlbumFragment.addNewAlbum(album);
                                        Toast.makeText(getApplicationContext(), getString(R.string.format_album_created, album.name), Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onError(@NonNull Throwable e) {
                                        e.printStackTrace();
                                        if (e instanceof AlbumAlreadyExistedException) {
                                            Toast.makeText(getApplicationContext(), R.string.prompt_album_existed, Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), R.string.prompt_unknown_error, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                })
                .create()
                .show(getSupportFragmentManager(), "CreateAlbum");
        fab_menu.close(true);
    }

    @OnClick(R.id.fab_encrypt_video)
    void selectVideoToEncrypt() {
        EncryptedFileAlbum videoAlbum = EncryptedFilesDao.loadVideoAlbum();
        SelectMediaActivity.launch(this, videoAlbum, FileTypeUtils.TYPE_VIDEO);
        fab_menu.close(true);
    }

    @OnClick(R.id.fab_encrypt_image)
    void selectImageToEncrypt() {
        SelectMediaActivity.launch(this, null, FileTypeUtils.TYPE_IMAGE);
        fab_menu.close(true);
    }

    @OnClick(R.id.fab_encrypt_file)
    void selectFileToEncrypt() {
        FilePickUtils.pickFile(this, REQUEST_CODE_PICK_FILE);
        fab_menu.close(true);
    }

}
