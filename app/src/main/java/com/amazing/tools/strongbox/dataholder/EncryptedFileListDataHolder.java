package com.amazing.tools.strongbox.dataholder;

import com.amazing.tools.strongbox.entity.EncryptedFileInfo;

import java.util.List;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/24 </li>
 * </ul>
 */

public enum EncryptedFileListDataHolder {
    INSTANCE;

    private List<EncryptedFileInfo> mDataList;

    public static boolean hasData() {
        return INSTANCE.mDataList != null;
    }

    public static void setData(final List<EncryptedFileInfo> objectList) {
        INSTANCE.mDataList = objectList;
    }

    public static List<EncryptedFileInfo> getData() {
        final List<EncryptedFileInfo> retList = INSTANCE.mDataList;
        INSTANCE.mDataList = null;
        return retList;
    }
}