package com.amazing.tools.strongbox.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.enums.FileOpCommand;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.ui.widget.FileThumbnailView;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.github.zagum.expandicon.ExpandIconView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.amazing.tools.strongbox.enums.FileOpCommand.DECRYPT;
import static com.amazing.tools.strongbox.enums.FileOpCommand.OPEN_WITH;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.EXPANDED;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/24 </li>
 * </ul>
 */

public class EncryptedFileOperationPanelFragment extends BaseFragment implements SlidingUpPanelLayout.PanelSlideListener {

    public static final String TAG = EncryptedFileOperationPanelFragment.class.getSimpleName();

    @BindView(R.id.thumbnail_view)
    FileThumbnailView thumbnail_view;
    @BindView(R.id.btn_open_with)
    View btn_open_with;
    @BindView(R.id.tv_file_name)
    TextView tv_file_name;
    @BindView(R.id.tv_file_size)
    TextView tv_file_size;
    @BindView(R.id.arrow)
    ExpandIconView arrow;
    SlidingUpPanelLayout mPanelLayout;

    private FileOpCommand mFileOpCommand;
    private List<EncryptedFileInfo> mDataList;
    private EncryptedFileInfo mItemInfo;
    private EncryptedFileUIHelper mEncryptedFileUIHelper;
    private EncryptedFileAlbum mAlbum;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sliding_panel_file_operation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    public void setAlbum(EncryptedFileAlbum album) {
        this.mAlbum = album;
    }

    public void setEncryptedFileUiHelper(EncryptedFileUIHelper helper) {
        mEncryptedFileUIHelper = helper;
    }

    public void setPanelLayout(SlidingUpPanelLayout panelLayout) {
        mPanelLayout = panelLayout;
        mPanelLayout.addPanelSlideListener(this);
        mPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
    }

    public void showSlidingUpPanel(List<EncryptedFileInfo> dataList, int itemPosition) {
        EncryptedFileInfo item = dataList.get(itemPosition);
        updateSlidingUpPanelUI(item);
        if (getView() != null) {
            getView().getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mPanelLayout.setPanelState(EXPANDED);
                    getView().getViewTreeObserver().removeOnPreDrawListener(this);
                    return false;
                }
            });
        } else {
            mPanelLayout.setPanelState(EXPANDED);
        }
        mDataList = dataList;
        mItemInfo = item;
    }

    private void updateSlidingUpPanelUI(EncryptedFileInfo item) {
        switch (item.fileType) {
            case FileTypeUtils.TYPE_IMAGE:
                thumbnail_view.showAsCircleImage(item.encryptedFilePath);
                break;
            case FileTypeUtils.TYPE_VIDEO:
                thumbnail_view.showAsCircleImage(item.thumbnailFilePath);
                break;
            case FileTypeUtils.TYPE_AUDIO:
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                thumbnail_view.showAsFile();
                break;
        }
        tv_file_name.setText(item.originalFileName);
        tv_file_size.setText(Formatter.formatFileSize(getContext(), item.fileSize));
    }

    @OnClick({
            R.id.btn_open,
            R.id.btn_open_with,
            R.id.btn_decrypt,
            R.id.btn_rename,
            R.id.btn_share,
            R.id.btn_delete,
            R.id.btn_details,
            R.id.btn_move,
    })
    void onPanelOpItemClicked(View view) {
        mPanelLayout.setPanelState(COLLAPSED);
        FileOpCommand command;
        switch (view.getId()) {
            case R.id.btn_open:
                command = FileOpCommand.OPEN;
                break;
            case R.id.btn_open_with:
                command = OPEN_WITH;
                break;
            case R.id.btn_decrypt:
                command = DECRYPT;
                break;
            case R.id.btn_rename:
                command = FileOpCommand.RENAME;
                break;
            case R.id.btn_share:
                command = FileOpCommand.SHARE;
                break;
            case R.id.btn_delete:
                command = FileOpCommand.DELETE;
                break;
            case R.id.btn_details:
                command = FileOpCommand.INFO;
                break;
            case R.id.btn_move:
                command = FileOpCommand.MOVE;
                break;
            default:
                command = null;
                break;
        }
        mFileOpCommand = command;
    }

    @Override
    public void onPanelSlide(View view, float slideOffset) {
        float fraction = 1 - slideOffset;
        if (fraction >= 0 && fraction <= 1) {
            arrow.setFraction(1 - slideOffset, true);
        }
    }

    @Override
    public void onPanelStateChanged(View view, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
        switch (newState) {
            case COLLAPSED: {
                if (mFileOpCommand != null) {
                    switch (mFileOpCommand) {
                        case OPEN:
                            mEncryptedFileUIHelper.openFile(mItemInfo, mDataList);
                            break;
                        case OPEN_WITH:
                            mEncryptedFileUIHelper.openFileByOtherApp(mItemInfo);
                            break;
                        case DECRYPT:
                            mEncryptedFileUIHelper.showDecryptConfirmDialog(wrapItemInList());
                            break;
                        case RENAME:
                            mEncryptedFileUIHelper.showRenameFileDialog(mItemInfo);
                            break;
                        case SHARE:
                            mEncryptedFileUIHelper.shareFile(mItemInfo);
                            break;
                        case DELETE:
                            if (mAlbum != null && mAlbum.type == EncryptedFileAlbum.TYPE_RECYCLE_BIN) {
                                mEncryptedFileUIHelper.showConfirmDeleteFileDialog(wrapItemInList());
                            } else {
                                mEncryptedFileUIHelper.showConfirmMoveFileDialog(wrapItemInList());
                            }
                            break;
                        case INFO:
                            mEncryptedFileUIHelper.showFileInfoDialog(mItemInfo);
                            break;
                        case MOVE:
                            mEncryptedFileUIHelper.showChangeAlbumDialog(mAlbum, wrapItemInList());
                            break;
                        default:
                            break;
                    }
                    mFileOpCommand = null;
                }
            }
            break;
            default:
                break;
        }
    }

    private ArrayList<EncryptedFileInfo> wrapItemInList() {
        ArrayList<EncryptedFileInfo> imageItems = new ArrayList<>();
        imageItems.add(mItemInfo);
        return imageItems;
    }

}
