package com.amazing.tools.strongbox.ui.adatpter;

import android.widget.ImageView;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.typedef.FileType;
import com.amazing.tools.strongbox.utils.FileTypeUtils;

/**
 * @author LiuQing
 */
public class MediaItemListAdapter extends BaseImageListAdapter<FileItem> {

    @FileType
    private int mediaType;

    public MediaItemListAdapter(@FileType int mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    protected void loadImage(ImageView imageView, FileItem item) {
        GlideApp.with(imageView)
                .load(item.path)
                .into(imageView);
    }

    @Override
    protected boolean needShowVideoIndicator() {
        return mediaType == FileTypeUtils.TYPE_VIDEO;
    }
}