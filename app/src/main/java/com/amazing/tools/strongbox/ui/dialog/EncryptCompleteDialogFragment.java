package com.amazing.tools.strongbox.ui.dialog;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.codetail.animation.ViewAnimationUtils;

/**
 * @authoer AvatarQing
 * @date 2017/7/11.
 */

public class EncryptCompleteDialogFragment extends BaseDialogFragment {

    @BindView(R.id.tv_encrypt_consuming_time)
    TextView tv_encrypt_consuming_time;
    @BindView(R.id.tv_encrypt_state)
    TextView tv_encrypt_state;
    @BindView(R.id.iv_state_tip)
    ImageView iv_state_tip;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.list_container)
    View list_container;
    @BindView(R.id.header)
    View header;

    private List<FileInfo> mFileInfoList = new ArrayList<>();
    private long mConsumingTime = 0;
    private DialogInterface.OnDismissListener mOnDismissListener;
    private Animator mEnterAnim;

    private void initFromBuilder(Builder builder) {
        mFileInfoList = builder.fileInfoList;
        mConsumingTime = builder.consumingTime;
        mOnDismissListener = builder.onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
        }
    }

    @Override
    protected boolean enableWindowAnim() {
        return false;
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getContext()).inflate(R.layout.dialog_encrypt_complete, parentView, false);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);
        updateStatHeaderUI();
        initRecyclerView();
        resetLayout();
        // 等待View布局完成后才开始动画
        header.post(new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    return;
                }
                startEnterAnim();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEnterAnim != null) {
            mEnterAnim.cancel();
        }
    }

    private void updateStatHeaderUI() {
        int failCount = calculateFailEncryptCount();

        @ColorInt int headerBgColor;
        @DrawableRes int tipIconResId;
        String stateText;
        if (failCount == 0) {
            stateText = getString(R.string.prompt_all_files_encrypted);
            headerBgColor = ContextCompat.getColor(getContext(), R.color.colorSuccessDark);
            tipIconResId = R.drawable.ic_done;
        } else {
            int totalCount = mFileInfoList.size();
            stateText = getString(R.string.format_encrypt_fail_count, failCount, totalCount);
            headerBgColor = ContextCompat.getColor(getContext(), R.color.colorWarn);
            tipIconResId = R.drawable.ic_error;
        }
        tv_encrypt_state.setText(stateText);
        header.setBackgroundColor(headerBgColor);
        iv_state_tip.setImageResource(tipIconResId);

        long totalFileSize = calculateTotalFileSize();
        String formatFileSizeText = Formatter.formatFileSize(getContext(), totalFileSize);
        String consumingTimeText = getString(R.string.format_encrypted_consuming_time, formatFileSizeText, mConsumingTime);
        tv_encrypt_consuming_time.setText(consumingTimeText);
    }

    private void resetLayout() {
        fab.setScaleX(0f);
        fab.setScaleY(0f);
        fab.setAlpha(0f);
    }

    private long calculateTotalFileSize() {
        long totalSize = 0;
        for (FileInfo item : mFileInfoList) {
            totalSize += item.fileSize;
        }
        return totalSize;
    }

    private int calculateFailEncryptCount() {
        int failCount = 0;
        for (FileInfo item : mFileInfoList) {
            if (!item.isEncryptSuccess) {
                failCount++;
            }
        }
        return failCount;
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(layoutManager);

        FileStateListAdapter adapter = new FileStateListAdapter(R.layout.item_encrypt_or_decrypt_file_state, mFileInfoList);
        recycler_view.setAdapter(adapter);
        addPlaceHolderFooterToRecyclerView(adapter);
    }

    private void addPlaceHolderFooterToRecyclerView(FileStateListAdapter adapter) {
        View emptyFooterView = new View(getContext());
        int itemHeight = getResources().getDimensionPixelSize(R.dimen.item_height);
        LinearLayout.LayoutParams footerViewLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, itemHeight);
        emptyFooterView.setLayoutParams(footerViewLp);
        adapter.setFooterView(emptyFooterView);
    }

    private void resetRecyclerViewHeight() {
        int maxCount = 3;
        int visibleItemCount = mFileInfoList.size();
        if (visibleItemCount > maxCount) {
            visibleItemCount = maxCount;
        }
        visibleItemCount++; // take placeholder footer view into account
        int itemHeight = getResources().getDimensionPixelSize(R.dimen.item_height);
        int recyclerViewHeight = itemHeight * visibleItemCount;
        ViewUtils.updateViewHeight(recycler_view, recyclerViewHeight);
    }

    private Animator getCircleRevealAnim(final View revealView) {
        float finalRadius = (float) Math.hypot(revealView.getWidth() / 2f, revealView.getHeight());
        int startX = revealView.getWidth() / 2;
        int startY = revealView.getHeight();
        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(revealView, startX, startY, 0, finalRadius);

        revealAnimator.setDuration(400);
        revealAnimator.setStartDelay(200);
        revealAnimator.setInterpolator(new FastOutLinearInInterpolator());
        revealAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                revealView.setVisibility(View.VISIBLE);
            }
        });
        return revealAnimator;
    }

    private void startEnterAnim() {
        Animator revealAnim = getCircleRevealAnim(header);
        ObjectAnimator fabAnim = ViewPropertyObjectAnimator.animate(fab)
                .scaleX(1f)
                .scaleY(1f)
                .alpha(1f)
                .setDuration(400)
                .setStartDelay(400)
                .withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        list_container.setVisibility(View.VISIBLE);
                        resetRecyclerViewHeight();
                    }
                })
                .get();
        AnimatorSet set = new AnimatorSet();
        set.play(revealAnim).before(fabAnim);
        set.start();
        mEnterAnim = set;
    }

    @OnClick(R.id.fab)
    void close() {
        dismiss();
    }

    private static class FileStateListAdapter extends BaseQuickAdapter<FileInfo, BaseViewHolder> {

        private FileStateListAdapter(@LayoutRes int layoutResId, @Nullable List<FileInfo> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, FileInfo item) {
            helper.setText(R.id.tv_file_name, item.fileName);
            helper.setText(R.id.tv_state, item.isEncryptSuccess ? R.string.prompt_success : R.string.prompt_fail);
            helper.setBackgroundRes(R.id.iv_tip, item.isEncryptSuccess ? R.drawable.circle_success : R.drawable.circle_fail);
            helper.setImageResource(R.id.iv_tip, item.isEncryptSuccess ? R.drawable.ic_done : R.drawable.ic_exclaim);
        }
    }

    public static class Builder {
        List<FileInfo> fileInfoList;
        long consumingTime = 0;
        DialogInterface.OnDismissListener onDismissListener;

        public Builder setFileInfoList(List<FileInfo> fileInfoList) {
            this.fileInfoList = fileInfoList;
            return this;
        }

        public Builder setConsumingTime(long consumingTime) {
            this.consumingTime = consumingTime;
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
            return this;
        }

        public EncryptCompleteDialogFragment build() {
            EncryptCompleteDialogFragment dialogFragment = new EncryptCompleteDialogFragment();
            dialogFragment.initFromBuilder(this);
            return dialogFragment;
        }
    }

    public static class FileInfo {
        public String fileName;
        public long fileSize;
        public boolean isEncryptSuccess;
    }
}