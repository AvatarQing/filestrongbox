package com.amazing.tools.strongbox.ui.adatpter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

/**
 * @author LiuQing
 */
public class EncryptedFileAlbumsGridAdapter extends BaseQuickAdapter<EncryptedFileAlbum, BaseViewHolder> {

    public EncryptedFileAlbumsGridAdapter() {
        super(R.layout.item_album);
    }

    @Override
    protected BaseViewHolder createBaseViewHolder(ViewGroup parent, int layoutResId) {
        BaseViewHolder holder = super.createBaseViewHolder(parent, layoutResId);
        holder.addOnClickListener(R.id.btn_more);
        return holder;
    }

    @Override
    protected void convert(BaseViewHolder helper, EncryptedFileAlbum item) {
        Context context = helper.itemView.getContext();

        // 封面图片
        if (item.type != EncryptedFileAlbum.TYPE_RECYCLE_BIN && item.latestFileInfo != null) {
            helper.setBackgroundColor(R.id.album_cover, Color.TRANSPARENT);
            ImageView imageView = helper.getView(R.id.album_cover);
            switch (item.latestFileInfo.fileType) {
                case FileTypeUtils.TYPE_IMAGE:
                    GlideApp.with(imageView)
                            .load(item.latestFileInfo.encryptedFilePath)
                            .centerCrop()
                            .into(imageView);
                    break;
                case FileTypeUtils.TYPE_VIDEO:
                    GlideApp.with(imageView)
                            .load(item.latestFileInfo.thumbnailFilePath)
                            .centerCrop()
                            .into(imageView);
                    break;
                case FileTypeUtils.TYPE_TEXT:
                case FileTypeUtils.TYPE_FILE:
                case FileTypeUtils.TYPE_AUDIO:
                default:
                    helper.setBackgroundColor(R.id.album_cover, ContextCompat.getColor(context, R.color.file_thumbnail_normal_bg));
                    helper.setImageResource(R.id.album_cover, R.drawable.ic_file);
                    break;
            }
        } else {
            @DrawableRes int coverImageResId;
            @ColorRes int coverBgColorResId;
            switch (item.type) {
                case EncryptedFileAlbum.TYPE_MAIN:
                    coverImageResId = R.drawable.album_cover_photos_88_dp;
                    coverBgColorResId = R.color.album_main;
                    break;
                case EncryptedFileAlbum.TYPE_RECYCLE_BIN:
                    coverImageResId = R.drawable.album_cover_trash_88_dp;
                    coverBgColorResId = R.color.album_recycle_bin;
                    break;
                case EncryptedFileAlbum.TYPE_IMPORTANT:
                    coverImageResId = R.drawable.album_cover_heart_88_dp;
                    coverBgColorResId = R.color.album_important_files;
                    break;
                case EncryptedFileAlbum.TYPE_VIDEO:
                    coverImageResId = R.drawable.album_cover_video_88_dp;
                    coverBgColorResId = R.color.album_videos;
                    break;
                case EncryptedFileAlbum.TYPE_USER_CREATE:
                default:
                    coverImageResId = R.drawable.album_cover_photos_88_dp;
                    coverBgColorResId = R.color.album_user_created;
                    break;
            }
            int coverBgColor = ContextCompat.getColor(context, coverBgColorResId);
            Drawable coverDrawable = ContextCompat.getDrawable(context, coverImageResId);
            coverDrawable.setColorFilter(coverBgColor, PorterDuff.Mode.SCREEN);

            helper.setBackgroundColor(R.id.album_cover, coverBgColor);
            helper.setImageDrawable(R.id.album_cover, coverDrawable);
        }

        // 专辑名称
        helper.setText(R.id.album_name, item.name);

        // 更多按钮
        boolean showMoreBtn = item.type != EncryptedFileAlbum.TYPE_MAIN && item.type != EncryptedFileAlbum.TYPE_RECYCLE_BIN;
        helper.setVisible(R.id.btn_more, showMoreBtn);
    }

}