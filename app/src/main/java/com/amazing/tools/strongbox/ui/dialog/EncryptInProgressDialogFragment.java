package com.amazing.tools.strongbox.ui.dialog;

import android.animation.Animator;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.codetail.animation.ViewAnimationUtils;

public class EncryptInProgressDialogFragment extends BaseDialogFragment {

    @BindView(R.id.tv_progress)
    TextView tv_progress;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_file_name)
    TextView tv_file_name;
    @BindView(R.id.content_view)
    View content_view;

    private Animator mEnterAnim;

    @Override
    protected boolean enableWindowAnim() {
        return false;
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getContext()).inflate(R.layout.dialog_encrypt_in_progress, parentView, false);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);
        updateEncryptingProgress(0, 0, "");

        // 等待View布局完成后才开始执行揭露动画
        content_view.post(() -> startCircleRevealAnim(content_view));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEnterAnim != null) {
            mEnterAnim.cancel();
        }
    }

    public void updateEncryptingProgress(int currentIndex, int totalCount, String fileName) {
        int progress = totalCount == 0 ? 0 : 100 * currentIndex / totalCount;
        tv_progress.setText(String.format(Locale.getDefault(), "%d%%", progress));
        tv_title.setText(getString(R.string.format_encrypting_files, currentIndex + 1, totalCount));
        tv_file_name.setText(fileName);
    }

    private void startCircleRevealAnim(View revealView) {
        revealView.setVisibility(View.VISIBLE);

        float finalRadius = (float) Math.hypot(revealView.getWidth() / 2f, revealView.getHeight());
        int startX = revealView.getWidth() / 2;
        int startY = revealView.getHeight();
        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(revealView, startX, startY, 0, finalRadius);

        revealAnimator.setDuration(400);
        revealAnimator.setStartDelay(200);
        revealAnimator.setInterpolator(new FastOutLinearInInterpolator());
        revealAnimator.start();

        mEnterAnim = revealAnimator;
    }

}