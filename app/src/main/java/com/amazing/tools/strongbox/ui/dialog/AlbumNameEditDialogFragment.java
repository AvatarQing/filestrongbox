package com.amazing.tools.strongbox.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创建相册对话框
 *
 * @author LiuQing
 */
public class AlbumNameEditDialogFragment extends BaseDialogFragment {

    @BindView(R.id.edit_text)
    EditText mEditText;
    @BindView(R.id.btn_ok)
    View mBtnOk;
    @BindView(R.id.tv_title)
    TextView mTvTitle;

    private OnPositiveButtonClickListener mPositiveClickListener;
    private String mName;
    private String mTitle;

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getContext()).inflate(R.layout.dialog_add_album, parentView, false);
    }

    @Override
    protected void configWindow(Window window) {
    }

    @Override
    protected void configDialogSize(Dialog dialog) {
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);
        mTvTitle.setText(mTitle);
        mEditText.setText(mName);
        mEditText.setSelection(0, mEditText.getText().length());
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mBtnOk.setEnabled(s.length() > 0);
            }
        });
        mEditText.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEditText.setFocusable(true);
                mEditText.setFocusableInTouchMode(true);
                mEditText.requestFocus();
                InputMethodManager inputManager = (InputMethodManager) mEditText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(mEditText, 0);
            }
        }, 100);
    }

    @OnClick(R.id.btn_cancel)
    void closeDialog() {
        dismiss();
    }

    @OnClick(R.id.btn_ok)
    void createAlbum() {
        if (mPositiveClickListener != null) {
            mPositiveClickListener.onPositiveButtonClicked(this, mEditText.getText().toString());
        }
    }

    public interface OnPositiveButtonClickListener {
        /**
         * “确认”按钮被点击时调用
         *
         * @param dialogFragment 对话框实例
         * @param inputText      输入的文本
         */
        void onPositiveButtonClicked(AlbumNameEditDialogFragment dialogFragment, String inputText);
    }

    public static class Builder {

        private OnPositiveButtonClickListener positiveClickListener;
        private String name;
        private String title;

        public Builder setPositiveClickListener(OnPositiveButtonClickListener listener) {
            this.positiveClickListener = listener;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public AlbumNameEditDialogFragment create() {
            AlbumNameEditDialogFragment dialogFragment = new AlbumNameEditDialogFragment();
            dialogFragment.mPositiveClickListener = positiveClickListener;
            dialogFragment.mName = name;
            dialogFragment.mTitle = title;
            return dialogFragment;
        }
    }

}
