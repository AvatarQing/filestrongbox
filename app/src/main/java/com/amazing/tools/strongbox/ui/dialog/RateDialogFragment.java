package com.amazing.tools.strongbox.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.amazing.tools.strongbox.R;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/6 </li>
 * </ul>
 */
public class RateDialogFragment extends AppCompatDialogFragment {

    public static final String TAG = "RateDialogFragment";

    /**
     * GooglePlay包名
     */
    private static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending";
    /**
     * GooglePlay地址https前缀
     */
    private static final String GOOGLE_PLAY_PREFIX_HTTPS = "https://play.google.com/store/apps/details?id=";

    private Builder mBuilder;

    private void setBuilder(Builder builder) {
        this.mBuilder = builder;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_rate_us)
                .setMessage(R.string.prompt_rate_app)
                .setPositiveButton(R.string.btn_rate_good, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Activity activityRef = getActivity();
                        if (activityRef != null) {
                            goToPlayStore(activityRef);
                        }
                        if (mBuilder != null && mBuilder.positiveButtonClickListener != null) {
                            mBuilder.positiveButtonClickListener.onClick(dialog, which);
                        }
                    }
                })
                .setNeutralButton(R.string.btn_complain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Activity activityRef = getActivity();
                        if (activityRef != null) {
                            sendEmail(activityRef);
                        }
                        if (mBuilder != null && mBuilder.neutralButtonClickListener != null) {
                            mBuilder.neutralButtonClickListener.onClick(dialog, which);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mBuilder != null && mBuilder.negativeButtonClickListener != null) {
                            mBuilder.negativeButtonClickListener.onClick(dialog, which);
                        }
                    }
                })
                .create();
    }

    private void goToPlayStore(Activity activity) {
        String playUrl = GOOGLE_PLAY_PREFIX_HTTPS + activity.getPackageName();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(playUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // 如果安装了GooglePlay
        if (isAppInstall(activity, GOOGLE_PLAY_PACKAGE_NAME)) {
            // 就用GooglePlay打开链接
            intent.setPackage(GOOGLE_PLAY_PACKAGE_NAME);
        }

        try {
            activity.startActivity(intent);
        } catch (Exception e) {
            // 做一个异常捕获，防止没有第三方程序可以打开这个链接
            e.printStackTrace();
        }
    }

    private void sendEmail(Context context) {
        String receiver = context.getString(R.string.feedback_email);
        String subject = "";
        sendTextEmail(context, new String[]{receiver}, subject, null);
    }

    private void sendTextEmail(Context context, String[] receivers, String subject, String content) {
        // 将反馈内容发送邮件给作者
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        // 设置邮件收件人
        intent.putExtra(Intent.EXTRA_EMAIL, receivers);
        // 设置邮件标题
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        // 设置邮件内容
        intent.putExtra(Intent.EXTRA_TEXT, content);
        // 调用系统的邮件系统
        intent = Intent.createChooser(intent, subject);
        try {
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            // 如果设备没有安装可以发送邮件的程序，就会出错，所以手动捕获以下
            e.printStackTrace();
        }
    }

    private boolean isAppInstall(Context context, String packageName) {
        boolean installed = false;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            if (packageInfo != null) {
                installed = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return installed;
    }

    public static class Builder {
        DialogInterface.OnClickListener positiveButtonClickListener;
        DialogInterface.OnClickListener negativeButtonClickListener;
        DialogInterface.OnClickListener neutralButtonClickListener;

        public Builder setPositiveButtonClickListener(DialogInterface.OnClickListener positiveButtonClickListener) {
            this.positiveButtonClickListener = positiveButtonClickListener;
            return this;
        }

        public Builder setNegativeButtonClickListener(DialogInterface.OnClickListener negativeButtonClickListener) {
            this.negativeButtonClickListener = negativeButtonClickListener;
            return this;
        }

        public Builder setNeutralButtonClickListener(DialogInterface.OnClickListener neutralButtonClickListener) {
            this.neutralButtonClickListener = neutralButtonClickListener;
            return this;
        }

        public RateDialogFragment create() {
            RateDialogFragment dialogFragment = new RateDialogFragment();
            dialogFragment.setBuilder(this);
            return dialogFragment;
        }
    }

}
