package com.amazing.tools.strongbox.utils.entityhelper;

import android.database.Cursor;
import android.provider.MediaStore;

import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.MimeTypeUtils;

import java.io.File;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/23 </li>
 * </ul>
 */

public class FileItemFactory {

    public static FileItem parseFileItemFromPath(String path) {
        if (path != null) {
            File file = new File(path);
            if (file.exists() && file.length() > 0) {
                FileItem fileItem = new FileItem();
                fileItem.path = file.getAbsolutePath();
                fileItem.name = file.getName();
                fileItem.size = file.length();
                fileItem.modifiedTime = file.lastModified();
                fileItem.mimeType = MimeTypeUtils.getMimeType(file);
                fileItem.fileType = FileTypeUtils.getFileType(fileItem.mimeType);
                return fileItem;
            }
        }
        return null;
    }

    public static FileItem parseFileItemFromMediaStoreImagesCursor(Cursor cursor) {
        int pathColumnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        int sizeColumnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.SIZE);
        int dateModifiedColumnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATE_MODIFIED);
        int mimeTypeColumnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.MIME_TYPE);

        String path = cursor.getString(pathColumnIndex);
        File file = new File(path);
        if (file.exists() && file.length() > 0) {
            FileItem fileItem = new FileItem();
            fileItem.name = file.getName();
            fileItem.path = path;
            fileItem.size = cursor.getLong(sizeColumnIndex);
            fileItem.modifiedTime = cursor.getLong(dateModifiedColumnIndex);
            fileItem.mimeType = cursor.getString(mimeTypeColumnIndex);
            fileItem.fileType = FileTypeUtils.getFileType(fileItem.mimeType);
            return fileItem;
        }
        return null;
    }

    public static FileItem parseFileItemFromEncryptFileInfo(EncryptedFileInfo encryptedFileInfo) {
        FileItem fileItem = new FileItem();
        fileItem.path = encryptedFileInfo.encryptedFilePath;
        fileItem.name = encryptedFileInfo.encryptedFileName;
        fileItem.size = encryptedFileInfo.fileSize;
        fileItem.modifiedTime = encryptedFileInfo.modifiedTime;
        fileItem.mimeType = encryptedFileInfo.mimeType;
        fileItem.fileType = encryptedFileInfo.fileType;
        return fileItem;
    }

}