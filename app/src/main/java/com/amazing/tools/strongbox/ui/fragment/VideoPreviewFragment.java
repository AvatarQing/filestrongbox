package com.amazing.tools.strongbox.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.encrypt.EncryptedFileDataSource;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author AvatarQing
 */
public class VideoPreviewFragment extends BaseFragment {
    private static final String ARG_VIDEO_PATH = "ARG_VIDEO_PATH";
    private static final String ARG_IS_ENCRYPTED = "ARG_IS_ENCRYPTED";

    @BindView(R.id.player_view)
    SimpleExoPlayerView playerView;

    private SimpleExoPlayer mPlayer;

    private String mVideoPath;
    private boolean mIsEncrypted;

    public static VideoPreviewFragment newInstance(String videoPath, boolean isEncrypted) {
        Bundle args = new Bundle();
        args.putString(ARG_VIDEO_PATH, videoPath);
        args.putBoolean(ARG_IS_ENCRYPTED, isEncrypted);

        VideoPreviewFragment fragment = new VideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parseArguments();
        initPlayer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        playerView.setPlayer(mPlayer);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        togglePlayOrPauseVideo();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseVideo();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            togglePlayOrPauseVideo();
        }
    }

    private void parseArguments() {
        if (getArguments() == null) {
            return;
        }
        mVideoPath = getArguments().getString(ARG_VIDEO_PATH);
        mIsEncrypted = getArguments().getBoolean(ARG_IS_ENCRYPTED, false);
    }

    private void initPlayer() {
        // 1. Create a default TrackSelector
        Handler mainHandler = new Handler();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(mainHandler, videoTrackSelectionFactory);

        // 2. Create the player
        SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, new DefaultLoadControl());

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory;
        if (mIsEncrypted) {
            dataSourceFactory = new EncryptedFileDataSource.Factory();
        } else {
            dataSourceFactory = new DefaultDataSourceFactory(getContext(),
                    Util.getUserAgent(getContext(), getContext().getPackageName()));
        }
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource(Uri.fromFile(new File(mVideoPath)), dataSourceFactory, new DefaultExtractorsFactory(), mainHandler, Throwable::printStackTrace);
        videoSource = new LoopingMediaSource(videoSource);

        // Prepare the player with the source.
        player.prepare(videoSource);

        mPlayer = player;
    }

    private void togglePlayOrPauseVideo() {
        if (getUserVisibleHint()) {
            playVideo();
        } else {
            pauseVideo();
        }
    }

    private void playVideo() {
        mPlayer.setPlayWhenReady(true);
    }

    private void pauseVideo() {
        mPlayer.setPlayWhenReady(false);
    }

    private void releaseVideo() {
        mPlayer.release();
    }

}
