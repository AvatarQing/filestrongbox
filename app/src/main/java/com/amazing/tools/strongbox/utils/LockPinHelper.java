package com.amazing.tools.strongbox.utils;

import android.text.TextUtils;

import com.amazing.tools.strongbox.utils.data.Keeper;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/7/30 </li>
 * </ul>
 */

public class LockPinHelper {

    private static volatile LockPinHelper sInstance;

    public static LockPinHelper get() {
        if (sInstance == null) {
            synchronized (LockPinHelper.class) {
                if (sInstance == null) {
                    sInstance = new LockPinHelper();
                }
            }
        }
        return sInstance;
    }

    private LockPinHelper() {
    }

    public boolean hasSetPin() {
        return !TextUtils.isEmpty(Keeper.get().getPin());
    }

    public void savePin(String rawPin) {
        String md5Pin = MD5Utils.md5(rawPin);
        Keeper.get().savePin(md5Pin);
    }

    public boolean verifyPin(String pin) {
        String savedPin = Keeper.get().getPin();
        String md5Pin = MD5Utils.md5(pin);
        return TextUtils.equals(savedPin, md5Pin);
    }
}
