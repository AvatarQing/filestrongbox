package com.amazing.tools.strongbox.ui.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazing.tools.ad.FacebookLogAdListener;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.db.BatchFlowContentObserver;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.exception.AlbumAlreadyExistedException;
import com.amazing.tools.strongbox.ui.activity.EncryptedFilesActivity;
import com.amazing.tools.strongbox.ui.adatpter.EncryptedFileAlbumsGridAdapter;
import com.amazing.tools.strongbox.ui.decoration.SpacingDecoration;
import com.amazing.tools.strongbox.ui.dialog.AlbumNameEditDialogFragment;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.facebook.ads.Ad;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdView;
import com.raizlabs.android.dbflow.runtime.OnTableChangedListener;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * @author LiuQing
 */
public class EncryptedFileAlbumsFragment extends BaseFragment {

    public static final String TAG = "EncryptedFileAlbumsFragment";

    private RecyclerView mRecyclerView;
    private EncryptedFileAlbumsGridAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BatchFlowContentObserver.getInstance().addOnTableChangedListener(mOnTableChangedListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRecyclerView = new RecyclerView(getContext());
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        initAd();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BatchFlowContentObserver.getInstance().removeTableChangedListener(mOnTableChangedListener);
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(layoutManager);

        int itemSpacing = getResources().getDimensionPixelOffset(R.dimen.album_item_spacing);
        SpacingDecoration spacingDecoration = new SpacingDecoration(itemSpacing, itemSpacing, true);
        mRecyclerView.addItemDecoration(spacingDecoration);

        mAdapter = new EncryptedFileAlbumsGridAdapter();
        mAdapter.bindToRecyclerView(mRecyclerView);

        mAdapter.setOnItemClickListener(mOnItemClickListener);
        mAdapter.setOnItemChildClickListener(mOnItemChildClickListener);
    }

    private void initAd() {
        NativeAd nativeAd = new NativeAd(getContext(), getString(R.string.fb_native_ad_id));
        nativeAd.setAdListener(new FacebookLogAdListener("NativeAd-Main") {
            @Override
            public void onAdLoaded(Ad ad) {
                super.onAdLoaded(ad);
                View adView = NativeAdView.render(getContext(), nativeAd, NativeAdView.Type.HEIGHT_300);
                mAdapter.setHeaderView(adView);
            }
        });
        nativeAd.loadAd();
    }

    private void loadData() {
        EncryptedFilesDao.loadAllAlbums()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<EncryptedFileAlbum>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull List<EncryptedFileAlbum> albumList) {
                        mAdapter.setNewData(albumList);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void addNewAlbum(EncryptedFileAlbum album) {
        // 插入倒数第二个位置，也就是回收站的前面
        int insertPos = mAdapter.getHeaderLayoutCount() + mAdapter.getData().size() - 1;
        mAdapter.addData(insertPos, album);
    }

    private void deleteAlbumFromAdapter(EncryptedFileAlbum album) {
        int listIndex = mAdapter.getData().indexOf(album);
        int adapterIndex = listIndex + mAdapter.getHeaderLayoutCount();
        mAdapter.remove(adapterIndex);
    }

    private void refreshAlbumItem(EncryptedFileAlbum album) {
        int listIndex = mAdapter.getData().indexOf(album);
        int adapterIndex = listIndex + mAdapter.getHeaderLayoutCount();
        mAdapter.notifyItemChanged(adapterIndex);
    }

    private void showAlbumMoreMenu(View anchorView, final EncryptedFileAlbum album) {
        PopupMenu popupMenu = new PopupMenu(getContext(), anchorView);
        popupMenu.inflate(R.menu.album_more_options);
        MenuItem renameMenuItem = popupMenu.getMenu().findItem(R.id.rename);
        MenuItem deleteMenuItem = popupMenu.getMenu().findItem(R.id.delete);
        for (int reservedType : EncryptedFileAlbum.CANNOT_DELETE_ALBUM_TYPE) {
            if (album.type == reservedType) {
                renameMenuItem.setVisible(false);
                deleteMenuItem.setVisible(false);
                break;
            }
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    // 重命名相册
                    case R.id.rename:
                        showRenameAlbumDialog(album);
                        return true;
                    // 删除相册
                    case R.id.delete:
                        showDeleteAlbumConfirmDialog(album);
                        return true;
                    // 相册设置
                    case R.id.settings:
                        // TODO: 2017/10/24 相册设置
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private void showRenameAlbumDialog(final EncryptedFileAlbum album) {
        new AlbumNameEditDialogFragment.Builder()
                .setTitle(getString(R.string.title_rename_album))
                .setName(album.name)
                .setPositiveClickListener(new AlbumNameEditDialogFragment.OnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlbumNameEditDialogFragment dialogFragment, String inputText) {
                        dialogFragment.dismiss();
                        EncryptedFilesDao.renameAlbumInRx(album, inputText)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new SingleObserver<EncryptedFileAlbum>() {
                                    @Override
                                    public void onSubscribe(@NonNull Disposable d) {
                                    }

                                    @Override
                                    public void onSuccess(@NonNull EncryptedFileAlbum album) {
                                        refreshAlbumItem(album);
                                    }

                                    @Override
                                    public void onError(@NonNull Throwable e) {
                                        e.printStackTrace();
                                        if (e instanceof AlbumAlreadyExistedException) {
                                            Toast.makeText(getContext(), R.string.prompt_album_existed, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                })
                .create()
                .show(getChildFragmentManager(), "RenameAlbum");
    }

    private void showDeleteAlbumConfirmDialog(final EncryptedFileAlbum album) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.title_confirm)
                .setMessage(R.string.prompt_confirm_delete_album)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EncryptedFilesDao.deleteAlbumAndMoveFilesToRecycleBinInRx(album)
                                .compose(bindUntilEvent(FragmentEvent.DESTROY))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new SingleObserver<Boolean>() {
                                    @Override
                                    public void onSubscribe(@NonNull Disposable d) {
                                    }

                                    @Override
                                    public void onSuccess(@NonNull Boolean deleteSuccess) {
                                        if (deleteSuccess) {
                                            deleteAlbumFromAdapter(album);
                                            Toast.makeText(getContext(), getString(R.string.prompt_deleted_album), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onError(@NonNull Throwable e) {
                                        e.printStackTrace();
                                    }
                                });
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create()
                .show();
    }

    private BaseQuickAdapter.OnItemClickListener mOnItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            EncryptedFileAlbum album = mAdapter.getItem(position);
            if (album != null) {
                EncryptedFilesActivity.launch(getActivity(), album);
            }
        }
    };

    private BaseQuickAdapter.OnItemChildClickListener mOnItemChildClickListener = new BaseQuickAdapter.OnItemChildClickListener() {
        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            switch (view.getId()) {
                case R.id.btn_more:
                    EncryptedFileAlbum album = mAdapter.getItem(position);
                    if (album != null) {
                        showAlbumMoreMenu(view, album);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private OnTableChangedListener mOnTableChangedListener = (tableChanged, action) -> {
        Timber.tag(TAG).d("table:%1$s, action:%2$s", tableChanged, action.name());
        loadData();
    };

}