package com.amazing.tools.strongbox.ui.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.DialogUtils;

/**
 * Author: LiuQing
 * Date: 2017/7/10
 */

public abstract class BaseDialogFragment extends AppCompatDialogFragment {

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());

        Window window = dialog.getWindow();
        if (window != null) {
            configWindow(window);
        }

        ViewGroup parentView = dialog.getWindow() != null ? (ViewGroup) dialog.getWindow().getDecorView() : null;
        View contentView = provideDialogContentView(parentView);
        dialog.setContentView(contentView);
        initViews(contentView);

        configDialogSize(dialog);
        return dialog;
    }

    protected abstract View provideDialogContentView(ViewGroup parentView);

    protected abstract void initViews(View dialogContentView);

    protected void configWindow(Window window) {
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (enableWindowAnim()) {
            window.setWindowAnimations(provideWindowAnimations());
        }
    }

    protected boolean enableWindowAnim() {
        return true;
    }

    @StyleRes
    protected int provideWindowAnimations() {
        return R.style.DialogAnimPushIn;
    }

    protected void configDialogSize(Dialog dialog) {
        DialogUtils.configDialogSize(dialog, 5f / 6f);
    }

}
