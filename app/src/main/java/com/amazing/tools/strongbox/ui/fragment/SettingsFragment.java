package com.amazing.tools.strongbox.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.View;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.ui.activity.LotteryActivity;
import com.amazing.tools.strongbox.ui.dialog.SetLockPinDialogFragment;

/**
 * <ul>
 * <li> Authoer: AvatarQing </li>
 * <li> Date: 2017/7/22 </li>
 * </ul>
 */

public class SettingsFragment extends PreferenceFragmentCompat {

    public static final String CHANGE_NUMBER_PASSWORD = "change_number_password";
    public static final String DRAW_LOTTERY = "draw_lottery";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findPreference(CHANGE_NUMBER_PASSWORD).setOnPreferenceClickListener(mPrefClickListener);
        findPreference(DRAW_LOTTERY).setOnPreferenceClickListener(mPrefClickListener);
    }

    private void showChangePasswordDialog() {
        new SetLockPinDialogFragment()
                .show(getFragmentManager(), "SetLockPinDialogFragment");
    }

    private void goToLotteryPage() {
        startActivity(new Intent(getContext(), LotteryActivity.class));
    }

    private Preference.OnPreferenceClickListener mPrefClickListener = preference -> {
        switch (preference.getKey()) {
            case CHANGE_NUMBER_PASSWORD:
                showChangePasswordDialog();
                return true;
            case DRAW_LOTTERY:
                goToLotteryPage();
                return true;
            default:
                return false;
        }
    };

}
