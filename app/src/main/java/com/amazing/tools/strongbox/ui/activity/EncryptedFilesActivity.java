package com.amazing.tools.strongbox.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.ui.fragment.EncryptedFileOperationPanelFragment;
import com.amazing.tools.strongbox.ui.fragment.EncryptedFilesFragment;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.utils.FilePickUtils;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.entityhelper.FileItemFactory;
import com.amazing.utility.multichoice.ChoiceModeHelper;
import com.github.clans.fab.FloatingActionMenu;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * @author LiuQing
 */
public class EncryptedFilesActivity extends BaseAppCompatActivity implements ChoiceModeHelper.MultiChoiceModeListener {
    private static final String TAG = EncryptedFilesActivity.class.getSimpleName();
    private static final String EXTRA_ALBUM = "EXTRA_ALBUM";
    private static final int REQUEST_CODE_PICK_FILE = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab_menu)
    FloatingActionMenu fabMenu;
    @BindView(R.id.sliding_up_panel)
    SlidingUpPanelLayout slidingUpPanel;
    @BindView(R.id.btn_clear_files)
    Button btnClearFiles;
    @BindView(R.id.btn_clear_files_divider)
    View btnClearFilesDivider;

    private EncryptedFileUIHelper mEncryptedFileUIHelper;
    private EncryptedFileAlbum mAlbum;
    private EncryptedFilesFragment mFilesFragment;

    public static void launch(Activity activity, EncryptedFileAlbum album) {
        activity.startActivity(
                new Intent(activity, EncryptedFilesActivity.class)
                        .putExtra(EXTRA_ALBUM, album)
        );
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypted_files);
        parseArguments();
        initHelpers();
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PICK_FILE: {
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                    String filePath = FilePickUtils.getPath(this, data.getData());
                    Timber.tag(TAG).d("Selected file path:" + filePath);
                    FileItem fileItem = FileItemFactory.parseFileItemFromPath(filePath);
                    if (fileItem != null) {
                        ArrayList<FileItem> fileItems = new ArrayList<>();
                        fileItems.add(fileItem);
                        mEncryptedFileUIHelper.showEncryptConfirmDialog(fileItems, mAlbum);
                    } else {
                        Toast.makeText(this, R.string.prompt_file_does_not_exist, Toast.LENGTH_LONG).show();
                    }
                }
            }
            break;
            default:
                break;
        }
    }

    private void parseArguments() {
        mAlbum = getIntent().getParcelableExtra(EXTRA_ALBUM);
    }

    private void initHelpers() {
        mEncryptedFileUIHelper = new EncryptedFileUIHelper(this, this);
    }

    private void initViews() {
        ButterKnife.bind(this);
        initTitleBar();
        initFab();
        initClearButton();
        initFragments();
        initFileOpPanel();
    }

    private void initTitleBar() {
        toolbar.setTitle(getTitle());
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        toolbar.setTitle(mAlbum.name);
    }

    private void initFab() {
        if (mAlbum.type == EncryptedFileAlbum.TYPE_RECYCLE_BIN) {
            fabMenu.hideMenu(false);
        } else {
            fabMenu.showMenu(false);
        }
        fabMenu.setClosedOnTouchOutside(true);
    }

    private void initClearButton() {
        boolean shownClearButton = mAlbum.type == EncryptedFileAlbum.TYPE_RECYCLE_BIN;
        btnClearFiles.setVisibility(shownClearButton ? View.VISIBLE : View.GONE);
        btnClearFilesDivider.setVisibility(shownClearButton ? View.VISIBLE : View.GONE);
    }

    private void initFragments() {
        EncryptedFileOperationPanelFragment fileOpPanelFragment = new EncryptedFileOperationPanelFragment();
        fileOpPanelFragment.setPanelLayout(slidingUpPanel);
        fileOpPanelFragment.setEncryptedFileUiHelper(mEncryptedFileUIHelper);
        fileOpPanelFragment.setAlbum(mAlbum);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.sliding_up_panel_frame, fileOpPanelFragment, EncryptedFileOperationPanelFragment.TAG)
                .commit();

        EncryptedFilesFragment fileListFragment = EncryptedFilesFragment.newInstance(mAlbum);
        fileListFragment.setEncryptedFileUiHelper(mEncryptedFileUIHelper);
        fileListFragment.setAdapterDataObserver(mFilesDataObserver);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fileListFragment, EncryptedFilesFragment.TAG)
                .commit();
        mFilesFragment = fileListFragment;
    }

    private void initFileOpPanel() {
        slidingUpPanel.addPanelSlideListener(new SlidingUpPanelLayout.SimplePanelSlideListener() {
            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                switch (newState) {
                    case COLLAPSED:
                        fabMenu.showMenuButton(true);
                        break;
                    case EXPANDED:
                        fabMenu.hideMenuButton(true);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @OnClick(R.id.fab_encrypt_image)
    void selectImageToEncrypt() {
        SelectMediaActivity.launch(this, mAlbum, FileTypeUtils.TYPE_IMAGE);
        fabMenu.close(true);
    }

    @OnClick(R.id.fab_encrypt_video)
    void selectVideoToEncrypt() {
        SelectMediaActivity.launch(this, mAlbum, FileTypeUtils.TYPE_VIDEO);
        fabMenu.close(true);
    }

    @OnClick(R.id.fab_encrypt_file)
    void selectFileToEncrypt() {
        FilePickUtils.pickFile(this, REQUEST_CODE_PICK_FILE);
        fabMenu.close(true);
    }

    @OnClick(R.id.btn_clear_files)
    void clearFiles() {
        List<EncryptedFileInfo> dataList = mFilesFragment.getDataList();
        if (dataList == null || dataList.isEmpty()) {
            return;
        }
        mEncryptedFileUIHelper.showConfirmDeleteFileDialog(new ArrayList<>(dataList));
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        fabMenu.hideMenuButton(true);
        changeStatusBarColorGradually(ContextCompat.getColor(getApplicationContext(), R.color.colorAccentDark));
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        fabMenu.showMenuButton(true);
        changeStatusBarColorGradually(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
    }

    private RecyclerView.AdapterDataObserver mFilesDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            btnClearFiles.setEnabled(!mFilesFragment.getDataList().isEmpty());
        }
    };

}