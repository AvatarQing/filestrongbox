package com.amazing.tools.strongbox.ui.activity;

import android.animation.ValueAnimator;
import android.os.Build;
import android.support.annotation.ColorInt;

import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

/**
 * Created by lq on 2017/6/24.
 */

public class BaseAppCompatActivity extends RxAppCompatActivity {

    public void updateStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    public void changeStatusBarColorGradually(@ColorInt int targetColor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int oldColor = getWindow().getStatusBarColor();
            ValueAnimator animator = ValueAnimator.ofArgb(oldColor, targetColor);
            animator.setDuration(300);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    updateStatusBarColor((Integer) animation.getAnimatedValue());
                }
            });
            animator.start();
        }
    }

    protected String getLogTag() {
        return getClass().getSimpleName();
    }

}
