package com.amazing.tools.strongbox.contentobserver;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.AppIconHelper;

import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * @author LiuQing
 */
public class ImageContentObserver extends ContentObserver {

    private static final String TAG = ImageContentObserver.class.getSimpleName();

    private static final int NOTIFICATION_ID_ENCRYPT_NEW_IMAGE = 0x10;
    private static final int REQUEST_CODE_ENCRYPT_NEW_IMAGE = 0x111;

    private Context mContext;
    private ImageInfo mLastImageInfo;

    public ImageContentObserver(Context context) {
        super(new Handler(Looper.getMainLooper()));
        mContext = context.getApplicationContext();
        mLastImageInfo = queryLatestImage();
    }

    @Override
    public void onChange(boolean selfChange) {
        Timber.tag(TAG).d("onChange() -> selfChange:" + selfChange);

        Single
                .fromCallable(new Callable<ImageInfo>() {
                    @Override
                    public ImageInfo call() throws Exception {
                        return queryLatestImage();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ImageInfo>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull ImageInfo latestImageInfo) {
                        if (mLastImageInfo != null) {
                            // 如果最新查询到的照片添加时间大于之前查询到的照片的添加时间，说明是新增了照片，否则是删除了照片
                            if (latestImageInfo.addedTimeInMilli > mLastImageInfo.addedTimeInMilli) {
                                notifyEncryptNewAddedImage();
                            }
                        }
                        mLastImageInfo = latestImageInfo;
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    /**
     * 向通知栏发送一个通知，通知用户是否需要加密新增的照片
     */
    private void notifyEncryptNewAddedImage() {
        // TODO: 2017/9/7 得跳转到合适的类，待优化
        Intent intent = new Intent()
                .setComponent(new ComponentName(mContext.getPackageName(), AppIconHelper.getDisguiseIconLaunchActivityPath()))
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, REQUEST_CODE_ENCRYPT_NEW_IMAGE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // FIXME: 2017/9/7 不出现ticker了，不知道怎么回事
        Notification notification = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_lock)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                .setTicker(mContext.getString(R.string.prompt_do_you_want_encrypt_new_image))
                .setContentTitle(mContext.getString(R.string.title_tips))
                .setContentText(mContext.getString(R.string.prompt_do_you_want_encrypt_new_image))
                .setOngoing(false)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManagerCompat nm = NotificationManagerCompat.from(mContext);
        nm.notify(NOTIFICATION_ID_ENCRYPT_NEW_IMAGE, notification);
    }

    private ImageInfo queryLatestImage() {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String limit = "LIMIT 1";
        String sortOrder = MediaStore.Images.ImageColumns.DATE_ADDED + " DESC " + limit;
        Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, sortOrder);

        if (cursor != null && cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
            int dateAddedIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATE_ADDED);

            ImageInfo imageInfo = new ImageInfo();
            imageInfo.imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cursor.getLong(idIndex));
            imageInfo.addedTimeInMilli = cursor.getLong(dateAddedIndex);

            cursor.close();

            return imageInfo;
        } else {
            return null;
        }
    }

    private static class ImageInfo {
        long addedTimeInMilli;
        Uri imageUri;

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ImageInfo) {
                ImageInfo another = (ImageInfo) obj;
                return this.imageUri.equals(another.imageUri);
            } else {
                return false;
            }
        }
    }

}