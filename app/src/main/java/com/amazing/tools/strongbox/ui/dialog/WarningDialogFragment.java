package com.amazing.tools.strongbox.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Author: LiuQing
 * Date: 2017/7/12
 */

public class WarningDialogFragment extends BaseDialogFragment {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_message)
    TextView tv_message;

    private String mTitle;
    private String mMessage;
    private DialogInterface.OnClickListener mOnOkButtonClickListener;

    private void initFromBuilder(Builder builder) {
        mOnOkButtonClickListener = builder.onOkButtonClickListener;
        mTitle = builder.title;
        mMessage = builder.message;
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getContext()).inflate(R.layout.dialog_warning, parentView, false);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);
        tv_title.setText(mTitle);
        tv_message.setText(mMessage);
    }

    @OnClick(R.id.btn_cancel)
    void onCancelButtonClicked() {
        dismiss();
    }

    @OnClick(R.id.btn_ok)
    void onOkButtonClicked() {
        if (mOnOkButtonClickListener != null) {
            mOnOkButtonClickListener.onClick(getDialog(), Dialog.BUTTON_POSITIVE);
        }
    }

    public static class Builder {
        String title;
        String message;
        DialogInterface.OnClickListener onOkButtonClickListener;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setOnOkButtonClickListener(DialogInterface.OnClickListener onOkButtonClickListener) {
            this.onOkButtonClickListener = onOkButtonClickListener;
            return this;
        }

        public WarningDialogFragment build() {
            WarningDialogFragment dialogFragment = new WarningDialogFragment();
            dialogFragment.initFromBuilder(this);
            return dialogFragment;
        }
    }

}