package com.amazing.tools.strongbox.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.amazing.tools.strongbox.typedef.FileType;

/**
 * @author LiuQing
 */
public class FileItem implements Parcelable {
    /**
     * 图片名称
     */
    public String name;
    /**
     * 图片路径
     */
    public String path;
    /**
     * MIME 类型
     */
    public String mimeType;
    /**
     * 文件大小，单位：字节
     */
    public long size;
    /**
     * 文件修改时间，单位：秒
     */
    public long modifiedTime;
    /**
     * 文件类型，如图片、音频、视频
     */
    @FileType
    public int fileType;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FileItem) {
            FileItem another = (FileItem) obj;
            return TextUtils.equals(this.path, another.path);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "FileItem{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", size=" + size +
                ", modifiedTime=" + modifiedTime +
                ", fileType=" + fileType +
                '}';
    }

    public FileItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.path);
        dest.writeString(this.mimeType);
        dest.writeLong(this.size);
        dest.writeLong(this.modifiedTime);
        dest.writeInt(this.fileType);
    }

    protected FileItem(Parcel in) {
        this.name = in.readString();
        this.path = in.readString();
        this.mimeType = in.readString();
        this.size = in.readLong();
        this.modifiedTime = in.readLong();
        this.fileType = in.readInt();
    }

    public static final Creator<FileItem> CREATOR = new Creator<FileItem>() {
        @Override
        public FileItem createFromParcel(Parcel source) {
            return new FileItem(source);
        }

        @Override
        public FileItem[] newArray(int size) {
            return new FileItem[size];
        }
    };
}