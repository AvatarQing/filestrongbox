package com.amazing.tools.strongbox.exception;

import com.amazing.tools.strongbox.entity.EncryptedFileInfo;

/**
 * @author AvatarQing
 */
public class DecryptFileException extends Exception {

    public DecryptFileException(String message) {
        super(message);
    }

    public DecryptFileException(EncryptedFileInfo fileInfo) {
        super("Failed to decrypt file. " + fileInfo.toString());
    }

}
