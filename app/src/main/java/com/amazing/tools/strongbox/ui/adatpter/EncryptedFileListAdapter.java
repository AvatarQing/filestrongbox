package com.amazing.tools.strongbox.ui.adatpter;

import android.content.Context;
import android.text.format.Formatter;
import android.view.ViewGroup;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.ui.widget.FileThumbnailView;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.utility.multichoice.ChoiceModeHelper;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

/**
 * @author LiuQing
 */
public class EncryptedFileListAdapter extends BaseQuickAdapter<EncryptedFileInfo, BaseViewHolder> {

    private ChoiceModeHelper mChoiceModeHelper;

    public EncryptedFileListAdapter() {
        super(R.layout.item_file);
    }

    @Override
    protected BaseViewHolder createBaseViewHolder(ViewGroup parent, int layoutResId) {
        BaseViewHolder holder = super.createBaseViewHolder(parent, layoutResId);
        holder.addOnClickListener(R.id.thumbnail);
        holder.addOnClickListener(R.id.btn_more);
        return holder;
    }

    @Override
    protected void convert(BaseViewHolder helper, EncryptedFileInfo item) {
        Context context = helper.itemView.getContext();
        int position = helper.getAdapterPosition();
        boolean checked = mChoiceModeHelper != null && mChoiceModeHelper.isItemChecked(position);

        // 背景色
        updateSelectedBgUI(helper, checked);

        // 图标
        updateThumbnailUI(helper, checked);

        // 名称
        helper.setText(R.id.file_name, item.originalFileName);

        // 文件大小
        helper.setText(R.id.file_size, Formatter.formatFileSize(context, item.fileSize));
    }

    private void updateThumbnailUI(BaseViewHolder helper, boolean checked) {
        int position = helper.getAdapterPosition();
        EncryptedFileInfo item = getItem(position);

        FileThumbnailView thumbnailView = helper.getView(R.id.thumbnail);
        if (checked) {
            thumbnailView.showAsCheckBox();
        } else {
            if (item == null) {
                thumbnailView.showAsFile();
            } else {
                switch (item.fileType) {
                    case FileTypeUtils.TYPE_IMAGE:
                        thumbnailView.showAsCircleImage(item.encryptedFilePath);
                        break;
                    case FileTypeUtils.TYPE_VIDEO:
                        thumbnailView.showAsCircleImage(item.thumbnailFilePath);
                        break;
                    case FileTypeUtils.TYPE_AUDIO:
                    case FileTypeUtils.TYPE_FILE:
                    case FileTypeUtils.TYPE_TEXT:
                    default:
                        thumbnailView.showAsFile();
                        break;
                }
            }
        }
    }

    private void updateSelectedBgUI(BaseViewHolder helper, boolean checked) {
        helper.setVisible(R.id.selected_bg, checked);
    }

    public void setChoiceModeHelper(ChoiceModeHelper choiceModeHelper) {
        this.mChoiceModeHelper = choiceModeHelper;
    }

    public void setItemChecked(int position, boolean checked) {
        BaseViewHolder holder = (BaseViewHolder) getRecyclerView().findViewHolderForAdapterPosition(position);
        if (holder != null) {
            updateThumbnailUI(holder, checked);
            updateSelectedBgUI(holder, checked);
        }
    }

}