package com.amazing.tools.strongbox.utils.imageloader.glide;

import android.support.annotation.NonNull;

import com.amazing.tools.strongbox.utils.encrypt.CipherManager;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;

/**
 * @author AvatarQing
 */
public class ConcealImageDataFetcher implements DataFetcher<InputStream> {

    private final String mFilePath;
    private volatile boolean mIsCanceled;
    private InputStream mInputStream;

    public ConcealImageDataFetcher(String filePath) {
        mFilePath = filePath;
    }

    @Override
    public void loadData(Priority priority, DataCallback<? super InputStream> callback) {
        if (mIsCanceled) {
            return;
        }
        try {
            Cipher cipher = CipherManager.getEncryptCipher(Cipher.DECRYPT_MODE);
            mInputStream = new CipherInputStream(new FileInputStream(new File(mFilePath)), cipher);
            callback.onDataReady(mInputStream);
        } catch (Exception e) {
            e.printStackTrace();
            callback.onLoadFailed(e);
        }
    }

    @Override
    public void cleanup() {
        if (mInputStream != null) {
            try {
                mInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mInputStream = null;
            }
        }
    }

    @Override
    public void cancel() {
        mIsCanceled = true;
    }

    @NonNull
    @Override
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @NonNull
    @Override
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
