package com.amazing.tools.strongbox.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.disguise.protocol.DisguiseManager;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.utils.FilePickUtils;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;
import com.amazing.tools.strongbox.utils.entityhelper.FileItemFactory;
import com.amazing.tools.strongbox.utils.rx.MapWithIndex;
import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.codetail.animation.ViewAnimationUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * @author AvatarQing
 */

public class ReceiveFileToEncryptActivity extends BaseAppCompatActivity {

    private static final String TAG = "ReceiveFileToEncryptActivity";

    @BindView(R.id.tv_encrypt_state)
    TextView mEncryptStateView;
    @BindView(R.id.tv_encrypt_result)
    TextView mEncryptResultView;
    @BindView(R.id.encrypt_text_progress)
    TextView mTextProgress;
    @BindView(R.id.btn_open_app)
    TextView mBtnOpenApp;
    @BindView(R.id.encrypt_finish_view)
    View mEncryptFinishView;
    @BindView(R.id.encrypt_progress_view)
    View mEncryptProgressView;
    @BindView(R.id.ic_file)
    View mFileIconView;
    @BindView(R.id.ic_safe_box)
    View mSafeBoxIconView;
    @BindView(R.id.iv_state_tip)
    ImageView mEncryptStateIcon;

    private EncryptedFileUIHelper mEncryptUIHelper;
    private Animator mMoveFileAnimator;
    private long mEncryptConsumeTime = 0;
    private ArrayList<Uri> mUris;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_file_to_encrypt);
        ButterKnife.bind(this);
        initViews();
        handleIntent(getIntent());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        // Get intent, action and MIME type
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
            ArrayList<Uri> uris = new ArrayList<>();
            uris.add(uri);
            encryptFiles(uris);
            mUris = uris;
            Timber.tag(TAG).d(uri != null ? uri.toString() : "null uri");
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            ArrayList<Uri> uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
            encryptFiles(uris);
            mUris = uris;
            Timber.tag(TAG).d(uris != null ? uris.toString() : "null uris");
        }
    }

    private void initViews() {
        mEncryptUIHelper = new EncryptedFileUIHelper(this, this);
        mBtnOpenApp.setText(getString(R.string.format_open, getString(R.string.app_name)));
        mEncryptProgressView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                startMoveFileToSafeBoxAnim();
                mEncryptProgressView.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });
    }

    @OnClick(R.id.btn_open_app)
    void enterApp() {
        DisguiseManager.get().startDisguiseActivity(this, false);
        finish();
    }

    private void encryptFiles(ArrayList<Uri> uris) {
        if (uris == null) {
            return;
        }
        final int fileCount = uris.size();
        EncryptHelper encryptHelper = EncryptHelper.getInstance();
        if (encryptHelper.hasEnoughEncryptCount(fileCount)) {
            Observable.fromIterable(uris)
                    .map(uri -> FilePickUtils.getPath(this, uri))
                    .map(FileItemFactory::parseFileItemFromPath)
                    .compose(MapWithIndex.instance())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(fileItemIndexed -> mTextProgress.setText(getString(R.string.format_encrypting_files, fileItemIndexed.index() + 1, fileCount)))
                    .observeOn(Schedulers.io())
                    .doOnNext(fileItemIndexed -> {
                        long encryptStartTime = System.currentTimeMillis();
                        EncryptHelper.getInstance().encryptFile(getApplicationContext(), fileItemIndexed.value(), null);
                        mEncryptConsumeTime += System.currentTimeMillis() - encryptStartTime;
                    })
                    .compose(bindToLifecycle())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<MapWithIndex.Indexed<FileItem>>() {
                        private long totalFileSize = 0;

                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(MapWithIndex.Indexed<FileItem> fileItemIndexed) {
                            totalFileSize += fileItemIndexed.value().size;
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            updateFinishHeaderUi(false);
                            hideProgressViewAndShowFinishView();
                        }

                        @Override
                        public void onComplete() {
                            updateFinishHeaderUi(true);

                            String formatFileSizeText = Formatter.formatFileSize(getApplicationContext(), totalFileSize);
                            String consumingTimeText = getString(R.string.format_encrypted_consuming_time, formatFileSizeText, mEncryptConsumeTime);
                            mEncryptResultView.setText(consumingTimeText);

                            final long minimumProgressAnimShowTime = 2000;
                            if (mEncryptConsumeTime > minimumProgressAnimShowTime) {
                                hideProgressViewAndShowFinishView();
                            } else {
                                Observable.timer(minimumProgressAnimShowTime - mEncryptConsumeTime, TimeUnit.MILLISECONDS)
                                        .compose(bindUntilEvent(ActivityEvent.DESTROY))
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(aLong -> hideProgressViewAndShowFinishView());
                            }
                        }
                    });
        } else {
            mEncryptUIHelper.showEncryptCountNotEnoughDialog();
        }
    }

    private void updateFinishHeaderUi(boolean encryptSuccess) {
        @ColorInt int headerBgColor;
        @DrawableRes int tipIconResId;
        String stateText;
        if (encryptSuccess) {
            stateText = getString(R.string.prompt_all_files_encrypted);
            headerBgColor = ContextCompat.getColor(this, R.color.colorSuccessDark);
            tipIconResId = R.drawable.ic_done;
        } else {
            stateText = getString(R.string.prompt_encrypt_failed);
            headerBgColor = ContextCompat.getColor(this, R.color.colorWarn);
            tipIconResId = R.drawable.ic_error;
        }
        mEncryptStateView.setText(stateText);
        mEncryptStateIcon.setImageResource(tipIconResId);
        mEncryptFinishView.setBackgroundColor(headerBgColor);
    }

    private void startMoveFileToSafeBoxAnim() {
        int fileIconCenterX = mFileIconView.getLeft() + mFileIconView.getWidth() / 2;
        int safeBoxIconCenterX = mSafeBoxIconView.getLeft() + mSafeBoxIconView.getWidth() / 2;
        int xMoveDistance = safeBoxIconCenterX - fileIconCenterX;
        int extraYDistance = (int) ViewUtils.dp2px(this, 48f);
        int yMoveDistance = mFileIconView.getTop() + mFileIconView.getHeight() / 2 - mSafeBoxIconView.getTop() + extraYDistance;

        ObjectAnimator moveXAnim = ObjectAnimator.ofFloat(mFileIconView, View.TRANSLATION_X, xMoveDistance);
        moveXAnim.setDuration(1000);
        moveXAnim.setInterpolator(new LinearInterpolator());

        ObjectAnimator moveYAnim1 = ObjectAnimator.ofFloat(mFileIconView, View.TRANSLATION_Y, -yMoveDistance);
        moveYAnim1.setDuration(1000);
        moveYAnim1.setInterpolator(new DecelerateInterpolator());

        ObjectAnimator moveYAnim2 = ObjectAnimator.ofFloat(mFileIconView, View.TRANSLATION_Y, 0);
        moveYAnim2.setDuration(1000);
        moveYAnim2.setInterpolator(new AccelerateInterpolator());

        AnimatorSet set = new AnimatorSet();
        set.play(moveXAnim).with(moveYAnim1).before(moveYAnim2);
        set.start();
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animation.setStartDelay(500);
                animation.start();
            }
        });
        mMoveFileAnimator = set;
    }

    private Animator getEncryptFinishCircleRevealAnim() {
        final View revealView = mEncryptFinishView;
        float finalRadius = (float) Math.hypot(revealView.getWidth() / 2f, revealView.getHeight());
        int startX = revealView.getWidth() / 2;
        int startY = revealView.getHeight();
        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(revealView, startX, startY, 0, finalRadius);

        revealAnimator.setDuration(400);
        revealAnimator.setStartDelay(200);
        revealAnimator.setInterpolator(new FastOutLinearInInterpolator());
        revealAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                revealView.setVisibility(View.VISIBLE);
            }
        });
        return revealAnimator;
    }

    private void hideProgressViewAndShowFinishView() {
        if (mMoveFileAnimator != null) {
            mMoveFileAnimator.cancel();
            mMoveFileAnimator = null;
        }
        ObjectAnimator hideProgressViewAnim = ViewPropertyObjectAnimator.animate(mEncryptProgressView)
                .translationY(ViewUtils.dp2px(this, 24f))
                .alpha(0)
                .setDuration(400)
                .setStartDelay(400)
                .get();
        ObjectAnimator btnOpenAppAnim = ViewPropertyObjectAnimator.animate(mBtnOpenApp)
                .alpha(1)
                .scaleX(1)
                .scaleY(1)
                .setDuration(800)
                .setInterpolator(new BounceInterpolator())
                .addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        mBtnOpenApp.setVisibility(View.VISIBLE);
                    }
                })
                .get();
        Animator showFinishViewAnim = getEncryptFinishCircleRevealAnim();
        AnimatorSet set = new AnimatorSet();
        set.playSequentially(hideProgressViewAnim, showFinishViewAnim, btnOpenAppAnim);
        set.start();
    }
}