package com.amazing.tools.strongbox.entity;

import com.amazing.tools.strongbox.db.EncryptedFilesDatabase;
import com.raizlabs.android.dbflow.annotation.QueryModel;

/**
 * @author AvatarQing
 */
@QueryModel(database = EncryptedFilesDatabase.class, allFields = true)
public class AlbumQueryModel {
    public long id;
    public String name;
    public int type;
    public long createdTime;

    public long file_id;
    public String file_originalFileName;
    public String file_originalFilePath;
    public String file_encryptedFileName;
    public String file_encryptedFilePath;
    public String file_mimeType;
    public String file_thumbnailFilePath;
    public long file_fileSize;
    public long file_modifiedTime;
    public long file_encryptedTime;
    public int file_fileType;
    public int fileCountInAlbum;
}
