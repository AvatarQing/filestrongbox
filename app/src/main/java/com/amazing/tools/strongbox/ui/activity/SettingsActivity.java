package com.amazing.tools.strongbox.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.amazing.tools.ad.AdmobLogAdListener;
import com.amazing.tools.ad.FacebookLogAdListener;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.ColorUtils;
import com.facebook.ads.Ad;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdViewAttributes;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <ul>
 * <li> Authoer: AvatarQing </li>
 * <li> Date: 2017/7/21 </li>
 * </ul>
 */

public class SettingsActivity extends BaseAppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initTooBar();
        initAd();
    }

    private void initAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_in_settings_ad_id));
        interstitialAd.setAdListener(new AdmobLogAdListener("InterstitialAd-Settings") {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }
        });
        interstitialAd.loadAd(new AdRequest.Builder().build());

        ViewGroup adContainerView = findViewById(R.id.ad_container);
        NativeAd nativeAd = new NativeAd(getApplicationContext(), getString(R.string.fb_native_ad_id));
        nativeAd.setAdListener(new FacebookLogAdListener("NativeAd-Settings") {
            @Override
            public void onAdLoaded(Ad ad) {
                super.onAdLoaded(ad);
                NativeAdViewAttributes viewAttributes = new NativeAdViewAttributes()
                        .setBackgroundColor(ColorUtils.getColorFromAttr(getApplicationContext(), R.attr.background))
                        .setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white100))
                        .setDescriptionTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white50))
                        .setButtonTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white100))
                        .setButtonBorderColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryLight));
                View adView = NativeAdView.render(getApplicationContext(), nativeAd, NativeAdView.Type.HEIGHT_100, viewAttributes);
                adContainerView.removeAllViews();
                adContainerView.addView(adView);
            }
        });
        nativeAd.loadAd();
    }

    private void initTooBar() {
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.title_settings);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

}
