package com.amazing.tools.strongbox.ui.behavior;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import com.github.clans.fab.FloatingActionMenu;

/**
 * 参考链接
 * <ol>
 * <li><a href="https://github.com/Clans/FloatingActionButton/issues/299">Issue: FloatingActionButton/Menu hide on ListView scroll</a></li>
 * <li><a href="https://stackoverflow.com/a/35904421/2011291">StackOverflow: How to move a view above SnackBar just like FloatingButton</a></li>
 * </ol>
 *
 * @author LiuQing
 */
public class FabMenuBehavior extends CoordinatorLayout.Behavior<FloatingActionMenu> {

    private int accumulator = 0;
    private int threshold = 0;

    public FabMenuBehavior() {
        super();
    }

    public FabMenuBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionMenu child, View directTargetChild, View target, int nestedScrollAxes) {
        threshold = (child.getChildCount() > 0 ? child.getChildAt(0).getHeight() : child.getHeight()) / 2;
        return true;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionMenu child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        if ((accumulator * dyConsumed) < 0) { //scroll direction change
            accumulator = 0;
        }
        accumulator += dyConsumed;

        if (accumulator > threshold && !child.isMenuButtonHidden()) {
            child.hideMenuButton(true);
        } else if (accumulator < -threshold && child.isMenuButtonHidden()) {
            child.showMenuButton(true);
        }
    }

    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionMenu child, View target) {
        super.onStopNestedScroll(coordinatorLayout, child, target);
        accumulator = 0;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionMenu child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionMenu child, View dependency) {
        float translationY = Math.min(0, ViewCompat.getTranslationY(dependency) - dependency.getHeight());
        ViewCompat.setTranslationY(child, translationY);
        return true;
    }

    @Override
    public void onDependentViewRemoved(CoordinatorLayout parent, FloatingActionMenu child, View dependency) {
        ViewCompat.animate(child).translationY(0).start();
    }

}