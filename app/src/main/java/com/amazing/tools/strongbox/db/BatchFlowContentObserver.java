package com.amazing.tools.strongbox.db;

import com.raizlabs.android.dbflow.runtime.FlowContentObserver;

/**
 * @author AvatarQing
 */

public class BatchFlowContentObserver extends FlowContentObserver {
    private static volatile BatchFlowContentObserver sObserver;

    public static BatchFlowContentObserver getInstance() {
        if (sObserver == null) {
            synchronized (BatchFlowContentObserver.class) {
                if (sObserver == null) {
                    sObserver = new BatchFlowContentObserver();
                }
            }
        }
        return sObserver;
    }
}