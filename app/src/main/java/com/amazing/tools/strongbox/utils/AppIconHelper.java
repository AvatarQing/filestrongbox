package com.amazing.tools.strongbox.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;

import com.amazing.tools.strongbox.ui.activity.LaunchActivity;

/**
 * @author LiuQing
 */
public class AppIconHelper {

    public static void changeAppIconToDisguise(Context context) {
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(new ComponentName(context, getStringBoxIconLaunchActivityPath()),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        pm.setComponentEnabledSetting(new ComponentName(context, getDisguiseIconLaunchActivityPath()),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }

    public static String getStringBoxIconLaunchActivityPath() {
        return LaunchActivity.class.getName().replace(LaunchActivity.class.getSimpleName(), "StringBoxIconLaunchActivity");
    }

    public static String getDisguiseIconLaunchActivityPath() {
        return LaunchActivity.class.getName().replace(LaunchActivity.class.getSimpleName(), "DisguiseIconLaunchActivity");
    }

}
