package com.amazing.tools.strongbox.utils;

import com.amazing.tools.strongbox.typedef.FileType;

import java.io.File;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/23 </li>
 * </ul>
 */

public class FileTypeUtils {

    public static final int TYPE_FILE = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_AUDIO = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_TEXT = 4;

    @FileType
    public static int getFileType(File file) {
        String mimeType = MimeTypeUtils.getMimeType(file);
        return getFileType(mimeType);
    }

    @FileType
    public static int getFileType(String mimeType) {
        if (mimeType != null) {
            if (MimeTypeUtils.isImage(mimeType)) {
                return TYPE_IMAGE;
            } else if (MimeTypeUtils.isAudio(mimeType)) {
                return TYPE_AUDIO;
            } else if (MimeTypeUtils.isVideo(mimeType)) {
                return TYPE_VIDEO;
            } else if (MimeTypeUtils.isText(mimeType)) {
                return TYPE_TEXT;
            }
        }
        return TYPE_FILE;
    }

}
