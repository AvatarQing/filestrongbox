package com.amazing.tools.strongbox.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.amazing.tools.strongbox.disguise.protocol.DisguiseManager;
import com.amazing.tools.strongbox.utils.LockPinHelper;
import com.amazing.tools.strongbox.utils.data.Keeper;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/7/31 </li>
 * </ul>
 */

public class LaunchActivity extends FragmentActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (LockPinHelper.get().hasSetPin()) {
            DisguiseManager.get().startDisguiseActivity(this, false);
        } else if (Keeper.get().needShowIntroPage()) {
            startActivity(
                    new Intent(this, IntroActivity.class)
            );
        } else {
            startActivity(
                    new Intent(this, MainActivity.class)
            );
        }
        finish();
    }
}
