package com.amazing.tools.strongbox.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.ui.adatpter.EncryptedFileListAdapter;
import com.amazing.tools.strongbox.ui.helper.EncryptedFileUIHelper;
import com.amazing.tools.strongbox.utils.ColorUtils;
import com.amazing.tools.strongbox.utils.FeatureDiscoveryUtils;
import com.amazing.tools.strongbox.utils.RateHelper;
import com.amazing.utility.multichoice.ChoiceModeHelper;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/24 </li>
 * </ul>
 */

public class EncryptedFilesFragment extends BaseFragment {

    private static final String EXTRA_ALBUM = "EXTRA_ALBUM";
    public static final String TAG = EncryptedFilesFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private EncryptedFileListAdapter mAdapter;

    private ChoiceModeHelper mChoiceModeHelper;
    private EncryptedFileUIHelper mEncryptedFileUIHelper;

    private ChoiceModeHelper.MultiChoiceModeListener mDelegateChoiceModeListener;
    private RecyclerView.AdapterDataObserver mAdapterDataObserver;

    private EncryptedFileAlbum mAlbum;

    public static EncryptedFilesFragment newInstance(EncryptedFileAlbum album) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_ALBUM, album);
        EncryptedFilesFragment fragment = new EncryptedFilesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChoiceModeHelper.MultiChoiceModeListener) {
            mDelegateChoiceModeListener = (ChoiceModeHelper.MultiChoiceModeListener) context;
        }
        parseArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRecyclerView = new RecyclerView(getContext());
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapterDataObserver != null) {
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }
        if (mEncryptedFileUIHelper != null) {
            mEncryptedFileUIHelper.removeOnEncryptCompleteDialogDismissListener(mOnEncryptCompleteDialogDismissListener);
            mEncryptedFileUIHelper.removeOnDecryptCompleteListener(mOnDecryptCompleteListener);
            mEncryptedFileUIHelper.removeOnDeleteCompleteListener(mOnDeleteCompleteListener);
            mEncryptedFileUIHelper.removeOnChangeFilesAlbumListener(mOnChangeFilesAlbumListener);
        }
    }

    public void setEncryptedFileUiHelper(EncryptedFileUIHelper helper) {
        mEncryptedFileUIHelper = helper;
        mEncryptedFileUIHelper.addOnEncryptCompleteDialogDismissListener(mOnEncryptCompleteDialogDismissListener);
        mEncryptedFileUIHelper.addOnDecryptCompleteListener(mOnDecryptCompleteListener);
        mEncryptedFileUIHelper.addOnDeleteCompleteListener(mOnDeleteCompleteListener);
        mEncryptedFileUIHelper.addOnChangeFilesAlbumListener(mOnChangeFilesAlbumListener);
    }

    public List<EncryptedFileInfo> getDataList() {
        return mAdapter.getData();
    }

    public void setAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        this.mAdapterDataObserver = observer;
    }

    private void parseArguments() {
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        mAlbum = args.getParcelable(EXTRA_ALBUM);
    }

    private void initRecyclerView() {
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(lm);

        EncryptedFileListAdapter adapter = new EncryptedFileListAdapter();
        adapter.bindToRecyclerView(mRecyclerView);

        adapter.setEmptyView(R.layout.list_empty_view);
        TextView tvEmptyText = adapter.getEmptyView().findViewById(R.id.empty_text);
        if (mAlbum.type == EncryptedFileAlbum.TYPE_RECYCLE_BIN) {
            tvEmptyText.setText(R.string.prompt_empty_recycle_bin);
        } else {
            tvEmptyText.setText(R.string.prompt_no_encrypt_files);
        }

        adapter.setOnItemClickListener(mOnItemClickListener);
        adapter.setOnItemLongClickListener(mOnItemLongClickListener);
        adapter.setOnItemChildClickListener(mOnItemChildClickListener);
        if (mAdapterDataObserver != null) {
            adapter.registerAdapterDataObserver(mAdapterDataObserver);
        }

        mChoiceModeHelper = new ChoiceModeHelper(mRecyclerView);
        mChoiceModeHelper.setChoiceMode(ChoiceModeHelper.CHOICE_MODE_MULTIPLE_MODAL);
        mChoiceModeHelper.setMultiChoiceModeListener(mMultiChoiceModeListener);
        mChoiceModeHelper.setUpdateOnScreenCheckedViewsCallback(mUpdateOnScreenCheckedViewsCallback);
        adapter.setChoiceModeHelper(mChoiceModeHelper);

        mAdapter = adapter;
    }

    private void loadData() {
        Single<List<EncryptedFileInfo>> dataSource;
        if (mAlbum != null && mAlbum.id >= 0) {
            dataSource = EncryptedFilesDao.loadFilesInAlbum(mAlbum.id);
        } else {
            dataSource = EncryptedFilesDao.loadAllFiles();
        }
        dataSource
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<EncryptedFileInfo>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull List<EncryptedFileInfo> fileInfoList) {
                        Timber.tag(TAG).i("Load encrypted images count: " + fileInfoList.size());
                        mAdapter.replaceData(fileInfoList);
                        delayShowSelectImageFeatureDiscovery();
                        if (!fileInfoList.isEmpty()) {
                            RateHelper.getInstance().showRateDialogAfterEncrypt(getActivity());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        Timber.tag(TAG).w("Load encrypted images failed. " + e.getMessage());
                    }
                });
    }

    private void delayShowSelectImageFeatureDiscovery() {
        if (FeatureDiscoveryUtils.hasShownSelectImageFromVault()) {
            return;
        }
        Observable.timer(800, TimeUnit.MILLISECONDS)
                .compose(bindUntilEvent(FragmentEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        showSelectImageFeatureDiscovery();
                    }
                });
    }

    private void showSelectImageFeatureDiscovery() {
        if (mAdapter.getData().isEmpty()) {
            return;
        }
        View targetView;
        String msg;
        if (mAdapter instanceof EncryptedFileListAdapter) {
            targetView = mRecyclerView.getChildAt(0).findViewById(R.id.thumbnail);
            msg = getString(R.string.prompt_click_to_multiple_select);
        } else {
            targetView = mRecyclerView.getChildAt(0);
            msg = getString(R.string.prompt_multiple_select);
        }
        new TapTargetSequence(getActivity())
                .target(
                        TapTarget.forView(targetView, getString(R.string.title_multiple_select), msg)
                                .outerCircleColorInt(ColorUtils.getColorFromAttr(getContext(), R.attr.colorPrimary))
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.colorPrimaryDark)
                                .textColor(R.color.white100)
                                .dimColor(R.color.black100)
                                .drawShadow(true)
                                .tintTarget(false)
                )
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        FeatureDiscoveryUtils.setShownSelectImageFromVault(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget tapTarget, boolean b) {
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget tapTarget) {
                    }
                })
                .start();
    }

    private ArrayList<EncryptedFileInfo> getSelectedFileList() {
        ArrayList<EncryptedFileInfo> selectedFileList = new ArrayList<>();
        SparseBooleanArray checkedItemPositions = mChoiceModeHelper.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            int positionKey = checkedItemPositions.keyAt(i);
            if (checkedItemPositions.get(positionKey, false)) {
                EncryptedFileInfo item = mAdapter.getItem(positionKey);
                if (item != null) {
                    selectedFileList.add(item);
                }
            }
        }
        return selectedFileList;
    }

    private void showSlidingUpPanel(int clickedItemPosition) {
        EncryptedFileOperationPanelFragment filePanelFragment =
                (EncryptedFileOperationPanelFragment) getActivity().getSupportFragmentManager()
                        .findFragmentByTag(EncryptedFileOperationPanelFragment.TAG);
        if (filePanelFragment != null) {
            filePanelFragment.showSlidingUpPanel(mAdapter.getData(), clickedItemPosition);
        }
    }

    private BaseQuickAdapter.OnItemClickListener mOnItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (mChoiceModeHelper.isInActionMode()) {
                mChoiceModeHelper.performItemClick(view, position);
            } else {
                mEncryptedFileUIHelper.openFile(mAdapter.getItem(position), mAdapter.getData());
            }
        }
    };

    private BaseQuickAdapter.OnItemLongClickListener mOnItemLongClickListener = new BaseQuickAdapter.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
            return mChoiceModeHelper.performItemLongPress(view, position);
        }
    };

    private BaseQuickAdapter.OnItemChildClickListener mOnItemChildClickListener = new BaseQuickAdapter.OnItemChildClickListener() {
        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            switch (view.getId()) {
                case R.id.thumbnail: {
                    if (mChoiceModeHelper.isInActionMode()) {
                        mChoiceModeHelper.performItemClick(view, position);
                    } else {
                        mChoiceModeHelper.setItemChecked(position, true);
                    }
                }
                break;
                case R.id.btn_more: {
                    if (mChoiceModeHelper.isInActionMode()) {
                        mChoiceModeHelper.finishActionMode();
                    }
                    showSlidingUpPanel(position);
                }
                break;
                default:
                    break;
            }
        }
    };

    private ChoiceModeHelper.MultiChoiceModeListener mMultiChoiceModeListener = new ChoiceModeHelper.MultiChoiceModeListener() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.action_mode_enrypted_files, menu);
            if (mDelegateChoiceModeListener != null) {
                boolean handled = mDelegateChoiceModeListener.onCreateActionMode(mode, menu);
                if (handled) {
                    return true;
                }
            }
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            if (mDelegateChoiceModeListener != null) {
                mDelegateChoiceModeListener.onPrepareActionMode(mode, menu);
            }
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (mDelegateChoiceModeListener != null) {
                boolean handled = mDelegateChoiceModeListener.onActionItemClicked(mode, item);
                if (handled) {
                    return true;
                }
            }
            switch (item.getItemId()) {
                case R.id.decrypt: {
                    mEncryptedFileUIHelper.showDecryptConfirmDialog(getSelectedFileList());
                }
                return true;
                case R.id.select_all: {
                    if (mChoiceModeHelper.hasSelectedAll()) {
                        mChoiceModeHelper.deselectAll();
                        item.setTitle(R.string.menu_select_all);
                    } else {
                        mChoiceModeHelper.selectAll();
                        item.setTitle(R.string.menu_deselect_all);
                    }
                }
                return true;
                case R.id.delete: {
                    if (mAlbum.type == EncryptedFileAlbum.TYPE_RECYCLE_BIN) {
                        mEncryptedFileUIHelper.showConfirmDeleteFileDialog(getSelectedFileList());
                    } else {
                        mEncryptedFileUIHelper.showConfirmMoveFileDialog(getSelectedFileList());
                    }
                }
                break;
                case R.id.move: {
                    mEncryptedFileUIHelper.showChangeAlbumDialog(mAlbum, getSelectedFileList());
                }
                return true;
                default:
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            if (mDelegateChoiceModeListener != null) {
                mDelegateChoiceModeListener.onDestroyActionMode(mode);
            }
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            updateSelectedItemsUI(mode, mChoiceModeHelper.getCheckedItemCount());
        }

        private void updateSelectedItemsUI(ActionMode actionMode, int selectedCount) {
            if (actionMode == null) {
                return;
            }
            String title = getString(R.string.format_items_selected, selectedCount);
            actionMode.setTitle(title);
        }
    };

    private ChoiceModeHelper.UpdateOnScreenCheckedViewsCallback mUpdateOnScreenCheckedViewsCallback = new ChoiceModeHelper.UpdateOnScreenCheckedViewsCallback() {
        @Override
        public void onItemUpdate(int position, boolean checked) {
            mAdapter.setItemChecked(position, checked);
        }
    };

    private DialogInterface.OnDismissListener mOnEncryptCompleteDialogDismissListener = dialog -> loadData();

    private EncryptedFileUIHelper.OnDecryptCompleteListener mOnDecryptCompleteListener = () -> {
        loadData();
        mChoiceModeHelper.finishActionMode();
    };

    private EncryptedFileUIHelper.OnDeleteCompleteListener mOnDeleteCompleteListener = (toDeleteFileCount, deletedFileCount) -> {
        loadData();
        mChoiceModeHelper.finishActionMode();
    };

    private EncryptedFileUIHelper.OnChangeFilesAlbumListener mOnChangeFilesAlbumListener = () -> {
        loadData();
        mChoiceModeHelper.finishActionMode();
    };

}
