package com.amazing.tools.strongbox.enums;

/**
 * <ul>
 * <li> Author: LiuQing </li>
 * <li> Date: 2017/7/19 </li>
 * </ul>
 */
public enum FileOpCommand {
    OPEN,
    OPEN_WITH,
    DECRYPT,
    RENAME,
    SHARE,
    DELETE,
    INFO,
    MOVE,
}