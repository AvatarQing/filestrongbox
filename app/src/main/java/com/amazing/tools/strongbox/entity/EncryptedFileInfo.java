package com.amazing.tools.strongbox.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.amazing.tools.strongbox.db.EncryptedFilesDatabase;
import com.amazing.tools.strongbox.typedef.FileType;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

/**
 * Author: LiuQing
 * Date: 2017/7/11
 */

@Table(database = EncryptedFilesDatabase.class, allFields = true)
public class EncryptedFileInfo implements Parcelable {

    @PrimaryKey(autoincrement = true)
    public long id;
    public String originalFileName;
    public String originalFilePath;
    public String encryptedFileName;
    public String encryptedFilePath;
    public String thumbnailFilePath;
    public String mimeType;
    public long fileSize; // Unit:byte
    public long modifiedTime;
    public long encryptedTime;
    @FileType
    public int fileType;
    @ForeignKey(tableClass = EncryptedFileAlbum.class, references = {@ForeignKeyReference(columnName = "albumId", foreignKeyColumnName = "id", notNull = @NotNull)})
    public long albumId;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof EncryptedFileInfo) {
            EncryptedFileInfo another = (EncryptedFileInfo) obj;
            return this.id == another.id;
        }
        return false;
    }

    @Override
    public String toString() {
        return "EncryptedFileInfo{" +
                "id=" + id +
                ", originalFileName='" + originalFileName + '\'' +
                ", originalFilePath='" + originalFilePath + '\'' +
                ", encryptedFileName='" + encryptedFileName + '\'' +
                ", encryptedFilePath='" + encryptedFilePath + '\'' +
                ", thumbnailFilePath='" + thumbnailFilePath + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", fileSize=" + fileSize +
                ", modifiedTime=" + modifiedTime +
                ", encryptedTime=" + encryptedTime +
                ", fileType=" + fileType +
                ", album=" + albumId +
                '}';
    }

    public boolean isValid() {
        return id > 0;
    }

    public EncryptedFileInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.originalFileName);
        dest.writeString(this.originalFilePath);
        dest.writeString(this.encryptedFileName);
        dest.writeString(this.encryptedFilePath);
        dest.writeString(this.mimeType);
        dest.writeLong(this.fileSize);
        dest.writeLong(this.modifiedTime);
        dest.writeLong(this.encryptedTime);
        dest.writeInt(this.fileType);
        dest.writeLong(this.albumId);
    }

    protected EncryptedFileInfo(Parcel in) {
        this.id = in.readLong();
        this.originalFileName = in.readString();
        this.originalFilePath = in.readString();
        this.encryptedFileName = in.readString();
        this.encryptedFilePath = in.readString();
        this.mimeType = in.readString();
        this.fileSize = in.readLong();
        this.modifiedTime = in.readLong();
        this.encryptedTime = in.readLong();
        this.fileType = in.readInt();
        this.albumId = in.readLong();
    }

    public static final Creator<EncryptedFileInfo> CREATOR = new Creator<EncryptedFileInfo>() {
        @Override
        public EncryptedFileInfo createFromParcel(Parcel source) {
            return new EncryptedFileInfo(source);
        }

        @Override
        public EncryptedFileInfo[] newArray(int size) {
            return new EncryptedFileInfo[size];
        }
    };
}
