package com.amazing.tools.strongbox.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.amazing.tools.strongbox.db.EncryptedFilesDatabase;
import com.amazing.tools.strongbox.typedef.EncryptedFileAlbumType;
import com.raizlabs.android.dbflow.annotation.ColumnIgnore;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;

/**
 * @author LiuQing
 */

@Table(database = EncryptedFilesDatabase.class, allFields = true)
public class EncryptedFileAlbum implements Parcelable {
    /**
     * 文件夹类别：主相册
     */
    public static final int TYPE_MAIN = 0;
    /**
     * 文件夹类别：重要资料
     */
    public static final int TYPE_IMPORTANT = 1;
    /**
     * 文件夹类别：视频
     */
    public static final int TYPE_VIDEO = 2;
    /**
     * 文件夹类别：用户自建
     */
    public static final int TYPE_USER_CREATE = 3;
    /**
     * 文件夹类别：回收站
     */
    public static final int TYPE_RECYCLE_BIN = 4;

    public static final int[] CANNOT_DELETE_ALBUM_TYPE = new int[]{
            TYPE_MAIN,
            TYPE_RECYCLE_BIN,
    };

    /**
     * 主键id
     */
    @PrimaryKey(autoincrement = true)
    public long id;

    /**
     * 文件夹名称
     */
    @NotNull
    @Unique
    public String name;

    /**
     * 文件夹类别
     */
    @EncryptedFileAlbumType
    public int type;

    /**
     * 创建时间，单位毫秒
     */
    public long createdTime;

    /**
     * 相册中最新的一个加密文件，为null代表相册为空
     */
    @ColumnIgnore
    public EncryptedFileInfo latestFileInfo;

    /**
     * 相册中包含的加密文件个数
     */
    @ColumnIgnore
    public int fileCount;

    public static EncryptedFileAlbum create(String name, @EncryptedFileAlbumType int type, long createdTime) {
        EncryptedFileAlbum folder = new EncryptedFileAlbum();
        folder.name = name;
        folder.type = type;
        folder.createdTime = createdTime;
        return folder;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof EncryptedFileAlbum) {
            EncryptedFileAlbum another = (EncryptedFileAlbum) obj;
            return this.id == another.id;
        }
        return false;
    }

    @Override
    public String toString() {
        return "EncryptedFileAlbum{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", createdTime=" + createdTime +
                '}';
    }

    public EncryptedFileAlbum() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.type);
        dest.writeLong(this.createdTime);
        dest.writeParcelable(this.latestFileInfo, flags);
        dest.writeInt(this.fileCount);
    }

    protected EncryptedFileAlbum(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.type = in.readInt();
        this.createdTime = in.readLong();
        this.latestFileInfo = in.readParcelable(EncryptedFileInfo.class.getClassLoader());
        this.fileCount = in.readInt();
    }

    public static final Creator<EncryptedFileAlbum> CREATOR = new Creator<EncryptedFileAlbum>() {
        @Override
        public EncryptedFileAlbum createFromParcel(Parcel source) {
            return new EncryptedFileAlbum(source);
        }

        @Override
        public EncryptedFileAlbum[] newArray(int size) {
            return new EncryptedFileAlbum[size];
        }
    };
}