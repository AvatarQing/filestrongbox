package com.amazing.tools.strongbox.utils;

import java.util.concurrent.TimeUnit;

/**
 * @author AvatarQing
 */
public class Const {
    public static final int DEFAULT_MAX_ENCRYPT_COUNT = 100;

    public static final int MAX_AWARD_SPACE = 100;
    public static final int MAX_AWARD_COUNT = 5;
    public static final long NEXT_LOTTERY_PERIOD = TimeUnit.HOURS.toMillis(5);
}
