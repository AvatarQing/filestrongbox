package com.amazing.tools.strongbox.utils;

import android.webkit.MimeTypeMap;

import java.io.File;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/23 </li>
 * </ul>
 */

public class MimeTypeUtils {

    /**
     * @return The MIME type for the given file.
     */
    public static String getMimeType(String filePath) {
        return getMimeType(new File(filePath));
    }

    /**
     * @return The MIME type for the given file.
     */
    public static String getMimeType(File file) {
        String extension = getExtension(file.getName());
        if (extension.length() > 0) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1));
        }
        return "application/octet-stream";
    }

    /**
     * Gets the extension of a file name, like ".png" or ".jpg".
     *
     * @param uri
     * @return Extension including the dot("."); "" if there is no extension;
     * null if uri was null.
     */
    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }
        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }

    public static boolean isImage(String mimeType) {
        return mimeType.toLowerCase().startsWith("image/");
    }

    public static boolean isVideo(String mimeType) {
        return mimeType.toLowerCase().startsWith("video/");
    }

    public static boolean isAudio(String mimeType) {
        return mimeType.toLowerCase().startsWith("audio/");
    }

    public static boolean isText(String mimeType) {
        return mimeType.toLowerCase().startsWith("text/");
    }

}