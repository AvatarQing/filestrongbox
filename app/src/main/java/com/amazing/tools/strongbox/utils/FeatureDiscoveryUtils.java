package com.amazing.tools.strongbox.utils;

import com.amazing.tools.strongbox.utils.data.Keeper;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/8/14 </li>
 * </ul>
 */

public class FeatureDiscoveryUtils {

    private static final String PK_IMPORT_TO_VAULT = "PK_IMPORT_TO_VAULT";
    private static final String PK_SELECT_IMAGE_FROM_GALLERY = "PK_SELECT_IMAGE_FROM_GALLERY";
    private static final String PK_SELECT_IMAGE_FROM_VAULT = "PK_SELECT_IMAGE_FROM_VAULT";

    public static boolean hasShownImportToVault() {
        return Keeper.get().hasShownFeatureDiscovery(PK_IMPORT_TO_VAULT);
    }

    public static void setShownImportToVault(boolean shown) {
        Keeper.get().setShownFeatureDiscovery(PK_IMPORT_TO_VAULT, shown);
    }

    public static boolean hasShownSelectImageFromGallery() {
        return Keeper.get().hasShownFeatureDiscovery(PK_SELECT_IMAGE_FROM_GALLERY);
    }

    public static void setShownSelectImageFromGallery(boolean shown) {
        Keeper.get().setShownFeatureDiscovery(PK_SELECT_IMAGE_FROM_GALLERY, shown);
    }

    public static boolean hasShownSelectImageFromVault() {
        return Keeper.get().hasShownFeatureDiscovery(PK_SELECT_IMAGE_FROM_VAULT);
    }

    public static void setShownSelectImageFromVault(boolean shown) {
        Keeper.get().setShownFeatureDiscovery(PK_SELECT_IMAGE_FROM_VAULT, shown);
    }

}
