package com.amazing.tools.strongbox.ui.helper;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.amazing.tools.ad.AdmobInterstitialAdManager;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.dataholder.FileListDataHolder;
import com.amazing.tools.strongbox.db.BatchFlowContentObserver;
import com.amazing.tools.strongbox.db.EncryptedFilesDao;
import com.amazing.tools.strongbox.entity.EncryptedFileAlbum;
import com.amazing.tools.strongbox.entity.EncryptedFileInfo;
import com.amazing.tools.strongbox.entity.FileItem;
import com.amazing.tools.strongbox.exception.AlbumAlreadyExistedException;
import com.amazing.tools.strongbox.ui.activity.LotteryActivity;
import com.amazing.tools.strongbox.ui.activity.MediaPreviewActivity;
import com.amazing.tools.strongbox.ui.dialog.AlbumNameEditDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.ChangeFilesAlbumDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.DecryptInProgressDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.EncryptCompleteDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.EncryptFilesDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.EncryptInProgressDialogFragment;
import com.amazing.tools.strongbox.ui.dialog.WarningDialogFragment;
import com.amazing.tools.strongbox.utils.FileTypeUtils;
import com.amazing.tools.strongbox.utils.encrypt.EncryptHelper;
import com.amazing.tools.strongbox.utils.entityhelper.FileItemFactory;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

/**
 * @author LiuQing
 */
public class EncryptedFileUIHelper {
    private static final String TAG = EncryptedFileUIHelper.class.getSimpleName();

    public interface OnDecryptCompleteListener {
        void onDecryptCompleted();
    }

    public interface OnDeleteCompleteListener {
        void onDeleteCompleted(int toDeleteFileCount, int deletedFileCount);
    }

    public interface OnChangeFilesAlbumListener {
        void onFilesAlbumChangedCompleted();
    }

    private FragmentActivity mActivity;
    private FragmentManager mFragmentManager;
    private LifecycleProvider<ActivityEvent> mLifecycleProvider;
    private List<DialogInterface.OnDismissListener> mEncryptCompleteDialogDismissListeners = new ArrayList<>();
    private List<OnDecryptCompleteListener> mOnDecryptCompleteListeners = new ArrayList<>();
    private List<OnDeleteCompleteListener> mOnDeleteCompleteListeners = new ArrayList<>();
    private List<OnChangeFilesAlbumListener> mOnChangeFilesAlbumListeners = new ArrayList<>();

    public EncryptedFileUIHelper(FragmentActivity activity, LifecycleProvider<ActivityEvent> lifecycleProvider) {
        this.mActivity = activity;
        this.mLifecycleProvider = lifecycleProvider;
        this.mFragmentManager = activity.getSupportFragmentManager();
    }

    public void addOnEncryptCompleteDialogDismissListener(DialogInterface.OnDismissListener listener) {
        this.mEncryptCompleteDialogDismissListeners.add(listener);
    }

    public void removeOnEncryptCompleteDialogDismissListener(DialogInterface.OnDismissListener listener) {
        this.mEncryptCompleteDialogDismissListeners.remove(listener);
    }

    public void addOnDecryptCompleteListener(OnDecryptCompleteListener listener) {
        this.mOnDecryptCompleteListeners.add(listener);
    }

    public void removeOnDecryptCompleteListener(OnDecryptCompleteListener listener) {
        this.mOnDecryptCompleteListeners.remove(listener);
    }

    public void addOnDeleteCompleteListener(OnDeleteCompleteListener listener) {
        this.mOnDeleteCompleteListeners.add(listener);
    }

    public void removeOnDeleteCompleteListener(OnDeleteCompleteListener listener) {
        this.mOnDeleteCompleteListeners.remove(listener);
    }

    public void addOnChangeFilesAlbumListener(OnChangeFilesAlbumListener listener) {
        this.mOnChangeFilesAlbumListeners.add(listener);
    }

    public void removeOnChangeFilesAlbumListener(OnChangeFilesAlbumListener listener) {
        this.mOnChangeFilesAlbumListeners.remove(listener);
    }

    public void removeAllListeners() {
        mEncryptCompleteDialogDismissListeners.clear();
        mOnDecryptCompleteListeners.clear();
        mOnDeleteCompleteListeners.clear();
    }

    private String getString(@StringRes int resId) {
        return mActivity.getString(resId);
    }

    private String getString(@StringRes int resId, Object... args) {
        return mActivity.getString(resId, args);
    }

    public void showEncryptCountNotEnoughDialog() {
        long availableEncryptCount = EncryptHelper.getInstance().getAvailableEncryptCount();
        new AlertDialog.Builder(mActivity)
                .setTitle(R.string.title_no_enough_encrypt_count)
                .setMessage(getString(R.string.format_available_encrypt_count, availableEncryptCount))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.btn_continue, (dialog, which) -> mActivity.startActivity(new Intent(mActivity, LotteryActivity.class)))
                .create()
                .show();
    }

    public void showEncryptConfirmDialog(final ArrayList<FileItem> selectedImageList, final EncryptedFileAlbum album) {
        String adId = getString(R.string.admob_interstitial_after_encrypt_ad_id);
        AdmobInterstitialAdManager.getInstance().createAdIfAbsent(mActivity, adId, true, "InterstitialAd-AfterEncrypt");
        AdmobInterstitialAdManager.getInstance().loadAd(adId);

        EncryptHelper encryptHelper = EncryptHelper.getInstance();
        if (encryptHelper.hasEnoughEncryptCount(selectedImageList.size())) {
            new EncryptFilesDialogFragment.Builder()
                    .setSelectedFileList(selectedImageList)
                    .setOnEncryptButtonClickListener((dialogFragment, view, selectedImageList1) -> {
                        startEncrypt(selectedImageList1, album);
                        dialogFragment.dismiss();
                    })
                    .build()
                    .show(mFragmentManager, "EncryptFilesDialogFragment");
        } else {
            showEncryptCountNotEnoughDialog();
        }
    }

    private void startEncrypt(final ArrayList<FileItem> selectedImageList, final EncryptedFileAlbum album) {
        final EncryptInProgressDialogFragment dialogFragment = new EncryptInProgressDialogFragment();
        dialogFragment.show(mFragmentManager, "EncryptInProgressDialogFragment");

        Observable.fromIterable(selectedImageList)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<FileItem>() {
                    @Override
                    public void accept(@NonNull FileItem fileItem) throws Exception {
                        int index = selectedImageList.indexOf(fileItem);
                        dialogFragment.updateEncryptingProgress(index, selectedImageList.size(), fileItem.name);
                    }
                })
                .observeOn(Schedulers.io())
                .map(new Function<FileItem, EncryptCompleteDialogFragment.FileInfo>() {
                    @Override
                    public EncryptCompleteDialogFragment.FileInfo apply(@NonNull FileItem fileItem) throws Exception {
                        boolean isEncryptSuccess = EncryptHelper.getInstance().encryptFile(getContext(), fileItem, album);
                        EncryptCompleteDialogFragment.FileInfo fileInfo = new EncryptCompleteDialogFragment.FileInfo();
                        fileInfo.fileName = fileItem.name;
                        fileInfo.fileSize = fileItem.size;
                        fileInfo.isEncryptSuccess = isEncryptSuccess;
                        return fileInfo;
                    }
                })
                .toList()
                .compose(mLifecycleProvider.bindUntilEvent(ActivityEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<EncryptCompleteDialogFragment.FileInfo>>() {
                    long encryptStartTime;

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        encryptStartTime = System.currentTimeMillis();
                        BatchFlowContentObserver.getInstance().beginTransaction();
                    }

                    @Override
                    public void onSuccess(@NonNull List<EncryptCompleteDialogFragment.FileInfo> fileInfoList) {
                        BatchFlowContentObserver.getInstance().endTransactionAndNotify();
                        long consumingTime = System.currentTimeMillis() - encryptStartTime;
                        dialogFragment.dismiss();
                        new EncryptCompleteDialogFragment.Builder()
                                .setFileInfoList(fileInfoList)
                                .setConsumingTime(consumingTime)
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        if (mEncryptCompleteDialogDismissListeners != null) {
                                            for (DialogInterface.OnDismissListener listener : mEncryptCompleteDialogDismissListeners) {
                                                listener.onDismiss(dialog);
                                            }
                                        }

                                        String adId = getString(R.string.admob_interstitial_after_encrypt_ad_id);
                                        AdmobInterstitialAdManager.getInstance().showAd(adId);
                                    }
                                })
                                .build()
                                .show(mFragmentManager, "EncryptCompleteDialogFragment");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void showDecryptConfirmDialog(final ArrayList<EncryptedFileInfo> selectedImageList) {
        new WarningDialogFragment.Builder()
                .setTitle(getString(R.string.prompt_decrypt_files_title))
                .setMessage(getString(R.string.prompt_decrypt_files_desc))
                .setOnOkButtonClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDecryptFiles(selectedImageList);
                        dialog.dismiss();
                    }
                })
                .build()
                .show(mFragmentManager, "showDecryptConfirmDialog");
    }

    private void startDecryptFiles(final ArrayList<EncryptedFileInfo> selectedImageList) {
        final DecryptInProgressDialogFragment dialogFragment = new DecryptInProgressDialogFragment();
        dialogFragment.show(mFragmentManager, "DecryptInProgressDialogFragment");

        Observable.fromIterable(selectedImageList)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<EncryptedFileInfo>() {
                    @Override
                    public void accept(@NonNull EncryptedFileInfo item) throws Exception {
                        if (!dialogFragment.isDetached()) {
                            int currentIndex = selectedImageList.indexOf(item) + 1;
                            int totalCount = selectedImageList.size();
                            dialogFragment.updateProgressUI(currentIndex, totalCount, item.originalFileName);
                        }
                    }
                })
                .observeOn(Schedulers.io())
                .map(new Function<EncryptedFileInfo, DecryptInProgressDialogFragment.FileInfo>() {
                    @Override
                    public DecryptInProgressDialogFragment.FileInfo apply(@NonNull EncryptedFileInfo item) throws Exception {
                        boolean isDecryptSuccess = EncryptHelper.getInstance().decryptFile(mActivity.getApplicationContext(), item);
                        DecryptInProgressDialogFragment.FileInfo fileInfo = new DecryptInProgressDialogFragment.FileInfo();
                        fileInfo.fileName = item.originalFileName;
                        fileInfo.isDecryptSuccess = isDecryptSuccess;
                        return fileInfo;
                    }
                })
                .toList()
                .compose(mLifecycleProvider.bindUntilEvent(ActivityEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DecryptInProgressDialogFragment.FileInfo>>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        BatchFlowContentObserver.getInstance().beginTransaction();
                    }

                    @Override
                    public void onSuccess(@NonNull List<DecryptInProgressDialogFragment.FileInfo> fileInfoList) {
                        BatchFlowContentObserver.getInstance().endTransactionAndNotify();
                        if (!dialogFragment.isDetached()) {
                            int totalCount = fileInfoList.size();
                            String lastFileName = fileInfoList.get(totalCount - 1).fileName;
                            dialogFragment.updateProgressUI(totalCount, totalCount, lastFileName);
                            dialogFragment.updateFinalStateUI(fileInfoList);
                        }
                        if (mOnDecryptCompleteListeners != null) {
                            for (OnDecryptCompleteListener listener : mOnDecryptCompleteListeners) {
                                listener.onDecryptCompleted();
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void showConfirmDeleteFileDialog(final ArrayList<EncryptedFileInfo> toDeleteFiles) {
        new WarningDialogFragment.Builder()
                .setTitle(getString(R.string.title_delete_files))
                .setMessage(getString(R.string.prompt_delete_files))
                .setOnOkButtonClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDeleteFiles(toDeleteFiles);
                        dialog.dismiss();
                    }
                })
                .build()
                .show(mFragmentManager, "showDecryptConfirmDialog");
    }

    private void startDeleteFiles(ArrayList<EncryptedFileInfo> toDeleteFiles) {
        if (toDeleteFiles == null || toDeleteFiles.isEmpty()) {
            Timber.tag(TAG).d("startDeleteFiles()-> no files to delete");
            return;
        }
        final int toDeleteFileCount = toDeleteFiles.size();
        Observable.fromIterable(toDeleteFiles)
                .map(new Function<EncryptedFileInfo, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull EncryptedFileInfo encryptedFileInfo) throws Exception {
                        File encryptedFile = new File(encryptedFileInfo.encryptedFilePath);
                        if (!TextUtils.isEmpty(encryptedFileInfo.thumbnailFilePath)) {
                            File thumbnailFile = new File(encryptedFileInfo.thumbnailFilePath);
                            if (thumbnailFile.exists()) {
                                thumbnailFile.delete();
                            }
                        }
                        boolean deleted = encryptedFile.delete();
                        if (deleted && encryptedFileInfo.isValid()) {
                            return FlowManager.getModelAdapter(EncryptedFileInfo.class).delete(encryptedFileInfo);
                        }
                        return deleted;
                    }
                })
                .toList()
                .compose(mLifecycleProvider.bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Boolean>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        BatchFlowContentObserver.getInstance().beginTransaction();
                    }

                    @Override
                    public void onSuccess(@NonNull List<Boolean> deleteResultList) {
                        BatchFlowContentObserver.getInstance().endTransactionAndNotify();
                        int deletedFileCount = 0;
                        for (Boolean deleted : deleteResultList) {
                            if (deleted) {
                                deletedFileCount++;
                            }
                        }
                        // TODO: 2017/7/20 显示删除失败的个数
                        String promptText = getString(R.string.format_files_deleted_successful, deletedFileCount);
                        Toast.makeText(mActivity, promptText, Toast.LENGTH_LONG).show();
                        if (mOnDeleteCompleteListeners != null) {
                            for (OnDeleteCompleteListener listener : mOnDeleteCompleteListeners) {
                                listener.onDeleteCompleted(toDeleteFileCount, deletedFileCount);
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void showConfirmMoveFileDialog(final ArrayList<EncryptedFileInfo> toMoveFiles) {
        new AlertDialog.Builder(mActivity)
                .setTitle(getString(R.string.title_delete_files))
                .setMessage(getString(R.string.prompt_move_files_to_recycle_bin))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startMoveFiles(toMoveFiles);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create()
                .show();
    }

    private void startMoveFiles(ArrayList<EncryptedFileInfo> toDeleteFiles) {
        if (toDeleteFiles == null || toDeleteFiles.isEmpty()) {
            Timber.tag(TAG).d("startDeleteFiles()-> no files to delete");
            return;
        }
        final int toDeleteFileCount = toDeleteFiles.size();
        Observable.just(toDeleteFiles)
                .map(fileList -> {
                    EncryptedFilesDao.moveFilesToRecycleBin(fileList);
                    return fileList.size();
                })
                .compose(mLifecycleProvider.bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new SingleObserver<Integer>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        BatchFlowContentObserver.getInstance().beginTransaction();
                    }

                    @Override
                    public void onSuccess(@NonNull Integer deletedFileCount) {
                        BatchFlowContentObserver.getInstance().endTransactionAndNotify();

                        String promptText = getString(R.string.format_files_had_move_to_recycle_bin, deletedFileCount);
                        Toast.makeText(mActivity, promptText, Toast.LENGTH_LONG).show();

                        if (mOnDeleteCompleteListeners != null) {
                            for (OnDeleteCompleteListener listener : mOnDeleteCompleteListeners) {
                                listener.onDeleteCompleted(toDeleteFileCount, deletedFileCount);
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void openFile(EncryptedFileInfo fileInfo, List<EncryptedFileInfo> belongDataList) {
        switch (fileInfo.fileType) {
            case FileTypeUtils.TYPE_IMAGE:
            case FileTypeUtils.TYPE_VIDEO:
                previewMedia(fileInfo, belongDataList);
                break;
            case FileTypeUtils.TYPE_AUDIO:
            case FileTypeUtils.TYPE_FILE:
            case FileTypeUtils.TYPE_TEXT:
            default:
                openFileByOtherApp(fileInfo);
                break;
        }
    }

    public void openFileByOtherApp(EncryptedFileInfo fileInfo) {
        File imageFile = new File(fileInfo.encryptedFilePath);
        String mimeType = fileInfo.mimeType;
        Intent intent = new Intent(Intent.ACTION_VIEW)
                .setDataAndType(Uri.fromFile(imageFile), mimeType);
        try {
            mActivity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(mActivity, R.string.prompt_no_app_to_open_this_file, Toast.LENGTH_LONG).show();
        }
    }

    private void previewMedia(final EncryptedFileInfo fileInfo, List<EncryptedFileInfo> belongDataList) {
        Observable.fromIterable(belongDataList)
                .filter(file -> file.fileType == FileTypeUtils.TYPE_IMAGE || file.fileType == FileTypeUtils.TYPE_VIDEO)
                .map(FileItemFactory::parseFileItemFromEncryptFileInfo)
                .toList()
                .compose(mLifecycleProvider.bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<FileItem>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull List<FileItem> dataList) {
                        FileItem clickedFileItem = FileItemFactory.parseFileItemFromEncryptFileInfo(fileInfo);
                        int filteredIndex = dataList.indexOf(clickedFileItem);
                        FileListDataHolder.setData(dataList);
                        MediaPreviewActivity.launch(mActivity, filteredIndex, true);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    public void showRenameFileDialog(final EncryptedFileInfo fileInfo) {
        @SuppressLint("InflateParams")
        View contentView = mActivity.getLayoutInflater().inflate(R.layout.dialog_content_rename_file, null);
        final MaterialEditText editText = ButterKnife.findById(contentView, R.id.edit_text);
        editText.setText(fileInfo.originalFileName);
        editText.selectAll();

        new AlertDialog.Builder(mActivity)
                .setTitle(R.string.btn_rename)
                .setView(contentView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO: 2017/7/19 如果输入的不符合命名规范，应该不给提交
                        startRenameFile(fileInfo, editText.getText().toString());
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void startRenameFile(final EncryptedFileInfo fileInfo, String newFileName) {
        if (!fileInfo.isValid()) {
            Timber.tag(TAG).d("startRenameFile()-> file is not valid. originalFilePath:" + fileInfo.originalFilePath);
            return;
        }
        fileInfo.originalFileName = newFileName;
        Observable
                .fromCallable(new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return FlowManager.getModelAdapter(EncryptedFileInfo.class).update(fileInfo);
                    }
                })
                .compose(mLifecycleProvider.bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull Boolean isUpdateSuccess) {
                        if (isUpdateSuccess) {
                            Toast.makeText(mActivity, R.string.prompt_rename_file_successful, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void shareFile(EncryptedFileInfo fileInfo) {
        File imageFile = new File(fileInfo.encryptedFilePath);
        Intent intent = new Intent(Intent.ACTION_SEND)
                .setDataAndType(Uri.fromFile(imageFile), "image/*");
        mActivity.startActivity(Intent.createChooser(intent, ""));
    }

    public void showFileInfoDialog(EncryptedFileInfo fileInfo) {
        @SuppressLint("InflateParams")
        View contentView = mActivity.getLayoutInflater().inflate(R.layout.dialog_content_file_info, null);
        TextView tv_file_name = ButterKnife.findById(contentView, R.id.tv_file_name);
        TextView tv_file_location = ButterKnife.findById(contentView, R.id.tv_file_location);
        TextView tv_file_size = ButterKnife.findById(contentView, R.id.tv_file_size);
        TextView tv_file_modified_date = ButterKnife.findById(contentView, R.id.tv_file_modified_date);

        tv_file_name.setText(fileInfo.originalFileName);
        tv_file_location.setText(fileInfo.originalFilePath);
        tv_file_size.setText(Formatter.formatFileSize(getContext(), fileInfo.fileSize));
        tv_file_modified_date.setText(SimpleDateFormat.getDateTimeInstance().format(fileInfo.modifiedTime * 1000));

        new AlertDialog.Builder(mActivity)
                .setTitle(R.string.btn_details)
                .setView(contentView)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    public void showChangeAlbumDialog(EncryptedFileAlbum album, List<EncryptedFileInfo> fileInfoList) {
        new ChangeFilesAlbumDialogFragment.Builder()
                .setCurrentAlbum(album)
                .setOnChangeFilesAlbumListener(new ChangeFilesAlbumDialogFragment.OnChangeFilesAlbumListener() {
                    @Override
                    public void onCreateNewAlbum(ChangeFilesAlbumDialogFragment changeFilesAlbumDialogFragment) {
                        changeFilesAlbumDialogFragment.dismiss();
                        new AlbumNameEditDialogFragment.Builder()
                                .setTitle(getString(R.string.title_create_album))
                                .setPositiveClickListener((createAlbumDialogFragment, inputText) -> {
                                    createAlbumDialogFragment.dismiss();
                                    EncryptedFilesDao.createAlbumInRx(inputText)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver<EncryptedFileAlbum>() {
                                                @Override
                                                public void onSubscribe(@NonNull Disposable d) {
                                                }

                                                @Override
                                                public void onSuccess(@NonNull EncryptedFileAlbum album) {
                                                    changeFilesAlbum(fileInfoList, album);
                                                }

                                                @Override
                                                public void onError(@NonNull Throwable e) {
                                                    e.printStackTrace();
                                                    if (e instanceof AlbumAlreadyExistedException) {
                                                        Toast.makeText(mActivity, R.string.prompt_album_existed, Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(mActivity, R.string.prompt_unknown_error, Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                })
                                .create()
                                .show(mFragmentManager, "CreateNewAlbumThenMoveFilesToIt");
                    }

                    @Override
                    public void onSelectedOneAlbum(ChangeFilesAlbumDialogFragment dialogFragment, EncryptedFileAlbum album) {
                        dialogFragment.dismiss();
                        changeFilesAlbum(fileInfoList, album);
                    }
                })
                .create()
                .show(mFragmentManager, "changeAlbum");
    }

    private void changeFilesAlbum(List<EncryptedFileInfo> fileInfoList, EncryptedFileAlbum album) {
        EncryptedFilesDao.changeFilesAlbumInRx(fileInfoList, album.id)
                .subscribe((hasChangeSuccess, throwable) -> {
                    if (hasChangeSuccess) {
                        String toastText = getString(R.string.format_change_file_album, fileInfoList.size(), album.name);
                        Toast.makeText(mActivity, toastText, Toast.LENGTH_SHORT).show();

                        for (OnChangeFilesAlbumListener listener : mOnChangeFilesAlbumListeners) {
                            listener.onFilesAlbumChangedCompleted();
                        }
                    }
                });
    }
}