package com.amazing.tools.strongbox.entity;

import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * @author LiuQing
 */
public class FileFolder implements Comparable<FileFolder> {
    /**
     * 图片文件夹名称
     */
    public String name;
    /**
     * 图片文件夹路径
     */
    public String path;
    /**
     * 所包含的图片文件个数
     */
    public int fileCount;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MediaFolder) {
            MediaFolder another = (MediaFolder) obj;
            return TextUtils.equals(this.path, another.path);
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@NonNull FileFolder another) {
        return this.name.toLowerCase().compareTo(another.name.toLowerCase());
    }

}
