package com.amazing.tools.strongbox.ui.adatpter;

import android.view.View;
import android.widget.ImageView;

import com.amazing.tools.strongbox.R;
import com.amazing.utility.multichoice.ChoiceModeHelper;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

/**
 * Author: LiuQing
 * Date: 2017/7/3
 */

public abstract class BaseImageListAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {

    private static final float PRESSED_SCALE = 0.92f;
    private static final int ANIMATION_DURATION = 300;

    private ChoiceModeHelper mChoiceModeHelper;

    public BaseImageListAdapter() {
        super(R.layout.item_image);
    }

    public void setChoiceModeHelper(ChoiceModeHelper choiceModeHelper) {
        this.mChoiceModeHelper = choiceModeHelper;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        int position = helper.getAdapterPosition();

        ImageView imageView = helper.getView(R.id.image);
        loadImage(imageView, item);

        helper.setVisible(R.id.video_indicator, needShowVideoIndicator());

        boolean checked = mChoiceModeHelper != null && mChoiceModeHelper.isItemChecked(position);

        float scale = checked ? PRESSED_SCALE : 1f;
        helper.itemView.setScaleX(scale);
        helper.itemView.setScaleY(scale);

        updateItemCheckUI(helper, checked);
    }

    protected boolean needShowVideoIndicator() {
        return false;
    }

    protected abstract void loadImage(ImageView imageView, T item);

    private void updateItemCheckUI(BaseViewHolder helper, boolean isChecked) {
        float checkboxScale = isChecked ? 1f : 0f;
        float maskAlpha = isChecked ? 1f : 0f;

        helper.getView(R.id.checkbox).setScaleX(checkboxScale);
        helper.getView(R.id.checkbox).setScaleY(checkboxScale);
        helper.getView(R.id.mask).setAlpha(maskAlpha);
    }

    public void startCheckedAnimation(int position, boolean checked) {
        BaseViewHolder holder = (BaseViewHolder) getRecyclerView().findViewHolderForAdapterPosition(position);
        if (holder != null) {
            float itemScale = checked ? PRESSED_SCALE : 1f;
            float checkboxScale = checked ? 1f : 0f;
            animateToScaleView(holder.itemView, itemScale);
            animateToScaleView(holder.getView(R.id.checkbox), checkboxScale);
            animateToFadeView(holder.getView(R.id.mask), checked);
        }
    }

    private void animateToScaleView(View view, float scale) {
        view.animate()
                .scaleX(scale)
                .scaleY(scale)
                .setDuration(ANIMATION_DURATION)
                .start();
    }

    private void animateToFadeView(View view, boolean fadeIn) {
        view.animate()
                .alpha(fadeIn ? 1f : 0f)
                .setDuration(ANIMATION_DURATION)
                .start();
    }
}