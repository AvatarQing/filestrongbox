package com.amazing.tools.strongbox.entity;

/**
 * @author LiuQing
 */
public class MediaFolder extends FileFolder {
    /**
     * 文件夹封面图片路径（最新一张图片）
     */
    public String cover;

    @Override
    public String toString() {
        return "MediaFolder{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", fileCount=" + fileCount +
                ", cover='" + cover + '\'' +
                '}';
    }

}