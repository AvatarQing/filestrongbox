package com.amazing.tools.strongbox.ui.dialog;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.daimajia.numberprogressbar.NumberProgressBar;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * <ul>
 * <li> Author: LiuQing </li>
 * <li> Date: 2017/7/12 </li>
 * </ul>
 */

public class DecryptInProgressDialogFragment extends BaseDialogFragment {

    @BindView(R.id.tv_current_file)
    TextView tv_current_file;
    @BindView(R.id.progress_bar)
    NumberProgressBar progress_bar;
    @BindView(R.id.btn_done)
    View btn_done;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private FileStateListAdapter mAdapter;

    @Override
    protected boolean enableWindowAnim() {
        return false;
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getContext()).inflate(R.layout.dialog_decrypt_in_progress, parentView, false);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);
        initRecyclerView();
        updateProgressUI(0, 0, "");
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(layoutManager);

        FileStateListAdapter adapter = new FileStateListAdapter(R.layout.item_encrypt_or_decrypt_file_state, null);
        recycler_view.setAdapter(adapter);

        mAdapter = adapter;
    }

    private void resetRecyclerViewHeight(int dataCount) {
        int maxCount = 3;
        int visibleItemCount = dataCount;
        if (visibleItemCount > maxCount) {
            visibleItemCount = maxCount;
        }
        int itemHeight = getResources().getDimensionPixelSize(R.dimen.item_height);
        int recyclerViewHeight = itemHeight * visibleItemCount;
        ViewUtils.updateViewHeight(recycler_view, recyclerViewHeight);
    }

    public void updateProgressUI(int currentIndex, int totalCount, String fileName) {
        int maxProgress = progress_bar.getMax();
        int progress = totalCount == 0 ? 0 : maxProgress * currentIndex / totalCount;
        String countProgressText = getString(R.string.format_progress_of_count, currentIndex, totalCount);
        String currentFileProgressText = String.format(Locale.getDefault(), "%s %s", countProgressText, fileName);

        progress_bar.setProgress(progress);
        tv_current_file.setText(currentFileProgressText);
    }

    @OnClick(R.id.btn_done)
    void done() {
        dismiss();
    }

    public void updateFinalStateUI(List<FileInfo> fileInfoList) {
        btn_done.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.VISIBLE);
        resetRecyclerViewHeight(fileInfoList.size());
        mAdapter.setNewData(fileInfoList);
    }

    private static class FileStateListAdapter extends BaseQuickAdapter<FileInfo, BaseViewHolder> {

        private FileStateListAdapter(@LayoutRes int layoutResId, @Nullable List<FileInfo> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, FileInfo item) {
            helper.setText(R.id.tv_file_name, item.fileName);
            helper.setText(R.id.tv_state, item.isDecryptSuccess ? R.string.prompt_success : R.string.prompt_fail);
            helper.setBackgroundRes(R.id.iv_tip, item.isDecryptSuccess ? R.drawable.circle_success : R.drawable.circle_fail);
            helper.setImageResource(R.id.iv_tip, item.isDecryptSuccess ? R.drawable.ic_done : R.drawable.ic_exclaim);
        }
    }

    public static class FileInfo {
        public String fileName;
        public boolean isDecryptSuccess;
    }

}
