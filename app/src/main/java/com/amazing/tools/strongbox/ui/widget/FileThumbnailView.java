package com.amazing.tools.strongbox.ui.widget;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.R;

/**
 * @author LiuQing
 */
public class FileThumbnailView extends android.support.v7.widget.AppCompatImageView {

    public FileThumbnailView(@NonNull Context context) {
        this(context, null);
    }

    public FileThumbnailView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FileThumbnailView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setScaleType(ScaleType.CENTER_INSIDE);
    }

    public void showAsCircleImage(String imageUrl) {
        setPadding(0, 0, 0, 0);
        GlideApp.with(this)
                .load(imageUrl)
                .placeholder(R.drawable.image_placeholder_circle)
                .circleCrop()
                .into(this);
    }

    public void showAsFile() {
        showAs(R.drawable.file_thumbnail_normal_bg, R.drawable.ic_file);
    }

    public void showAsCheckBox() {
        showAs(R.drawable.file_thumbnail_selected_bg, R.drawable.ic_done);
    }

    public void showAs(@DrawableRes int bg, @DrawableRes int src) {
        int padding = getResources().getDimensionPixelOffset(R.dimen.file_thumbnail_content_padding);
        setPadding(padding, padding, padding, padding);
        setBackgroundResource(bg);
        setImageResource(src);
    }

}