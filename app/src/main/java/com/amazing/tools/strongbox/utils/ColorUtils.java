package com.amazing.tools.strongbox.utils;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.util.TypedValue;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/8/13 </li>
 * </ul>
 */

public class ColorUtils {

    @ColorInt
    public static int getColorFromAttr(Context context, int attr) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attr, typedValue, true);
        return typedValue.data;
    }

}
