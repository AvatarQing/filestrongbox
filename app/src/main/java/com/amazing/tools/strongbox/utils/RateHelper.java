package com.amazing.tools.strongbox.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.amazing.tools.strongbox.ui.dialog.RateDialogFragment;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/5 </li>
 * </ul>
 */

public class RateHelper {

    private static final int RATE_TYPE_AFTER_LAUNCH = 1;
    private static final int RATE_TYPE_AFTER_ENCRYPT = 2;

    private static final String PREF_NAME_RATE = "PREF_NAME_RATE";
    private static final String PREF_KEY_LAUNCH_TIME = "PREF_KEY_LAUNCH_TIME";
    private static final String PREF_KEY_SHOW_RATE_AFTER_LAUNCH = "PREF_KEY_SHOW_RATE_AFTER_LAUNCH";
    private static final String PREF_KEY_SHOW_RATE_AFTER_ENCRYPT = "PREF_KEY_SHOW_RATE_AFTER_ENCRYPT";

    private static volatile RateHelper sInstance;

    public static RateHelper getInstance() {
        if (sInstance == null) {
            synchronized (RateHelper.class) {
                if (sInstance == null) {
                    sInstance = new RateHelper();
                }
            }
        }
        return sInstance;
    }

    private RateHelper() {
    }

    public void showRateDialogAfterLaunch(FragmentActivity activity) {
        if (getAppLaunchTime(activity) >= 3 && needShowRateAfterLaunch(activity)) {
            showRateDialog(activity, RATE_TYPE_AFTER_LAUNCH);
        }
    }

    public void showRateDialogAfterEncrypt(FragmentActivity activity) {
        if (needShowRateAfterEncrypt(activity)) {
            showRateDialog(activity, RATE_TYPE_AFTER_ENCRYPT);
        }
    }

    private void showRateDialog(final FragmentActivity activity, final int rateType) {
        FragmentManager fm = activity.getSupportFragmentManager();
        if (fm.findFragmentByTag(RateDialogFragment.TAG) == null) {
            new RateDialogFragment.Builder()
                    .setPositiveButtonClickListener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setNoNeedShowRateAfterLaunch(activity);
                            setNoNeedShowRateAfterEncrypt(activity);
                        }
                    })
                    .setNegativeButtonClickListener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (rateType) {
                                case RATE_TYPE_AFTER_LAUNCH: {
                                    setNoNeedShowRateAfterLaunch(activity);
                                }
                                break;
                                case RATE_TYPE_AFTER_ENCRYPT: {
                                    setNoNeedShowRateAfterEncrypt(activity);
                                }
                                break;
                            }
                        }
                    })
                    .setNeutralButtonClickListener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setNoNeedShowRateAfterLaunch(activity);
                            setNoNeedShowRateAfterEncrypt(activity);
                        }
                    })
                    .create()
                    .show(fm, RateDialogFragment.TAG);
        }
    }

    // >>>>>>>>>> SharedPreferences 操作 >>>>>>>>>>>>>>

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME_RATE, Context.MODE_PRIVATE);
    }

    private boolean needShowRateAfterLaunch(Context context) {
        return getPrefs(context).getBoolean(PREF_KEY_SHOW_RATE_AFTER_LAUNCH, true);
    }

    private boolean needShowRateAfterEncrypt(Context context) {
        return getPrefs(context).getBoolean(PREF_KEY_SHOW_RATE_AFTER_ENCRYPT, true);
    }

    private void setNoNeedShowRateAfterLaunch(Context context) {
        getPrefs(context).edit().putBoolean(PREF_KEY_SHOW_RATE_AFTER_LAUNCH, false).apply();
    }

    private void setNoNeedShowRateAfterEncrypt(Context context) {
        getPrefs(context).edit().putBoolean(PREF_KEY_SHOW_RATE_AFTER_ENCRYPT, false).apply();
    }

    private int getAppLaunchTime(Context context) {
        return getPrefs(context).getInt(PREF_KEY_LAUNCH_TIME, 0);
    }

    public void increaseAppLaunchTime(Context context) {
        SharedPreferences pref = getPrefs(context);
        int launchTime = pref.getInt(PREF_KEY_LAUNCH_TIME, 0);
        launchTime++;
        pref.edit().putInt(PREF_KEY_LAUNCH_TIME, launchTime).apply();
    }
}