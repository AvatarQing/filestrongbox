package com.amazing.tools.strongbox.ui.dialog;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.CycleInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.utils.LockPinHelper;
import com.amazing.tools.strongbox.utils.ViewUtils;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/7/29 </li>
 * </ul>
 */

public class SetLockPinDialogFragment extends BaseDialogFragment {

    private static final String TAG = SetLockPinDialogFragment.class.getSimpleName();

    @BindView(R.id.pin_lock_view)
    PinLockView pin_lock_view;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicator_dots;
    @BindView(R.id.tv_caution)
    TextView tv_caution;
    @BindView(R.id.btn_ok)
    Button btn_ok;

    private boolean hasInputOnce = false;
    private String tempPassword;
    private String firstInputPassword;
    private ViewPropertyAnimator shakeAnim;

    @Override
    protected int provideWindowAnimations() {
        return R.style.DialogAnimPushInAndOut;
    }

    @Override
    protected View provideDialogContentView(ViewGroup parentView) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.dialog_set_lock_pin, null);
    }

    @Override
    protected void initViews(View dialogContentView) {
        ButterKnife.bind(this, dialogContentView);

        pin_lock_view.setPinLength(getResources().getInteger(R.integer.pin_length));
        pin_lock_view.attachIndicatorDots(indicator_dots);
        pin_lock_view.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                Timber.tag(TAG).d("Pin complete: " + pin);
                tempPassword = pin;
                if (hasInputOnce) { // 第二次输入完密码，判断跟第一次输入的是不是一样
                    boolean isSame = TextUtils.equals(pin, firstInputPassword);
                    if (isSame) {
                        tv_caution.setText(R.string.prompt_pin_recorded);
                        btn_ok.setEnabled(true);
                    } else {
                        tv_caution.setText(R.string.prompt_password_is_not_consistent);
                        shakeInputBox();
                    }
                } else { // 第一次输入完密码，让用户可以点击下一步按钮
                    tv_caution.setText(R.string.prompt_pin_recorded);
                    btn_ok.setEnabled(true);
                }
            }

            @Override
            public void onEmpty() {
                Timber.tag(TAG).d("Pin empty");
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                Timber.tag(TAG).d("Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
                if (pinLength < getResources().getInteger(R.integer.pin_length)) {
                    btn_ok.setEnabled(false);
                }
                tv_caution.setText(hasInputOnce
                        ? R.string.prompt_confirm_new_password
                        : R.string.prompt_input_new_password
                );
            }
        });
    }

    private void shakeInputBox() {
        if (shakeAnim != null) {
            shakeAnim.cancel();
            indicator_dots.setTranslationX(0);
        }
        ViewPropertyAnimator animator = indicator_dots.animate()
                .translationX(ViewUtils.dp2px(getContext(), 8f))
                .setInterpolator(new CycleInterpolator(3))
                .setDuration(400);
        animator.start();
        shakeAnim = animator;
    }

    @OnClick(R.id.btn_cancel)
    void exit() {
        dismiss();
    }

    @OnClick(R.id.btn_ok)
    void nextOrUpdate() {
        if (hasInputOnce) { // 更新
            LockPinHelper.get().savePin(firstInputPassword);
            dismiss();
        } else { // 下一步
            hasInputOnce = true;
            firstInputPassword = tempPassword;
            pin_lock_view.resetPinLockView();
            tv_caution.setText(R.string.prompt_confirm_new_password);
            btn_ok.setText(R.string.btn_update);
            btn_ok.setEnabled(false);
        }
    }

}
