package com.amazing.tools.strongbox.ui.adatpter;

import android.widget.ImageView;

import com.amazing.tools.strongbox.GlideApp;
import com.amazing.tools.strongbox.R;
import com.amazing.tools.strongbox.entity.MediaFolder;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.Locale;

/**
 * <ul>
 * <li> Author: AvatarQing </li>
 * <li> Date: 2017/9/19 </li>
 * </ul>
 */

public class ImageFolderListAdapter extends BaseQuickAdapter<MediaFolder, BaseViewHolder> {

    public ImageFolderListAdapter() {
        super(R.layout.item_image_folder);
    }

    @Override
    protected void convert(BaseViewHolder helper, MediaFolder item) {
        helper.setText(R.id.folder_name, item.name);
        helper.setText(R.id.folder_file_count, String.format(Locale.getDefault(), "%d", item.fileCount));
        ImageView imageView = helper.getView(R.id.folder_cover);
        GlideApp.with(imageView)
                .load(item.cover)
                .into(imageView);
    }

}