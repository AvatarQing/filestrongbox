package com.amazing.tools.strongbox.utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.BidiFormatter;
import android.text.TextUtils;
import android.view.View;

import java.util.Locale;

/**
 * Author: LiuQing
 * Date: 2017/7/10
 */

public class FileSizeFormatter {

    private static final long KB_IN_BYTES = 1024;
    private static final long MB_IN_BYTES = KB_IN_BYTES * 1024;
    private static final long GB_IN_BYTES = MB_IN_BYTES * 1024;
    private static final long TB_IN_BYTES = GB_IN_BYTES * 1024;
    public static final long PB_IN_BYTES = TB_IN_BYTES * 1024;
    private static final int FLAG_SHORTER = 1 << 0;
    private static final int FLAG_CALCULATE_ROUNDED = 1 << 1;

    public static class BytesResult {
        public final String value;
        public final String units;
        public final long roundedBytes;

        public BytesResult(String value, String units, long roundedBytes) {
            this.value = value;
            this.units = units;
            this.roundedBytes = roundedBytes;
        }
    }

    /* Wraps the source string in bidi formatting characters in RTL locales */
    private static String bidiWrap(@NonNull Context context, String source) {
        final Locale locale = context.getResources().getConfiguration().locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if (TextUtils.getLayoutDirectionFromLocale(locale) == View.LAYOUT_DIRECTION_RTL) {
                return BidiFormatter.getInstance(true /* RTL*/).unicodeWrap(source);
            }
        }
        return source;
    }

    /**
     * Formats a content size to be in the form of bytes, kilobytes, megabytes, etc.
     * <p>
     * If the context has a right-to-left locale, the returned string is wrapped in bidi formatting
     * characters to make sure it's displayed correctly if inserted inside a right-to-left string.
     * (This is useful in cases where the unit strings, like "MB", are left-to-right, but the
     * locale is right-to-left.)
     *
     * @param context   Context to use to load the localized units
     * @param sizeBytes size value to be formatted, in bytes
     * @return formatted string with the number
     */
    public static String formatFileSize(@Nullable Context context, long sizeBytes) {
        if (context == null) {
            return "";
        }
        final BytesResult res = formatBytes(context.getResources(), sizeBytes, 0);
        return bidiWrap(context, String.format("%1$s %2$s", res.value, res.units));
    }

    /**
     * Like {@link #formatFileSize}, but trying to generate shorter numbers
     * (showing fewer digits of precision).
     */
    public static String formatShortFileSize(@Nullable Context context, long sizeBytes) {
        if (context == null) {
            return "";
        }
        final BytesResult res = formatBytes(context.getResources(), sizeBytes, FLAG_SHORTER);
        return bidiWrap(context, String.format("%1$s %2$s", res.value, res.units));
    }

    private static final int UNIT_B = 0;
    private static final int UNIT_KB = 1;
    private static final int UNIT_MB = 2;
    private static final int UNIT_GB = 3;
    private static final int UNIT_TB = 4;
    private static final int UNIT_PB = 5;

    public static BytesResult formatBytes(Resources res, long sizeBytes, int flags) {
        final boolean isNegative = (sizeBytes < 0);
        float result = isNegative ? -sizeBytes : sizeBytes;
        int unitType = UNIT_B;
        long mult = 1;
        if (result > 900) {
            unitType = UNIT_KB;
            mult = KB_IN_BYTES;
            result = result / 1024;
        }
        if (result > 900) {
            unitType = UNIT_MB;
            mult = MB_IN_BYTES;
            result = result / 1024;
        }
        if (result > 900) {
            unitType = UNIT_GB;
            mult = GB_IN_BYTES;
            result = result / 1024;
        }
        if (result > 900) {
            unitType = UNIT_TB;
            mult = TB_IN_BYTES;
            result = result / 1024;
        }
        if (result > 900) {
            unitType = UNIT_PB;
            mult = PB_IN_BYTES;
            result = result / 1024;
        }
        // Note we calculate the rounded long by ourselves, but still let String.format()
        // compute the rounded value. String.format("%f", 0.1) might not return "0.1" due to
        // floating point errors.
        final int roundFactor;
        final String roundFormat;
        if (mult == 1 || result >= 100) {
            roundFactor = 1;
            roundFormat = "%.0f";
        } else if (result < 1) {
            roundFactor = 100;
            roundFormat = "%.2f";
        } else if (result < 10) {
            if ((flags & FLAG_SHORTER) != 0) {
                roundFactor = 10;
                roundFormat = "%.1f";
            } else {
                roundFactor = 100;
                roundFormat = "%.2f";
            }
        } else { // 10 <= result < 100
            if ((flags & FLAG_SHORTER) != 0) {
                roundFactor = 1;
                roundFormat = "%.0f";
            } else {
                roundFactor = 100;
                roundFormat = "%.2f";
            }
        }

        if (isNegative) {
            result = -result;
        }
        final String roundedString = String.format(roundFormat, result);

        // Note this might overflow if abs(result) >= Long.MAX_VALUE / 100, but that's like 80PB so
        // it's okay (for now)...
        final long roundedBytes =
                (flags & FLAG_CALCULATE_ROUNDED) == 0 ? 0
                        : (((long) Math.round(result * roundFactor)) * mult / roundFactor);

        final String units = getUnitString(unitType);

        return new BytesResult(roundedString, units, roundedBytes);
    }

    private static String getUnitString(int unitType) {
        switch (unitType) {
            case UNIT_PB:
                return "PB";
            case UNIT_GB:
                return "GB";
            case UNIT_MB:
                return "MB";
            case UNIT_KB:
                return "KB";
            case UNIT_B:
            default:
                return "B";
        }
    }
}