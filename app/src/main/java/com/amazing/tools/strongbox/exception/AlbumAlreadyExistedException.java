package com.amazing.tools.strongbox.exception;

/**
 * @author LiuQing
 */
public class AlbumAlreadyExistedException extends Exception {

    public AlbumAlreadyExistedException(String name) {
        super("Album named \"" + name + "\" has already existed");
    }

}